# Installation
Put everything that's in this repository into your Moodle's `/local/rwth_participants_view` subfolder and go to Site administration > Plugins > Plugins overview to install it.

# Development
If you change any JS code in `./amd/src/` you need to run `grunt uglify` in your Moodle root directory. It generates the uglified versions of the JS files and puts them into `./amd/build/`.