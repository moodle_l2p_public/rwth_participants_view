import Fragment from 'core/fragment';
import Templates from 'core/templates';
import Notification from 'core/notification';
import {get_strings as getStrings} from 'core/str';

let contextId;
let courseId;
const participantsTab = document.querySelector('[href="#participants"');
const invitationsTab = document.querySelector('[href="#invitations"');
const participants_table = document.getElementById("participants");
const invitations_table = document.getElementById("invitations");

const inv_ov = invitationsTab ? invitationsTab.closest('.nav-item').querySelector('.overlay-icon-container') : null;

const loadTabs = () => {
    if (participantsTab) {
        loadParticipants().then(
            document.querySelector('[href="#participants"').addEventListener('click', () => { loadParticipants(); })
        );
    }
    if (invitationsTab) {
        inv_ov.classList.remove("hidden");
        invitationsTab.style.pointerEvents = "none";
        loadInvitations().then(
            document.querySelector('[href="#invitations"').addEventListener('click', () => { loadInvitations(); })
        );
    }
};

const loadInvitations = () => {
    return Fragment.loadFragment('local_rwth_participants_view', 'invitations_table', contextId, {'courseid': courseId})
    .then((html, js) => {
        Templates.replaceNodeContents(invitations_table, html, js);
        // Reenable on success only
        inv_ov.classList.add("hidden"); // Remove loading icons
        invitationsTab.style.removeProperty("pointer-events"); // Make tab clickable
    })
    .fail(handleSDException);
};

const loadParticipants = (args = {}) => {
    return Fragment.loadFragment('local_rwth_participants_view', 'participants_table', contextId, {'courseid': courseId, ...args}
    ).then((html, js) => {
        Templates.replaceNodeContents(participants_table, html, js);
        linkFilters();
    }).fail(handleSDException);
};

const linkFilters = () => {
    let roleFilters = document.querySelectorAll('a.role');
    let groupFilter = document.querySelector('select[name=group]');
    let perPage = document.querySelector('select[name=perpage]');
    let paging = document.querySelector('nav.pagination') ?
                 document.querySelector('nav.pagination').querySelectorAll('li.page-item') : null;
    let reset = document.querySelector(".resettable") ?
                 document.querySelector('.resettable').querySelector('a') : null;
    let surnameFilter = document.querySelector('div.initialbar.lastinitial') ?
                 document.querySelector('div.initialbar.lastinitial').querySelectorAll('li.page-item') : null;
    let nameFilter = document.querySelector('div.initialbar.firstinitial') ?
                 document.querySelector('div.initialbar.firstinitial').querySelectorAll('li.page-item') : null;

    if (roleFilters) { roleFilters.forEach((role) => {
        role.addEventListener('click', (e) => {
            e.preventDefault();
            let roleid = e.target.dataset.roleid;
            if (e.target.classList.contains('btn-primary')) {
                // Already filtering by this role => remove role filter.
                roleid = 0;
            }
            loadParticipants({'roleid': roleid});
        });
    });}

    if (nameFilter) { nameFilter.forEach((initial) => {
        initial.addEventListener('click', (e) => {
            e.preventDefault();
            let classes = e.target.closest('li.page-item').classList;
            classes.remove('page-item', 'active', 'disabled');
            loadParticipants({'courseid': courseId, 'nameselector': classes[0]});
        });
    });}

    if (surnameFilter) { surnameFilter.forEach((initial) => {
        initial.addEventListener('click', (e) => {
            e.preventDefault();
            let classes = e.target.closest('li.page-item').classList;
            classes.remove('page-item', 'active', 'disabled');
            loadParticipants({'courseid': courseId, 'surnameselector': classes[0]});
        });
    });}

    if (groupFilter) { groupFilter.addEventListener('change', (e) => {
        e.preventDefault();
        loadParticipants({'courseid': courseId,'group': e.target.value});
    });}

    if (perPage) { perPage.addEventListener('change', (e) => {
        e.preventDefault();
        loadParticipants({'courseid': courseId, 'perpage': e.target.value});
    });}

    if (paging) { paging.forEach((pagenr) => {
            pagenr.addEventListener('click', (e) => {
                e.preventDefault();
                loadParticipants({'page': e.target.closest('li.page-item').dataset.pageNumber - 1});
            });
    });}

    if (reset) { reset.addEventListener('click', (e) => {
            e.preventDefault();
            loadParticipants({'courseid': courseId, 'reset': 1});
    });}
};

const handleSDException = (exception) => {
    if (exception.errorcode == 'access_sitedirectory_external_db_connection_problem') {
        let requiredStrings = [];
        requiredStrings.push({key: 'sdfail_banner',
                              component: 'local_rwth_participants_view'});
        requiredStrings.push({key: 'sdfail_inv_title',
                              component: 'local_rwth_participants_view'});
        getStrings(requiredStrings).then(([banner, inv_title]) => {
            Notification.addNotification({
                message: banner,
                type: 'warning'
            });
            // replace tab titles with info that functionality is not available
            document.querySelector('[href="#invitations"').innerHTML = inv_title;
            // Hide loading animation
            inv_ov.classList.add("hidden");
        }).catch();
    } else {
        Notification.addNotification({message: exception.message, type: 'warning'});
    }
};

export const init = (ctxid, cid) => {
    contextId = ctxid;
    courseId = cid;
    loadTabs();
};
