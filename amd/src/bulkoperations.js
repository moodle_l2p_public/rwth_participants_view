// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @copyright 2017-2022 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

import Notification from 'core/notification';
import {get_string as getString} from 'core/str';
import $ from 'jquery';

const SELECTORS = {
    TABLE: 'table.flexible',
    SELECT_ALL: 'input[type="checkbox"].bulkoperation[name="all"]',
    SELECT_SINGLE: 'input[type="checkbox"].bulkoperation:not([name="all"])',
    BULK_OPERATIONS_BAR: '.bulk-operations-bar',
    BULK_OPERATIONS_BAR_BTNS: '.bulk-operations-bar-btns',
    SELECTION_COUNTER: '.selectioncounter',
    CLEAR_SELECTION: '.clear-selection-btn',
};

/**
 * Get number of selected elements.
 *
 * @param {object} tabPane which "tab-content" element to check
 * @returns {integer} how many elements are selected
 */
function numberOfCheckedElements(tabPane) {
    let num = 0;
    tabPane.querySelectorAll(SELECTORS.SELECT_SINGLE).forEach(checkbox => {
        if (checkbox.checked) {
            num++;
        }
    });
    return num;
}

/**
 * Show the bar if at least one element is selected. Also update text with number
 * of selected elements.
 * @param {object} tabPane which "tab-content" element to update
 * @param {string} stringIdSelectedSingle string id for "1 element selected"
 * @param {string} stringIdSelectedMultiple string id for "x elements selected"
 */
function updateVisibility(tabPane, stringIdSelectedSingle, stringIdSelectedMultiple) {
    let bulkoperationsbar = tabPane.querySelector(SELECTORS.BULK_OPERATIONS_BAR);
    const num = numberOfCheckedElements(tabPane);
    if (num > 0) {
        let strId = stringIdSelectedSingle;
        if (num > 1) {
            strId = stringIdSelectedMultiple;
        }
        getString(strId, 'local_rwth_participants_view', num)
        .then(string => {
            tabPane.querySelector(SELECTORS.SELECTION_COUNTER).innerHTML = string;
            bulkoperationsbar.classList.remove('hidden');
            return string;
        }).catch(Notification.exception);
    } else {
        bulkoperationsbar.classList.add('hidden');
    }
}

/**
 * Make the bar floating when it would be out of view and normally placed otherwise.
 * @param {object} tabPane which "tab-content" element to update
 */
function updatePosition(tabPane) {
    const table = tabPane.querySelector(SELECTORS.TABLE);
    let bulkoperationsbar = tabPane.querySelector(SELECTORS.BULK_OPERATIONS_BAR);
    let bulkoperationsbarbuttons = tabPane.querySelector(SELECTORS.BULK_OPERATIONS_BAR_BTNS);
    let selectioncounter = tabPane.querySelector(SELECTORS.SELECTION_COUNTER);
    const bounding = table.getBoundingClientRect();
    const atbottom = bounding.bottom + bulkoperationsbar.clientHeight <= document.documentElement.clientHeight;
    const originalLineHeight = window.getComputedStyle(document.body).getPropertyValue('line-height');
    if (atbottom) {
        bulkoperationsbarbuttons.classList.remove('floating');
        bulkoperationsbarbuttons.style.removeProperty('right');
        bulkoperationsbar.style.lineHeight = originalLineHeight;
        bulkoperationsbar.style.paddingRight = 0;
        selectioncounter.style.display = 'inline';
    } else {
        bulkoperationsbarbuttons.classList.add('floating');
        bulkoperationsbarbuttons.style.right = Math.max(0, document.documentElement.clientWidth - bounding.right) + 'px';
        bulkoperationsbarbuttons.style.lineHeight = originalLineHeight;
        const buttonswidth = window.getComputedStyle(bulkoperationsbarbuttons).getPropertyValue('width');
        const buttonsheight = window.getComputedStyle(bulkoperationsbarbuttons).getPropertyValue('height');
        bulkoperationsbar.style.lineHeight = 'calc(' + buttonsheight + ' - .6rem)'; // Remove .3rem top and bottom padding.
        bulkoperationsbar.style.paddingRight = 'calc(' + buttonswidth + ' + .3rem)'; // Add .3rem left margin of button.
        selectioncounter.style.display = 'block';
    }
}

export const init = (tabid, stringIdSelectedSingle, stringIdSelectedMultiple) => {
    const tabPane = document.getElementById(tabid);
    let $tab = $('[href="#' + tabid + '"]');

    // Select/deselect all entries.
    let checkAll = tabPane.querySelector(SELECTORS.SELECT_ALL);
    checkAll.addEventListener('change', function() {
        var checked = checkAll.checked;
        tabPane.querySelectorAll(SELECTORS.SELECT_SINGLE).forEach(checkbox => {
            checkbox.checked = checked;
        });
        updateVisibility(tabPane, stringIdSelectedSingle, stringIdSelectedMultiple);
    });

    // Select/deselect single entry.
    tabPane.querySelectorAll(SELECTORS.SELECT_SINGLE).forEach((checkbox) => {
        checkbox.addEventListener('change', () => {
            updateVisibility(tabPane, stringIdSelectedSingle, stringIdSelectedMultiple);
        });
    });

    // Clear selection.
    let clearSelection = tabPane.querySelector(SELECTORS.CLEAR_SELECTION);
    clearSelection.addEventListener('click', () => {
        checkAll.checked = false;
        tabPane.querySelectorAll(SELECTORS.SELECT_SINGLE).forEach(checkbox => {
            checkbox.checked = false;
        });
        updateVisibility(tabPane, stringIdSelectedSingle, stringIdSelectedMultiple);
    });

    // Make bulk operations bar float when out of view.
    let scrollListener = () => updatePosition(tabPane);
    let resizeListener = () => updatePosition(tabPane);
    if ($tab.hasClass('active')) {
        document.addEventListener('scroll', scrollListener);
        window.addEventListener('resize', resizeListener);
    }
    $tab.on('shown.bs.tab', () => {
        updatePosition(tabPane);
        document.addEventListener('scroll', scrollListener);
        window.addEventListener('resize', resizeListener);
    });
    $tab.on('hidden.bs.tab', () => {
        document.removeEventListener('scroll', scrollListener);
        window.removeEventListener('resize', resizeListener);
    });

    // Set initial visibility and position of bulk operations bar.
    updatePosition(tabPane);
    updateVisibility(tabPane, stringIdSelectedSingle, stringIdSelectedMultiple);
};
