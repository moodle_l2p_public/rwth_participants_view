// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @copyright 2022 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

import Ajax from 'core/ajax';
import Fragment from 'core/fragment';
import Notification from 'core/notification';
import {get_strings as getStrings} from 'core/str';
import Templates from 'core/templates';
import $ from 'jquery';

let oldsearch = '';
let oldroleid = -1;

/**
 * When switching to the "Add participants" tab, focus the search field.
 */
function initialFocus() {
    let $tab = $('[href="#addparticipants"]');
    if ($tab.hasClass('active')) {
        focusSearchField();
    }
    $tab.on('shown.bs.tab', () => {
        focusSearchField();
    });
}

/**
 * Focuses the user search field.
 */
function focusSearchField() {
    let searchfield = document.querySelector('#enrol-search-form textarea[name="usersearch"]');
    searchfield.focus();
}

export const init = (contextid) => {
    initialFocus();

    // Disable next button when no search is entered.
    let searchform = document.getElementById('enrol-search-form');
    let searchfield = searchform.querySelector('textarea[name="usersearch"]');
    let next = searchform.querySelector('input[name="next"]');
    if (searchfield.value == '') {
        next.setAttribute('disabled', 'disabled');
    }
    searchfield.addEventListener('input', () => {
        if (searchfield.value.trim() == '') {
            next.setAttribute('disabled', 'disabled');
        } else {
            next.removeAttribute('disabled');
        }
    });

    searchform.addEventListener('submit', (e) => {
        e.preventDefault();

        const search = searchfield.value;
        const roleid = searchform.querySelector('select[name="role"]').value;
        if (search != oldsearch || roleid != oldroleid) {
            // Search form input changed. Load new confirm page.

            let searchloadingoverlay = searchform.querySelector('.overlay-icon-container');
            searchloadingoverlay.classList.remove('hidden');

            const courseid = searchform.querySelector('input[name="id"]').value;
            Fragment.loadFragment('local_rwth_participants_view', 'enrol_overview', contextid, {
                courseid: courseid,
                search: search,
                roleid: roleid
            }).done((html, js) => {
                searchform.classList.add('hidden');
                searchloadingoverlay.classList.add('hidden');

                // Search successful. Store search form input. We can use this when going back
                // to the search form and submitting it again to see if any form fields changed.
                oldsearch = search;
                oldroleid = roleid;

                let confirmform = document.getElementById('enrol-confirm-form');
                if (confirmform) {
                    Templates.replaceNode(confirmform, html, js);
                } else {
                    let tabPane = document.getElementById('addparticipants');
                    Templates.appendNodeContents(tabPane, html, js);
                }
                confirmform = document.getElementById('enrol-confirm-form');

                // When back button is clicked just show search form again.
                let back = confirmform.querySelector('input[name="back"]');
                back.addEventListener('click', (e) => {
                    e.preventDefault();
                    confirmform.classList.add('hidden');
                    searchform.classList.remove('hidden');
                    focusSearchField();
                });

                let confirm = confirmform.querySelector('input[name="confirm"]');
                confirm.addEventListener('click', (e) => {
                    e.preventDefault();

                    let confirmloadingoverlay = confirmform.querySelector('.overlay-icon-container');
                    confirmloadingoverlay.classList.remove('hidden');

                    let userids = [];
                    confirmform.querySelectorAll('input[type="checkbox"][name^="enrol"]:not(:disabled)')
                            .forEach((checkbox) => {
                        if (checkbox.checked) {
                            const userid = checkbox.getAttribute('name').split('_')[1];
                            userids.push(userid);
                        }
                    });
                    let emails = [];
                    confirmform.querySelectorAll('input[type="checkbox"][name^="invite"]:not(:disabled)').forEach((checkbox) => {
                        if (checkbox.checked) {
                            const email = checkbox.getAttribute('name').split(/_(.*)/)[1];
                            emails.push(email);
                        }
                    });
                    let enrolparams = {
                        courseid: courseid,
                        roleid: roleid,
                        userids: userids,
                        emails: emails,
                    };
                    const sendconfirmationcheckbox = confirmform.querySelector('input[name="sendenrolmentemails"]');
                    let sendconfirmation = true;
                    if (sendconfirmationcheckbox) {
                        sendconfirmation = sendconfirmationcheckbox.checked;
                        enrolparams.sendconfirmation = sendconfirmation;
                    }
                    Ajax.call([{
                        methodname: 'local_rwth_participants_view_add_users',
                        args: {params: enrolparams},
                        done: function() {
                            // Clear search form.
                            searchform.querySelector('textarea[name="usersearch"]').value = '';
                            next.setAttribute('disabled', 'disabled');
                            oldsearch = '';
                            oldroleid = -1;

                            // Show success notification.
                            const addedorinvited = userids.length + emails.length;
                            let requiredStrings = [];
                            if (addedorinvited == 1) {
                                requiredStrings.push({key: 'enrolusers:useraddedorinvited',
                                                component: 'local_rwth_participants_view'});
                                requiredStrings.push({key: 'enrolusers:bookingconfirmationsent',
                                                component: 'local_rwth_participants_view'});
                            } else {
                                requiredStrings.push({key: 'enrolusers:usersaddedorinvited',
                                                component: 'local_rwth_participants_view', param: addedorinvited});
                                requiredStrings.push({key: 'enrolusers:bookingconfirmationssent',
                                                component: 'local_rwth_participants_view'});
                            }
                            getStrings(requiredStrings).then(([usersadded, bookingconfirmationssent]) => {
                                let message = '<p>' + usersadded + '</p>';
                                if (sendconfirmation) {
                                    message += '<p>' + bookingconfirmationssent + '</p>';
                                }
                                return Notification.addNotification({
                                    message: message,
                                    type: "success"
                                });
                            }).catch(Notification.exception);

                            // Reload participants table.
                            let participantstable = document.getElementById('participants');
                            if (participantstable && userids.length > 0) {
                                Fragment.loadFragment('local_rwth_participants_view', 'participants_table', contextid, {
                                    courseid: courseid
                                }).done((html, js) => {
                                    Templates.replaceNodeContents(participantstable, html, js);
                                }).fail(Notification.exception);
                            }

                            // Reload invitations table.
                            let invitationstable = document.getElementById('invitations');
                            if (invitationstable && emails.length > 0) {
                                Fragment.loadFragment('local_rwth_participants_view', 'invitations_table', contextid, {
                                    courseid: courseid
                                }).done((html, js) => {
                                    Templates.replaceNodeContents(invitationstable, html, js);
                                }).fail(Notification.exception);
                            }

                            // Show search form.
                            confirmform.classList.add('hidden');
                            searchform.classList.remove('hidden');
                            focusSearchField();
                        },
                        fail: (ex) => {
                            confirmloadingoverlay.classList.add('hidden');
                            Notification.exception(ex);
                        },
                    }]);
                    M.core_formchangechecker.set_form_submitted();
                });

                // Disable submit button when nothing is checked.
                const query = 'input[type="checkbox"][name^="enrol"]:not(:disabled),'
                            + 'input[type="checkbox"][name^="invite"]:not(:disabled)';
                let checkboxes = confirmform.querySelectorAll(query);
                const updateConfirmButton = () => {
                    let anythingchecked = false;
                    checkboxes.forEach((checkbox) => {
                        if (checkbox.checked) {
                            anythingchecked = true;
                        }
                    });
                    if (anythingchecked) {
                        confirm.removeAttribute('disabled');
                    } else {
                        confirm.setAttribute('disabled', 'disabled');
                    }
                };
                checkboxes.forEach((checkbox) => {
                    checkbox.addEventListener('change', updateConfirmButton);
                });
                const $checkall = $('input[name=select_all]');
                $checkall.on('select-all-clicked', updateConfirmButton);
            }).fail((ex) => {
                searchloadingoverlay.classList.add('hidden');
                Notification.exception(ex);
            });
        } else {
            // Search form input didn't change. Show previously loaded confirm page.
            searchform.classList.add('hidden');
            let confirmform = document.getElementById('enrol-confirm-form');
            confirmform.classList.remove('hidden');
        }
    });
};
