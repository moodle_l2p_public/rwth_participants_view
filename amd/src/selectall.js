// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

import $ from 'jquery';

export const init = (tableid) => {
    const table = document.getElementById(tableid);
    const checkall = table.querySelector('input[name=select_all]');
    checkall.addEventListener('change', () => {
        const checked = checkall.checked;
        const query = 'input[type="checkbox"][name^="enrol"]:not(:disabled), input[type="checkbox"][name^="invite"]:not(:disabled)';
        let checkboxes = table.querySelectorAll(query);
        checkboxes.forEach(c => {
            c.checked = checked;
        });

        const event = $.Event('select-all-clicked');
        $(checkall).trigger(event);
    });
};