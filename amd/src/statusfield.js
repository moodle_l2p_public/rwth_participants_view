// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * AMD module for the user enrolment status field in the course participants page.
 *
 * @module     core_user/status_field
 * @copyright  2017 Jun Pataleta
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

import * as Str from 'core/str';
import ModalEvents from 'core/modal_events';
import ModalFactory from 'core/modal_factory';
import Templates from 'core/templates';

const Selectors = {
    table: '[data-region="participants-table"]',
    showDetails: '[data-action="showdetails"]',
    statusElement: '[data-status]',
};

const getStatusContainer = link => link.closest(Selectors.statusElement);

const registerEventListeners = () => {

    document.addEventListener('click', e => {
        const tableRoot = e.target.closest(Selectors.table);
        if (!tableRoot) {
            return;
        }

        const showDetailsLink = e.target.closest(Selectors.showDetails);
        if (showDetailsLink) {
            e.preventDefault();

            showStatusDetails(showDetailsLink);
        }
    });
};

const showStatusDetails = link => {
    const container = getStatusContainer(link);

    const context = {
        editenrollink: '',
        statusclass: container.querySelector('span.badge').getAttribute('class'),
        ...container.dataset,
    };
    // Find the edit enrolment link.
    const editEnrolLink = container.querySelector(Selectors.editEnrolment);
    if (editEnrolLink) {
        // If there's an edit enrolment link for this user, clone it into the context for the modal.
        context.editenrollink = editEnrolLink.outerHTML;
    }

    ModalFactory.create({
        large: true,
        type: ModalFactory.types.CANCEL,
        title: Str.get_string('enroldetails', 'enrol'),
        body: Templates.render('core_user/status_details', context),
    })
    .then(modal => {
        if (editEnrolLink) {
            modal.getRoot().on('click', Selectors.editEnrolment, e => {
                e.preventDefault();
                modal.hide();

                // Trigger click event for the edit enrolment link to show the edit enrolment modal.
                editEnrolLink.click();
            });
        }

        modal.show();

        // Handle hidden event.
        modal.getRoot().on(ModalEvents.hidden, () => modal.destroy());
        return modal;
    })
    .catch(Notification.exception);
};


export const init = () => {
    registerEventListeners();
};