<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\cache;

defined('MOODLE_INTERNAL') || die;

use cache;

class manager {

    const AREA_COURSE_PARTICIPANTS = 'course_participants';
    const AREA_COURSE_INVITATIONS = 'course_invitations';

    private $cache = null;
    private static $cachecache = []; // Another cache layer (in-memory).

    public function __construct(string $area) {
        $this->cache = cache::make('local_rwth_participants_view', $area);
    }

    public function get(string $key) {
        if (!array_key_exists($key, self::$cachecache)) {
            self::$cachecache[$key] = $this->cache->get($key);
        }
        return self::$cachecache[$key];
    }

    public function set(string $key, $value) {
        self::$cachecache[$key] = $value;
        $this->cache->set($key, $value);
    }

    public function delete(string $key) {
        unset(self::$cachecache[$key]);
        $this->cache->delete($key);
    }

}
