<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die;

use dataformat_csv;

/**
 * The only purpose of this class is to give you access to protected members of the class
 * dataformat_csv\writer.
 */
class csv_writer_exposed extends dataformat_csv\writer {

    /**
     * Returns the given field value of the dataformat_csv\writer.
     *
     * @param dataformat_csv\writer $writer    the csv writer
     * @param string                $fieldname name of the field
     * @return mixed a reference to the field value
     */
    public static function get_property(dataformat_csv\writer $writer, string $fieldname) {
        return $writer->$fieldname;
    }

}