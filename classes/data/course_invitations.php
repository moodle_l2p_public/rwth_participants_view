<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\data;

defined('MOODLE_INTERNAL') || die;

use stdClass;

use coding_exception;
use context_course;

use local_rwth_participants_view\cache;
use \tool_sdapi\sitedirectory_api;

class course_invitations {

    /**
     * @var int
     */
    private $courseid = null;

    /**
     * @var context_course
     */
    private $context = null;

    /**
     * @var manager
     */
    private $cache = null;

    /**
     * @var stdClass db entry for course
     */
    public $course = null;

    /**
     * @var array roleid => role
     */
    public $roles = null;

    /**
     * @var array (lang, roleid) => gui role name
     */
    public $role2guirolename = null;

    private function __construct(int $courseid) {
        global $DB;
        $this->courseid = $courseid;
        $this->context = context_course::instance($courseid, MUST_EXIST);
        $this->cache = new cache\manager(cache\manager::AREA_COURSE_INVITATIONS);
    }

    /**
     * Gets course invitations either fram cache or from the DB.
     */
    public static function get(int $courseid) {
        $parts = new course_invitations($courseid);
        if (!$parts->get_from_cache()) {
            // No valid data in cache => query data from DB.
            $parts->get_from_db();
            $parts->cache();
        }
        return $parts;
    }

    private function get_from_cache() {
        $cv = $this->cache->get(self::get_cache_key($this->courseid));
        if ($cv === false) {
            // Cache miss.
            return false;
        }
        $this->course = $cv->course;
        $this->roles = $cv->roles;
        $this->invitations = $cv->invitations;
        $this->db_load_gui_role_names(); // Always load rolenames new.

        return true;
    }

    private function cache() {
        $parts = new stdClass();
        $parts->course = $this->course;
        $parts->roles = $this->roles;
        $parts->invitations = $this->invitations;

        $this->cache->set(self::get_cache_key($this->courseid), $parts);
    }

    private static function get_cache_key(int $courseid) {
        return $courseid."_invitations";
    }

    public static function clear_cache(int $courseid) {
        $key = self::get_cache_key($courseid);
        $cache = new cache\manager(cache\manager::AREA_COURSE_INVITATIONS);
        $cache->delete($key);
    }

    private function get_from_db() {
        $this->db_load_course();
        $this->db_load_roles();
        $this->db_load_gui_role_names();  // Must be loaded after roles.
        $this->db_load_user_invitations();
    }

    private function db_load_course() {
        global $DB;
        $this->course = $DB->get_record('course', array('id' => $this->courseid), '*', MUST_EXIST);
    }

    private function db_load_roles() {
        global $DB;

        $sql = "SELECT r.id, r.name, r.shortname, r.description, r.sortorder, r.archetype
                FROM {role} r
                ORDER BY r.sortorder ASC";
        $dbresult = $DB->get_recordset_sql($sql);
        if (!$dbresult) {
            $this->roles = null;
            return;
        }

        $this->roles = array();
        foreach ($dbresult as $role) {
            $this->roles[$role->id] = $role;
        }
    }

    private function db_load_gui_role_names() {
        static $guirolescache = [];
        $courseid = $this->course->id;
        if (!array_key_exists($courseid, $guirolescache)) {
            $guirolescache[$courseid] = \role_fix_names($this->roles, $this->context, ROLENAME_ALIAS, true);
        }
        $this->role2guirolename = $guirolescache[$courseid];
    }

    private function db_load_user_invitations() {
        $sdapi = new sitedirectory_api();
        $result = $sdapi->get_invitaions_for_course($this->course->id);

        $this->invitations = [];
        foreach ($result as $invitation) {
            $this->invitations[$invitation->id] = $invitation;
        }
    }
}
