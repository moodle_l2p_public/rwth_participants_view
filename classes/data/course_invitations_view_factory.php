<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\data;

defined('MOODLE_INTERNAL') || die;

use local_rwth_participants_view\role_helper;
use tool_sdapi\sitedirectory_api;

class course_invitations_view_factory {

    const SORT_COL_EMAIL = 'email';
    const SORT_COL_ROLE = 'role';
    const SORT_COL_INVITESTATUS = 'status';
    const SORT_COL_CREATEDBY = 'createdby';

    const SORT_DIR_ASC = 0;
    const SORT_DIR_DESC = 1;

    const LIMIT_NONE = null;

    private $courseid = -1;
    private $parts = null;

    private $filters = array();

    private $sortcolumn = self::SORT_COL_EMAIL;
    private $sortdirection = self::SORT_DIR_ASC;

    public function __construct(int $courseid) {
        $this->courseid = $courseid;
        $this->parts = course_invitations::get($courseid);
    }

    /**
     * [chainable]
     */
    public function add_filter(invitations_filter $filter): course_invitations_view_factory {
        $this->filters[] = $filter;
        return $this;
    }

    /**
     * [chainable]
     */
    public function remove_filter(invitations_filter $filter): course_invitations_view_factory {
        $key = array_search($filter, $this->filters, true);
        if ($key !== false) {
            unset($this->filters[$key]);
        }
        return $this;
    }

    /**
     * [chainable]
     */
    public function remove_filters(): course_invitations_view_factory {
        $this->filters = array();
        return $this;
    }

    /**
     * [chainable]
     */
    public function set_sort_column(string $col): course_invitations_view_factory {
        $this->sortcolumn = $col;
        return $this;
    }

    /**
     * [chainable]
     */
    public function set_sort_direction(int $dir): course_invitations_view_factory {
        $this->sortdirection = $dir;
        return $this;
    }

    public function create(): array {
        $parts = $this->parts;

        // Apply filters.
        foreach ($this->filters as $filter) {
            $filter->apply($parts);
        }

        // Sort.
        $this->sort($parts);

        // Aggregate groups and roles.
        $invitations = $this->aggregate($parts);

        return $invitations;
    }

    private function sort(course_invitations &$parts) {

        // Determine new order.
        $sort = array();
        foreach ($parts->invitations as $iid => $invitation) {
            $sortvalues = $this->get_invitation_sort_column_values($invitation, $parts);
            $sort[$iid] = [];
            if (empty($sortvalues)) {
                continue;
            }
            foreach ($sortvalues as $sortvalue) {
                $sortvalue = mb_strtolower($sortvalue); // Capitalization should not be important for sort.
                $sortvalue = iconv("UTF-8", "ASCII//TRANSLIT", $sortvalue); // Transliterate umlauts for sort.
                $sort[$iid][] = $sortvalue;
            }
        }
        uasort($sort, function($a, $b) {

            // Sort by amount of fields (does this ever happen?).
            if (count($a) != count($b)) {
                return $this->sortdirection === self::SORT_DIR_ASC
                        ? count($a) - count($b)
                        : count($b) - count($a);
            }

            // Sort by column values.
            for ($i = 0; $i < count($a); $i++) {
                if ($a[$i] != $b[$i]) {
                    return $this->sortdirection === self::SORT_DIR_ASC
                            ? strnatcmp($a[$i], $b[$i])
                            : strnatcmp($b[$i], $a[$i]);
                }
            }
            return 0;
        });

        // Rearrange.
        $sortedinvitations = array();
        foreach ($sort as $iid => $unused) {
            $sortedinvitations[$iid] = $parts->invitations[$iid];
        }
        $parts->invitations = $sortedinvitations;
    }

    private function get_invitation_sort_column_values($invitation, $parts) {
        $col = $this->sortcolumn;

        switch ($col) {
            case 'email':
                return [$invitation->externalid];
            case 'role':
                return [role_helper::get_rolename($invitation->roleid, $this->courseid)];
            case 'status':
                return [$invitation->status, $invitation->expires ?? null];
            case 'createdby':
                return [$invitation->createdby];
        }
        return [$invitation->$col];
    }

    private function aggregate(course_invitations $in): array {
        $out = array();
        foreach ($in->invitations as $invitation) {
            $v = new course_invitation_view();
            $v->id = $invitation->id;
            $v->couponcode = $invitation->couponcode;
            $v->email = $invitation->externalid;
            $v->rolename = role_helper::get_rolename($invitation->roleid, $this->courseid);
            $v->createdby = $invitation->createdby;
            $v->courseid = $invitation->moodlecourseid;
            $v->status = $invitation->status;
            if ($v->status == sitedirectory_api::INVITATION_STATUS_OPEN) {
                $v->expires = $invitation->expires;
            }
            $out[$invitation->id] = $v;
        }
        return $out;
    }
}

