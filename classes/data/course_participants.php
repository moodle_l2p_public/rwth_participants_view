<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\data;

defined('MOODLE_INTERNAL') || die;

use stdClass;

use coding_exception;
use context_course;

use local_rwth_participants_view\cache;

class course_participants {

    /**
     * @var int
     */
    private $courseid = null;

    /**
     * @var context_course
     */
    private $context = null;

    /**
     * @var manager
     */
    private $cache = null;

    /**
     * @var stdClass db entry for course
     */
    public $course = null;

    /**
     * @var array enrolmentid => enrolment
     */
    public $enrolments = null;

    /**
     * @var array userid => user
     */
    public $users = null;

    /**
     * @var array groupid => group
     */
    public $groups = null;

    /**
     * @var array roleid => role
     */
    public $roles = null;

    /**
     * Custom user profile fields.
     *
     * @var array fieldshortname => field
     */
    public $fields = null;

    /**
     * @var array userid => array of (enrolmentid => user_enrolment)
     */
    public $user2enrolment = null;

    /**
     * @var array userid => array of group ids
     */
    public $user2group = null;

    /**
     * @var array groupid => array of user ids
     */
    public $group2user = null;

    /**
     * @var array user id => array of (object with attributes 'roleid' and 'component')
     */
    public $user2role = null;

    /**
     * @var array roleid => array of (object with attributes 'userid' and 'component')
     */
    public $role2user = null;

    /**
     * @var array (userid, fieldid) => field data
     */
    public $user2fielddata = null;

    /**
     * @var array (lang, roleid) => gui role name
     */
    public $role2guirolename = null;

    private function __construct(int $courseid) {
        global $DB;
        $this->courseid = $courseid;
        $this->context = context_course::instance($courseid, MUST_EXIST);
        $this->cache = new cache\manager(cache\manager::AREA_COURSE_PARTICIPANTS);
    }

    /**
     * Gets course participants either fram cache or from the DB.
     */
    public static function get(int $courseid) {
        $parts = new course_participants($courseid);
        if (!$parts->get_from_cache()) {
            // No valid data in cache => query data from DB.
            $parts->get_from_db();
            $parts->cache();
        }
        return $parts;
    }

    private function get_from_cache() {
        $cv = $this->cache->get(self::get_cache_key($this->courseid));
        if ($cv === false) {
            // Cache miss.
            return false;
        }
        $this->course = $cv->course;
        $this->enrolments = $cv->enrolments;
        $this->users = $cv->users;
        $this->groups = $cv->groups;
        $this->roles = $cv->roles;
        $this->fields = $cv->fields;
        $this->user2enrolment = $cv->user2enrolment;
        $this->user2group = $cv->user2group;
        $this->group2user = $cv->group2user;
        $this->user2role = $cv->user2role;
        $this->role2user = $cv->role2user;
        $this->user2fielddata = $cv->user2fielddata;
        $this->db_load_gui_role_names(); // Always load rolenames new.
        return true;
    }

    private function cache() {
        $parts = new stdClass();
        $parts->course = $this->course;
        $parts->enrolments = $this->enrolments;
        $parts->users = $this->users;
        $parts->groups = $this->groups;
        $parts->roles = $this->roles;
        $parts->fields = $this->fields;
        $parts->user2enrolment = $this->user2enrolment;
        $parts->user2group = $this->user2group;
        $parts->group2user = $this->group2user;
        $parts->user2role = $this->user2role;
        $parts->role2user = $this->role2user;
        $parts->user2fielddata = $this->user2fielddata;

        $this->cache->set(self::get_cache_key($this->courseid), $parts);
    }

    private static function get_cache_key(int $courseid) {
        return $courseid;
    }

    public static function clear_cache(int $courseid) {
        $key = self::get_cache_key($courseid);
        $cache = new cache\manager(cache\manager::AREA_COURSE_PARTICIPANTS);
        $cache->delete($key);
    }

    private function get_from_db() {
        $this->db_load_course();
        $this->db_load_enrolments();
        $this->db_load_user_enrolments(); // Must be loaded after enrolments.
        $this->db_load_users();           // Must be loaded after user enrolments.
        $this->db_load_groups();
        $this->db_load_roles();
        $this->db_load_fields();
        $this->db_load_group_members();   // Must be loaded after users and groups.
        $this->db_load_role_users();
        $this->db_load_field_data();      // Must be loaded after users.
        $this->db_load_gui_role_names();  // Must be loaded after roles.
    }

    private function db_load_course() {
        global $DB;
        $this->course = $DB->get_record('course', array('id' => $this->courseid), '*', MUST_EXIST);
    }

    private function db_load_enrolments() {
        global $DB;
        $sql = "SELECT e.*
            FROM {enrol} e
                WHERE e.status = :enabled AND e.courseid = :courseid";
        $params = array();
        $params['enabled'] = ENROL_INSTANCE_ENABLED;
        $params['courseid'] = $this->courseid;
        $dbresult = $DB->get_recordset_sql($sql, $params);
        if (!$dbresult) {
            $this->enrolments = null;
            return;
        }

        $this->enrolments = array();
        foreach ($dbresult as $enrolment) {
            $this->enrolments[$enrolment->id] = $enrolment;
        }
    }

    private function db_load_user_enrolments() {
        global $DB;
        if (is_null($this->enrolments)) {
            throw new coding_exception('Database query is missing input values. (Did you call it in the wrong order?)');
        }
        if (empty($this->enrolments)) {
            // No users enrolled in the course yet.
            $this->user2enrolment = [];
            return;
        }
        $enrolmentids = array_keys($this->enrolments);
        $selfenrolsql = "SELECT DISTINCT enrol FROM {enrol}
                        WHERE courseid = :courseid AND id IN (".implode(', ', $enrolmentids).")";
        $selfenrolparams = array('courseid' => $this->courseid);
        $dbselfenrolresult = $DB->get_records_sql($selfenrolsql, $selfenrolparams);
        if (in_array('self', array_keys($dbselfenrolresult))) {
            $sql = "SELECT ue.*
                FROM {user_enrolments} ue
                WHERE ue.enrolid IN (".implode(', ', $enrolmentids).")";
            $dbresult = $DB->get_recordset_sql($sql);
        } else {
            $sql = "SELECT ue.*
                FROM {user_enrolments} ue
                WHERE ue.status = :active AND ue.enrolid IN (".implode(', ', $enrolmentids).")";
            $params = array('active' => ENROL_USER_ACTIVE);
            $dbresult = $DB->get_recordset_sql($sql, $params);
        }
        if (!$dbresult) {
            $this->user2enrolment = null;
            return;
        }

        $this->user2enrolment = array();
        foreach ($dbresult as $ue) {
            $uid = $ue->userid;
            if (!isset($this->user2enrolment[$uid])) {
                $this->user2enrolment[$uid] = array();
            }
            $this->user2enrolment[$uid][$ue->enrolid] = $ue;
        }
    }

    private function db_load_users() {
        global $DB;
        /*
         * We need to query all fields in user_picture::fields (lib/outputcomponents.php).
         * Otherwise the user_picture class will query them from the DB every time a user
         * picture is rendered. That would defeat the purpose of caching this user data.
         */
        if (is_null($this->user2enrolment)) {
            throw new coding_exception('Database query is missing input values. (Did you call it in the wrong order?)');
        }
        $userids = array_keys($this->user2enrolment);
        if (empty($userids)) {
            $this->users = [];
            return;
        }
        $sql = "SELECT u.id, u.firstname, u.lastname,
                    u.firstnamephonetic, u.lastnamephonetic,
                    u.middlename, u.alternatename,
                    u.email, u.idnumber, u.maildisplay,
                    u.picture, u.imagealt
                FROM {user} u
                WHERE u.id IN (".implode(', ', $userids).")
                    and (u.firstname NOT LIKE \"anonfirstname%\" and u.lastname NOT LIKE \"anonlastname%\")";
        $dbresult = $DB->get_recordset_sql($sql);
        if (!$dbresult) {
            $this->users = null;
            return;
        }

        $this->users = array();
        foreach ($dbresult as $user) {
            $this->users[$user->id] = $user;
        }
    }

    private function db_load_groups() {
        global $DB;

        $sql = "SELECT g.id, g.idnumber, g.name, g.description, g.descriptionformat, g.picture
                FROM {groups} g
                WHERE g.courseid = :courseid";
        $params = array('courseid' => $this->courseid);
        $dbresult = $DB->get_recordset_sql($sql, $params);
        if (!$dbresult) {
            $this->groups = null;
            return;
        }

        $this->groups = array();
        foreach ($dbresult as $group) {
            $this->groups[$group->id] = $group;
        }
    }

    private function db_load_roles() {
        global $DB;

        $sql = "SELECT r.id, r.name, r.shortname, r.description, r.sortorder, r.archetype
                FROM {role} r
                ORDER BY r.sortorder ASC";
        $dbresult = $DB->get_recordset_sql($sql);
        if (!$dbresult) {
            $this->roles = null;
            return;
        }

        $this->roles = array();
        foreach ($dbresult as $role) {
            $this->roles[$role->id] = $role;
        }
    }

    private function db_load_fields() {
        global $DB;

        $sql = "SELECT f.id, f.shortname, f.name, f.datatype, f.description, f.descriptionformat
                FROM {user_info_field} f";
        $dbresult = $DB->get_recordset_sql($sql);
        if (!$dbresult) {
            $this->fields = null;
            return;
        }

        $this->fields = array();
        foreach ($dbresult as $field) {
            $this->fields[$field->shortname] = $field;
        }
    }

    private function db_load_group_members() {
        global $DB;
        if (empty($this->groups) || empty($this->users)) {
            return;
        }

        if (is_null($this->users) || is_null($this->groups)) {
            throw new coding_exception('Database query is missing input values. (Did you call it in the wrong order?)');
        }
        $userids = array_keys($this->users);
        $groupids = array_keys($this->groups);
        if (empty($groupids)) {
            return; // No groups in this course.
        }
        $sql = "SELECT gm.userid, gm.groupid
                FROM {groups_members} gm
                WHERE gm.userid IN (".implode(', ', $userids).")
                    AND gm.groupid IN (".implode(', ', $groupids).")";
        $dbresult = $DB->get_recordset_sql($sql);
        if (!$dbresult) {
            $this->user2group = null;
            $this->group2user = null;
            return;
        }

        $this->user2group = array();
        $this->group2user = array();
        foreach ($dbresult as $gm) {
            $uid = $gm->userid;
            $gid = $gm->groupid;
            if (!isset($this->user2group[$uid])) {
                $this->user2group[$uid] = array();
            }
            if (!isset($this->group2user[$gid])) {
                $this->group2user[$gid] = array();
            }
            $this->user2group[$uid][] = $gid;
            $this->group2user[$gid][] = $uid;
        }
    }

    private function db_load_role_users() {
        global $DB;

        $sql = "SELECT ra.roleid, ra.userid, ra.component
                FROM {role_assignments} ra
                WHERE ra.contextid = :contextid";
        $params = array('contextid' => $this->context->id);
        $dbresult = $DB->get_recordset_sql($sql, $params);
        if (!$dbresult) {
            $this->user2role = null;
            $this->role2user = null;
            return;
        }

        $this->role2user = array();
        foreach ($dbresult as $ra) {
            $uid = $ra->userid;
            $rid = $ra->roleid;
            $component = $ra->component;
            if (!isset($this->role2user[$rid])) {
                $this->role2user[$rid] = array();
            }
            if (!isset($this->user2role[$uid])) {
                $this->user2role[$uid] = array();
            }
            $this->role2user[$rid][] = (object)['userid' => $uid, 'component' => $component];
            $this->user2role[$uid][] = (object)['roleid' => $rid, 'component' => $component];
        }
    }

    private function db_load_field_data() {
        global $DB;

        if (is_null($this->users)) {
            throw new coding_exception('Database query is missing input values. (Did you call it in the wrong order?)');
        }
        $userids = array_keys($this->users);
        if (empty($userids)) {
            $this->user2fielddata = [];
            return;
        }
        $sql = "SELECT fd.fieldid, fd.userid, fd.data, fd.dataformat
                FROM {user_info_data} fd
                WHERE fd.userid IN (".implode(', ', $userids).")";
        $dbresult = $DB->get_recordset_sql($sql);
        if (!$dbresult) {
            $this->user2fielddata = null;
            return;
        }

        $this->user2fielddata = array();
        foreach ($dbresult as $fd) {
            $fid = $fd->fieldid;
            $uid = $fd->userid;
            if (!isset($this->user2fielddata[$uid])) {
                $this->user2fielddata[$uid] = array();
            }
            $this->user2fielddata[$uid][$fid] = new stdClass();
            $this->user2fielddata[$uid][$fid]->data = $fd->data;
            $this->user2fielddata[$uid][$fid]->dataformat = $fd->dataformat;
        }
    }

    private function db_load_gui_role_names() {
        static $guirolescache = [];
        $courseid = $this->course->id;
        if (!array_key_exists($courseid, $guirolescache)) {
            $guirolescache[$courseid] = \role_fix_names($this->roles, $this->context, ROLENAME_ALIAS, true);
        }
        $this->role2guirolename = $guirolescache[$courseid];
    }
}