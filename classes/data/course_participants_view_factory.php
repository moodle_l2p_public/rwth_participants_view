<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\data;

defined('MOODLE_INTERNAL') || die;

use local_rwth_participants_view\hiddentutor_helper;
use local_rwth_participants_view\maildisplay_helper;
use local_rwth_participants_view\role_helper;

class course_participants_view_factory {

    const SORT_COL_FIRSTNAME = 'firstname';
    const SORT_COL_LASTNAME = 'lastname';
    const SORT_COL_EMAIL = 'email';
    const SORT_COL_ROLE = 'role';
    const SORT_COL_GROUP = 'group';
    const SORT_COL_ENROLTIME = 'enroltime';

    const SORT_DIR_ASC = 0;
    const SORT_DIR_DESC = 1;

    const LIMIT_NONE = null;

    private $courseid = -1;
    private $parts = null;

    private $filters = array();

    private $sortcolumn = self::SORT_COL_LASTNAME;
    private $sortdirection = self::SORT_DIR_ASC;

    private $offset = 0;
    private $limit = self::LIMIT_NONE;

    public function __construct(int $courseid) {
        $this->courseid = $courseid;
        $this->parts = course_participants::get($courseid);
    }

    /**
     * [chainable]
     */
    public function add_filter(filter $filter): course_participants_view_factory {
        $this->filters[] = $filter;
        return $this;
    }

    /**
     * [chainable]
     */
    public function remove_filter(filter $filter): course_participants_view_factory {
        $key = array_search($filter, $this->filters, true);
        if ($key !== false) {
            unset($this->filters[$key]);
        }
        return $this;
    }

    /**
     * [chainable]
     */
    public function remove_filters(): course_participants_view_factory {
        $this->filters = array();
        return $this;
    }

    /**
     * [chainable]
     */
    public function set_sort_column(string $col): course_participants_view_factory {
        $this->sortcolumn = $col;
        return $this;
    }

    /**
     * [chainable]
     */
    public function set_sort_direction(int $dir): course_participants_view_factory {
        $this->sortdirection = $dir;
        return $this;
    }

    /**
     * [chainable]
     */
    public function set_offset(int $offset) {
        $this->offset = $offset;
        return $this;
    }

    /**
     * [chainable]
     */
    public function set_limit($limit = self::LIMIT_NONE) {
        $this->limit = $limit;
        return $this;
    }

    public function create(): array {
        $parts = $this->parts;

        // Apply filters.
        foreach ($this->filters as $filter) {
            $filter->apply($parts);
        }

        // Sort.
        $this->sort($parts);

        // Limit.
        $this->limit($parts);

        // Aggregate groups and roles.
        $users = $this->aggregate($parts);

        return $users;
    }

    private function sort(course_participants &$parts) {

        // Determine new order.
        $sort = array();
        foreach ($parts->users as $uid => $user) {
            $sortvalues = $this->get_user_sort_column_values($user, $parts);
            $sort[$uid] = [];
            if (empty($sortvalues)) {
                continue;
            }
            foreach ($sortvalues as $sortvalue) {
                $sortvalue = mb_strtolower($sortvalue); // Capitalization should not be important for sort.
                $sortvalue = iconv("UTF-8", "ASCII//TRANSLIT", $sortvalue); // Transliterate umlauts for sort.
                $sort[$uid][] = $sortvalue;
            }
        }
        uasort($sort, function($a, $b) {

            // Make sure hidden users are always shown last.
            if (empty($a) && !empty($b)) {
                return 1;
            } else if (empty($b) && !empty($a)) {
                return -1;
            }

            // Sort by amount of fields (does this ever happen?).
            if (count($a) != count($b)) {
                return $this->sortdirection === self::SORT_DIR_ASC
                        ? count($a) - count($b)
                        : count($b) - count($a);
            }

            // Sort by column values.
            for ($i = 0; $i < count($a); $i++) {
                if ($a[$i] != $b[$i]) {
                    return $this->sortdirection === self::SORT_DIR_ASC
                            ? strcmp($a[$i], $b[$i])
                            : strcmp($b[$i], $a[$i]);
                }
            }
            return 0;
        });

        // Rearrange.
        $sortedusers = array();
        foreach ($sort as $uid => $unused) {
            $sortedusers[$uid] = $parts->users[$uid];
        }
        $parts->users = $sortedusers;
    }

    private function get_user_sort_column_values($user, $parts) {
        $col = $this->sortcolumn;

        $affectedbymaildisplay = $col === 'email';
        if (get_config('rwth_participants_view', 'emaildisplaycontrolsnamedisplay')) {
            $affectedbymaildisplay |= $col === 'firstname' || $col === 'lastname';
        }
        if ($affectedbymaildisplay) {
            // Special case because students can decide to hide their own name/email address towards other students.
            if (maildisplay_helper::hide_mail($parts, $user->id)) {
                // Name/Email address are hidden.
                return '';
            }
        }

        if (hiddentutor_helper::hide_tutor($parts, $user->id)
            && ($col === 'email' || $col === 'firsname' || $col === 'lastname')) {
                return '';
        }

        // When ordering by firstname, order by lastname second and vice versa.
        switch ($col) {
            case 'firstname':
                return [$user->firstname, $user->lastname];
            case 'lastname':
                return [$user->lastname, $user->firstname];
            case 'role':
                $user2role = $parts->user2role;
                if (empty($user2role[$user->id])) {
                    return [""];
                }
                $firstroleid = $user2role[$user->id][0]->roleid;
                $firstrolename = role_helper::get_rolename($firstroleid, $this->courseid);
                return [$firstrolename];
            case 'group':
                $user2group = $parts->user2group;
                if (empty($user2group[$user->id])) {
                    return [""];
                }
                $groups = [];
                foreach ($user2group[$user->id] as $group) {
                    $groups[] = groups_get_group_name((int)$group);
                }
                asort($groups, SORT_NATURAL);
                return [reset($groups)];
            case 'enroltime':
                // When ordering by enroltime, a single user may be enrolled by different means at different times so we select his first enrolment.
                $enrolments = array();
                foreach ($this->parts->user2enrolment[$user->id] as $ue) {
                    $enrolments[] = $ue->timecreated;
                }
                return [min($enrolments)];
        }
        return [$user->$col];
    }

    private function limit(course_participants &$parts) {
        if ($this->limit === self::LIMIT_NONE) {
            return;
        }
        $parts->users = array_slice($parts->users, $this->offset, $this->limit, true);
    }

    private function aggregate(course_participants $in): array {
        $out = array();
        foreach ($in->users as $user) {
            $v = new course_participant_view();
            $v->id = $user->id;
            $v->picture = $user->picture;
            $v->firstname = $user->firstname;
            $v->lastname = $user->lastname;
            $v->email = $user->email;
            $v->idnumber = $user->idnumber;
            // foreach ($user as $prop => $val) {
            //     $v->$prop = $val;
            // }
            $this->aggregate_user($v, $user->id, $in);
            $out[$user->id] = $v;
        }
        return $out;
    }

    private function aggregate_user(course_participant_view &$v, int $userid, course_participants $parts): course_participant_view {

        // Display of name/email address.
        $v->mailhidden = maildisplay_helper::hide_mail($parts, $userid);
        $v->tutorhidden = hiddentutor_helper::hide_tutor($parts, $userid);

        // Roles.
        if ($parts->user2role != null && array_key_exists($userid, $parts->user2role)) {
            $v->roles = array();
            foreach ($parts->user2role[$userid] as $ra) {
                $rid = $ra->roleid;
                if (!array_key_exists($rid, $parts->roles)) {
                    // There is a role assignment for a role that no longer exists. This can happen after restoring
                    // an old backup containing user data.
                    continue;
                }
                $v->roles[$rid] = clone $parts->roles[$rid];
                $v->roles[$rid]->assigning_component = $ra->component;
            }
        }

        // Groups.
        if (!is_null($parts->user2group) && array_key_exists($userid, $parts->user2group)) {
            $groupids = $parts->user2group[$userid];
            foreach ($groupids as $gid) {
                $v->groups[$gid] = $parts->groups[$gid];
            }
        }

        // Enrolments.
        if (!is_null($parts->user2enrolment) && array_key_exists($userid, $parts->user2enrolment)) {
            $ues = $parts->user2enrolment[$userid];
            $v->userenrolments = $ues;
            foreach ($ues as $ue) {
                $eid = $ue->enrolid;
                if (array_key_exists($eid, $parts->enrolments) &&
                        !array_key_exists($eid, $v->enrolments)) {
                    $v->enrolments[$eid] = $parts->enrolments[$eid];
                }
            }
        }

        return $v;
    }
}

