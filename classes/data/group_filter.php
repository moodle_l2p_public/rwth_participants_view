<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\data;

defined('MOODLE_INTERNAL') || die;

use context;

class group_filter implements filter {

    use userids_filter_trait;

    private $groupid;
    private $aag;

    public function __construct(int $groupid, context $context) {
        $this->groupid = $groupid;
        $this->aag = has_capability('moodle/site:accessallgroups', $context);
    }

    public function apply(course_participants &$parts) {
        $userids = [];
        if (get_config('rwth_participants_view', 'restrictgroupfiltertostudents') && !$this->aag) {

            // Filter only students by group if viewed by a student.
            $studentroleids = explode(',', get_config('rwth_participants_view', 'student_roles'));
            $studentids = [];
            foreach ($studentroleids as $rid) {
                $useridsforrole = array_column($parts->role2user[$rid] ?? [], 'userid');
                $studentids = array_merge($studentids, $useridsforrole);
            }
            $studentids = array_unique($studentids);
            // For students that also have a non-student role, don't treat them as students.
            foreach ($studentids as $idx => $studentid) {
                $roleids = array_column($parts->user2role[$studentid] ?? [], 'roleid');
                foreach ($roleids as $rid) {
                    if (!in_array($rid, $studentroleids)) {
                        unset($studentids[$idx]);
                    }
                }
            }
            $groupmemberids = $parts->group2user[$this->groupid] ?? [];
            $studentgroupmemberids = array_intersect($studentids, $groupmemberids);

            $alluserids = array_keys($parts->users);
            $nonstudentids = array_diff($alluserids, $studentids);

            $userids = array_merge($nonstudentids, $studentgroupmemberids);
        } else {

            // Filter all users by group.
            $userids = $parts->group2user[$this->groupid] ?? [];
        }
        $this->filter_by_userids($userids, $parts);
    }
}
