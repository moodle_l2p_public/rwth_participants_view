<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\data;

defined('MOODLE_INTERNAL') || die;

use SplObjectStorage;

use coding_exception;
use local_rwth_participants_view\hiddentutor_helper;
use local_rwth_participants_view\maildisplay_helper;

class initial_searcher {

    const CRITERION_FIRSTNAME = 0;
    const CRITERION_LASTNAME = 1;

    private $initials = array();

    /**
     * @param array $users array of course_participant_view
     */
    public function __construct(course_participants $parts) {
        $criterion2property = array(self::CRITERION_FIRSTNAME => 'firstname',
                self::CRITERION_LASTNAME => 'lastname');
        foreach ($criterion2property as $criterion => $property) {
            $this->initials[$criterion] = array();
        }
        foreach ($parts->users as $user) {
            foreach ($criterion2property as $criterion => $property) {

                // Don't count users whose name we can't see when looking for which initials exist. Otherwise
                // we might be able to determine these users' names.
                if (get_config('rwth_participants_view', 'emaildisplaycontrolsnamedisplay')
                        && maildisplay_helper::hide_mail($parts, $user->id)) {
                    // Not a bug. We (ab)use the maildisplay setting to control display of names aswell.
                    continue;
                }
                if (hiddentutor_helper::hide_tutor($parts, $user->id)) {
                    continue;
                }

                $value = $user->$property;
                if (empty($value)) {
                    continue;
                }
                // Transliterate non-ascii characters, e.g. umlauts.
                $value = iconv("UTF-8", "ASCII//TRANSLIT", $value);
                $initial = mb_strtoupper(mb_substr($value, 0, 1));
                $this->initials[$criterion][$initial] = true;
            }
        }
    }

    /**
     * @param string $initial initial to check for
     * @param int $criterion which user field to check
     * @see CRITERION_FIRSTNAME
     * @see CRITERION_LASTNAME
     */
    public function users_with_initial_exist(string $initial, int $criterion): bool {
        if (!array_key_exists($criterion, $this->initials)) {
            throw new coding_exception("Unknown criterion $criterion for initials.");
        };
        $initial = strtoupper($initial);
        return array_key_exists($initial, $this->initials[$criterion]);
    }

    public function users_with_initials_exist(array $initials, int $criterion): array {
        $result = array();
        foreach ($initials as $initial) {
            $result[$initial] = $this->users_with_initial_exist($initial, $criterion);
        }
        return $result;
    }
}