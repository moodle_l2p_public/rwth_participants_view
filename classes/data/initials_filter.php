<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\data;

defined('MOODLE_INTERNAL') || die;

use moodle_exception;

use local_rwth_participants_view\maildisplay_helper;
use local_rwth_participants_view\hiddentutor_helper;

class initials_filter implements filter {

    use userids_filter_trait;

    private $ifirst = '';
    private $ilast = '';

    private $initials = array();
    private $ifirstenabled = null;
    private $ilastenabled = null;

    /**
     * @param string $ifirst            initial of first name
     * @param string $ilast             initial of last name
     */
    public function __construct(string $ifirst, string $ilast) {
        $this->ifirst = $ifirst;
        $this->ilast = $ilast;
    }

    /**
     * Makes it so when this filter is applied it also counts for each given initial whether
     * a user exists whose first/last name begins with it.
     * @param array  $initials all initials to check
     * @see get_users_existing_per_initial_firstname
     * @see get_users_existing_per_initial_lastname
     */
    public function enable_search_users_existing_per_initial(array $initials) {
        $this->initials = $initials;
    }

    public function apply(course_participants &$parts) {
        $userids = array();
        foreach ($parts->users as $uid => $user) {

            // If we search by a certain initial, filter out all users whose name we can't see. Otherwise
            // we might be able to guess their name.
            if (!empty($this->ifirst) || !empty($this->ilast)) {
                // Searching by a ceratin initial.
                if (get_config('rwth_participants_view', 'emaildisplaycontrolsnamedisplay')
                        && maildisplay_helper::hide_mail($parts, $uid)) {
                    // Not a bug. We (ab)use the maildisplay setting to control display of names aswell.
                    continue;
                }

                if (hiddentutor_helper::hide_tutor($parts, $user->id)) {
                    continue;
                }
            }

            // Transliterate non-ascii characters, e.g. umlauts.
            $firstname = iconv("UTF-8", "ASCII//TRANSLIT", $user->firstname);
            $lastname = iconv("UTF-8", "ASCII//TRANSLIT", $user->lastname);
            if (!empty($this->ifirst) && stripos($firstname, $this->ifirst) !== 0) {
                // Initial of first name didn't match.
                continue;
            }
            if (!empty($this->ilast) && stripos($lastname, $this->ilast) !== 0) {
                // Initial of last name didn't match.
                continue;
            }
            $userids[] = $uid;
        }
        $this->filter_by_userids($userids, $parts);

        $this->search_existing_user_per_initial($parts);
    }

    private function search_existing_user_per_initial(course_participants $parts) {
        $initialsearcher = new initial_searcher($parts);
        $this->ifirstenabled = $initialsearcher->users_with_initials_exist($this->initials,
                initial_searcher::CRITERION_FIRSTNAME);
        $this->ilastenabled = $initialsearcher->users_with_initials_exist($this->initials,
                initial_searcher::CRITERION_LASTNAME);
    }

    /**
     * @return array initial => whether at least one user exists whose first name start with
     *               this initial
     */
    public function get_users_existing_per_initial_firstname(): array {
        return $this->ifirstenabled;
    }

    /**
     * @return array initial => whether at least one user exists whose first name start with
     *               this initial
     */
    public function get_users_existing_per_initial_lastname(): array {
        return $this->ilastenabled;
    }
}