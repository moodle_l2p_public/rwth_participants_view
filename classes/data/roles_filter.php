<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\data;

defined('MOODLE_INTERNAL') || die;

class roles_filter implements filter {

    use userids_filter_trait;

    private $roles;

    public function __construct(array $roles) {
        $this->roles = $roles;
    }

    public function apply(course_participants &$parts) {
        $userids = [];
        foreach ($this->roles as $role) {
            if (array_key_exists($role->id, $parts->role2user)) {
                $userids = array_merge($userids, array_column($parts->role2user[$role->id], 'userid'));
            }
        }
        $this->filter_by_userids($userids, $parts);
    }
}
