<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\data;

defined('MOODLE_INTERNAL') || die;

use stdClass;

use core_user;
use moodle_exception;

use local_rwth_participants_view\maildisplay_helper;
use local_rwth_participants_view\hiddentutor_helper;

/**
 * Filters users by a given search term.
 *
 * Each word in the search term must match at least one of these fields: first name, last name, email address,
 * matriculation number (if searching for it is allowed).
 */
class search_filter implements filter {

    use userids_filter_trait;

    /**
     * @var string search term
     */
    private $search = '';

    /**
     * @param string $search search term
     */
    public function __construct(string $search) {
        $this->search = $search;
    }

    public function apply(course_participants &$parts) {
        $search = preg_replace('/[[:punct:]]+/', ' ', $this->search);
        $search = str_replace("\xc2\xa0", ' ', $search); // Unicode \xc2\xa0 == non-breaking space.

        // Split search term into words.
        $words = array();
        $ret = preg_match_all('/[[:^space:]]+/', $search, $words);
        if (!$ret) {
            throw new moodle_exception('invalidarguments');
        }
        $words = $words[0]; // Use only full pattern matches. We use no subpatterns.

        // Each word of the search term must be matched by any of the user fields (that it is compared to).
        foreach ($words as $word) {
            $this->search_for_word($word, $parts);
        }
    }

    private function search_for_word(string $word, course_participants &$parts) {
        foreach ($parts->users as $user) {
            if (!$this->user_matches_word($user, $word, $parts)) {
                unset($parts->users[$user->id]);
            }
        }
    }

    private function user_matches_word(stdClass $user, string $word, course_participants $parts): bool {
        return $this->user_name_matches_word($user, $word, $parts)
            || ($this->user_email_matches_word($user, $word, $parts));
    }

    private function user_name_matches_word(stdClass $user, string $word, course_participants $parts): bool {
        if (get_config('rwth_participants_view', 'emaildisplaycontrolsnamedisplay')
                && maildisplay_helper::hide_mail($parts, $user->id)) {
            // Not a bug. We (ab)use the maildisplay setting to control display of names aswell.
            return false;
        }
        if (hiddentutor_helper::hide_tutor($parts, $user->id)) {
            return false;
        }
        return $this->string_contains_word($user->firstname, $word)
            || $this->string_contains_word($user->lastname, $word);
    }

    private function user_email_matches_word(stdClass $user, string $word, course_participants $parts): bool {
        if (maildisplay_helper::hide_mail($parts, $user->id)) {
            return false;
        }
        if (hiddentutor_helper::hide_tutor($parts, $user->id)) {
            return false;
        }
        return $this->string_contains_word($user->email, $word);
    }

    private function string_contains_word(string $string, string $word): bool {
        return strpos(mb_strtolower($string), mb_strtolower($word)) !== false;
    }
}