<?php
// This file is part of mod_grouptool for Moodle - http://moodle.org/
//
// It is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// It is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die();

use context_course;
use stdClass;

require_once($CFG->libdir.'/formslib.php');

class enrol_confirm_form extends \moodleform {

    protected function definition() {
        global $OUTPUT, $PAGE;
        $mform = $this->_form;
        $courseid = $this->_customdata['id'];
        $roleid = $this->_customdata['roleid'];
        $enrollable = $this->_customdata['enrollable'];
        $invitable = $this->_customdata['invitable'];
        $toomanyhits = $this->_customdata['toomanyhits'];
        $notfound = $this->_customdata['notfound'];
        $context = context_course::instance($courseid);

        $mform->addElement('hidden', 'id');
        $mform->setDefault('id', $courseid);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'roleid');
        $mform->setDefault('roleid', $roleid);
        $mform->setType('roleid', PARAM_INT);

        // Addable and invitable users.
        if (count($enrollable) + count($invitable) > 0) {
            $mform->addElement('html', '<div class="section">');
            $rolename = role_helper::get_rolename($roleid, $courseid);

            // Calculate how many users can actually be added, i.e. don't have the role yet.
            $addablecount = 0;
            foreach ($enrollable as $user) {
                if (!$user->hasrolealready) {
                    $addablecount++;
                }
            }
            foreach ($invitable as $extuser) {
                if (!$extuser->isalreadyinvited) {
                    $addablecount++;
                }
            }
            $caninvite = has_capability('local/rwth_participants_view:manageinvitations', $context);
            if ($addablecount == 1) {
                if ($caninvite) {
                    $description = get_string('enrolusers:confirmaddableorinvitablesingular', 'local_rwth_participants_view', $rolename);
                } else {
                    $description = get_string('enrolusers:confirmaddablesingular', 'local_rwth_participants_view', $rolename);
                }
            } else {
                $a = new stdClass();
                $a->num = $addablecount;
                $a->role = $rolename;
                if ($caninvite) {
                    $description = get_string('enrolusers:confirmaddableorinvitable', 'local_rwth_participants_view', $a);
                } else {
                    $description = get_string('enrolusers:confirmaddable', 'local_rwth_participants_view', $a);
                }
            }
            $mform->addElement('html', '<div class="addable-users-description">'.$description.'</div>');

            $mform->addElement('html', '<table id="enrol_addable" class="generaltable">');
            $mform->addElement('html', '<thead>');
            $mform->addElement('html', '<tr>');
            $mform->addElement('html', '<th class="header c0" scope="col">');
            $mform->addElement('checkbox', 'select_all', ''); 
            $mform->setDefault('select_all', 0);
            $mform->addElement('html', '</th>');
            $mform->addElement('html', '<th class="header c1" scope="col">'.get_string('enrolusers:heading_name', 'local_rwth_participants_view').'</th>');
            $mform->addElement('html', '<th class="header c2" scope="col"></th>');
            $mform->addElement('html', '<th class="header c3" scope="col">'.get_string('enrolusers:heading_action', 'local_rwth_participants_view').'</th>');
            $mform->addElement('html', '<th class="header c4 lastcol" scope="col"></th>');
            $mform->addElement('html', '</tr>');
            $mform->addElement('html', '</thead>');
            $mform->addElement('html', '<tbody>');
            foreach ($enrollable as $user) {
                $classes = ['internal-user'];
                if ($user->hasrolealready) {
                    $classes[] = 'already-added';
                }
                if (!$user->isemailunique) {
                    $classes[] = 'duplicate-account';
                }
                $class = implode(' ', $classes);
                $mform->addElement('html', '<tr class="'.$class.'">');
                $mform->addElement('html', '<td class="cell c0">');
                $mform->addElement('checkbox', 'enrol_'.$user->id, '');
                if ($user->hasrolealready) {
                    $mform->setDefault('enrol_'.$user->id, 1);
                    $mform->freeze('enrol_'.$user->id);
                } else {
                    $mform->setDefault('enrol_'.$user->id, $user->uniquehit ? 1 : 0);
                }
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '<td class="cell c1">');
                $mform->addElement('html', $user->fullname);
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '<td class="cell c2">');
                if (!empty($user->idnumber)) {
                    $mform->addElement('html', $user->idnumber);
                } else {
                    $mform->addElement('html', $user->email);
                }
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '<td class="cell c3">');
                $mform->addElement('html', get_string('enrolusers:action_add', 'local_rwth_participants_view'));
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '<td class="cell c4 lastcol">');
                $extras = [];
                if ($user->hasrolealready) {
                    $extras[] = get_string('enrolusers:alreadyadded', 'local_rwth_participants_view');
                }
                if (!$user->isemailunique) {
                    $extras[] = get_string('enrolusers:duplicateaccount', 'local_rwth_participants_view');
                }
                $mform->addElement('html', implode(', ', $extras));
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '</tr>');
            }
            foreach ($invitable as $extuser) {
                $classes = ['external-user'];
                if ($extuser->isalreadyinvited) {
                    $classes[] = 'already-invited';
                }
                $class = implode(' ', $classes);
                $mform->addElement('html', '<tr class="'.$class.'">');
                $mform->addElement('html', '<td class="cell c0">');
                $mform->addElement('checkbox', 'invite_'.$extuser->email, '');
                if ($extuser->isalreadyinvited) {
                    $mform->freeze('invite_'.$extuser->email);
                }
                $mform->setDefault('invite_'.$extuser->email, 1);
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '<td class="cell c1">');
                $mform->addElement('html', $extuser->email);
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '<td class="cell c2">');
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '<td class="cell c3">');
                $mform->addElement('html', get_string('enrolusers:action_invite', 'local_rwth_participants_view'));
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '<td class="cell c4 lastcol">');
                if ($extuser->isalreadyinvited) {
                    $mform->addElement('html', get_string('enrolusers:alreadyinvited', 'local_rwth_participants_view'));
                }
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '</tr>');
            }
            $mform->addElement('html', '</tbody>');
            $mform->addElement('html', '</table>');

            $PAGE->requires->js_call_amd('local_rwth_participants_view/selectall', 'init', ['enrol_addable']);

            // Send confirmation email checkbox.
            if (has_capability('local/rwth_participants_view:enrol_candisablemail', $context)) {
                $mform->addElement('checkbox', 'sendenrolmentemails', get_string('enrol_confirmationemails', 'local_rwth_participants_view'));
                $mform->setDefault('sendenrolmentemails', '1');
                $mform->addHelpButton('sendenrolmentemails', 'enrol_confirmationemails', 'local_rwth_participants_view');
            }
            $mform->addElement('html', '</div>');
        }

        // Search terms for which too many users were found.
        $disableconfirm = ''; // Will be set to ' disabled' if the confirm button should be disabled.
        if (!isset($addablecount) || $addablecount <= 0) { // Disable confirm button, because no user was found, that can be added to the course.
            $disableconfirm = ' disabled';
        }
        if (!empty($toomanyhits)) {
            $mform->addElement('html', '<div class="section">');
            if (count($toomanyhits) == 1) {
                // Only one search term with too many hits.
                $description = get_string('enrolusers:confirmtoomanyhitssingular', 'local_rwth_participants_view');
            } else {
                $description = get_string('enrolusers:confirmtoomanyhits', 'local_rwth_participants_view', count($toomanyhits));
            }
            $mform->addElement('html', '<div>'.$description.'</div>');

            $mform->addElement('html', '<ul class="searchterms">');
            foreach ($toomanyhits as $searchterm) {
                $mform->addElement('html', '<li>'.$searchterm.'</li>');
            }
            $mform->addElement('html', '</ul>');

            $mform->addElement('html', '</div>');
        }

        // Search terms for which no user was found.
        if (!empty($notfound)) {
            $mform->addElement('html', '<div class="section">');
            if (count($notfound) == 1) {
                $description = get_string('enrolusers:confirmnotfoundsingular', 'local_rwth_participants_view');
            } else {
                $description = get_string('enrolusers:confirmnotfound', 'local_rwth_participants_view', count($notfound));
            }
            $mform->addElement('html', '<div>'.$description.'</div>');
            if (has_capability('local/rwth_participants_view:manageinvitations', $context)) {
                $mform->addElement('html', '<div class="hint">'.get_string('enrolusers:confirmnotfoundhint', 'local_rwth_participants_view').'</div>');
            }

            $mform->addElement('html', '<ul class="searchterms">');
            foreach ($notfound as $searchterm) {
                $mform->addElement('html', '<li>'.$searchterm.'</li>');
            }
            $mform->addElement('html', '</ul>');

            $mform->addElement('html', '</div>');
        }

        $mform->addElement('html', '<div class="submit-buttons">');
        $mform->addElement('html', '<input type="submit" class="btn btn-secondary" name="back" id="id_back" value="'.get_string('back').'" data-skip-validation="1" data-cancel="1">');
        $mform->addElement('html', '<input type="submit" class="btn btn-primary" name="confirm" id="id_confirm" value="'.get_string('enrolusers:confirmaddselected', 'local_rwth_participants_view').'"'.$disableconfirm.'>');
        $mform->addElement('html', '</div>');

        // Loading animation.
        $loadingoverlay = $OUTPUT->render_from_template('core/overlay_loading', $context);
        $mform->addElement('html', $loadingoverlay);
    }
}