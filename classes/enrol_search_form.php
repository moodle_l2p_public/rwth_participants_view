<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');

use moodleform;
use moodle_exception;

class enrol_search_form extends moodleform {

    public function definition() {
        global $CFG, $DB, $OUTPUT, $PAGE;

        require_once($CFG->dirroot . '/enrol/locallib.php');

        $context = $this->_customdata['context'];

        // Get the course and enrolment instance.
        $coursecontext = $context->get_course_context();
        $course = $DB->get_record('course', ['id' => $coursecontext->instanceid]);
        $manager = new rwth_enrolment_manager($PAGE, $course);

        // Look for latest manual enrolment and use that.
        $instance = null;
        foreach ($manager->get_enrolment_instances() as $tempinstance) {
            if ($tempinstance->enrol == 'manual') {
                if ($instance === null) {
                    $instance = $tempinstance;
                    break;
                }
            }
        }
        if (empty($instance)) {
            // Create a new manual enrolment if there isn't one.
            $enrolplugin = new rwth_enrol_manual_plugin();
            if ($enrolplugin->can_add_instance($course->id)) {
                $instanceid = $enrolplugin->add_default_instance($course);
                $instance = $DB->get_record('enrol', ['id' => $instanceid]);
            } else {
                throw new moodle_exception('Cannot create new enrol instance.');
            }
        }

        $mform = $this->_form;
        $mform->disable_form_change_checker();

        // Hidden fields.
        $mform->addElement('hidden', 'id', $course->id);
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'action', 'enrol');
        $mform->setType('action', PARAM_ALPHA);

        // User search field.
        $threshold = get_config('rwth_participants_view', 'enrol_threshold');
        $mform->addElement('textarea', 'usersearch', get_string('enrolusers:usersearch', 'local_rwth_participants_view'), [
            'cols' => '90',
            'rows' => '9',
            'placeholder' => get_string('enrolusers:usersearchplaceholder', 'local_rwth_participants_view', $threshold)
        ]);
        $mform->addHelpButton('usersearch', 'enrolusers:usersearch', 'local_rwth_participants_view');
        $mform->setType('usersearch', PARAM_TEXT);
        if (has_capability('local/rwth_participants_view:manageinvitations', $context)) {
            $mform->addElement('static', 'usersearchhint', '', get_string('enrolusers:usersearchhint', 'local_rwth_participants_view'));
        }

        // Role select field.
        $visibleroles = role_helper::get_visible_roles($course->id);
        $assignableroles = get_assignable_roles($coursecontext);
        $roleoptions = [];
        foreach (array_keys($assignableroles) as $rid) {
            if (array_key_exists($rid, $visibleroles)) {
                if ($visibleroles[$rid]->shortname != "l2pmanagerexternal" || has_capability('moodle/site:config', $context)) {
                    $roleoptions[$rid] = $visibleroles[$rid]->guiname;
                }
            }
        }
        $mform->addElement('select', 'role', get_string("role"), $roleoptions, ['id' => 'roletoassign']);
        $mform->setDefault('role', get_config('rwth_participants_view', 'enrol_default_role'));

        // Submit button.
        $mform->addElement('submit', 'next', get_string('next'));

        // Loading animation.
        $loadingoverlay = $OUTPUT->render_from_template('core/overlay_loading', $context);
        $mform->addElement('html', $loadingoverlay);
    }
}
