<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2018 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view;

use context_course;
use moodle_exception;

defined('MOODLE_INTERNAL') || die;

class enrolment_helper {

    public static function uses_self_enrolment(int $courseid) {
        $parts = data\course_participants::get($courseid);

        foreach ($parts->enrolments as $enrolment) {
            if ($enrolment->enrol == 'self') {
                return true;
            }
        }
        return false;
    }

    public static function enrolment_instance_name(int $courseid, int $userid, $enrol) {
        $plugins = array();
        $instancename = '';
        $plugins = enrol_get_plugin($enrol->enrol);
        $instancename = $plugins->get_instance_name($enrol);
        return $instancename;
    }

    /**
     * Creates a manual enrolment instance for the given course if none exists. Returns
     * the new instance or an existing one.
     *
     * @return object "manual" enrol instance
     */
    public static function ensure_manual_enrolment_instance_exists(int $courseid) {
        global $DB, $PAGE;

        $course = $DB->get_record('course', ['id' => $courseid]);
        $manager = new rwth_enrolment_manager($PAGE, $course);

        // Look for latest manual enrolment and use that.
        $instance = null;
        foreach ($manager->get_enrolment_instances() as $ei) {
            if ($ei->enrol == 'manual') {
                $instance = $ei;
                break;
            }
        }
        if (empty($instance)) {
            // Create a new manual enrolment if there isn't one.
            $enrolplugin = new rwth_enrol_manual_plugin();
            if ($enrolplugin->can_add_instance($course->id)) {
                $instanceid = $enrolplugin->add_default_instance($course);
                $instance = $DB->get_record('enrol', ['id' => $instanceid]);
            } else {
                throw new moodle_exception('Cannot create new enrol instance.');
            }
        }
        return $instance;
    }
}
