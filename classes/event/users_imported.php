<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_rwth_participants_view\event;

defined('MOODLE_INTERNAL') || die;

class users_imported extends \core\event\base{

    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {  
        $this->data['crud'] = 'c';
        $this->data['edulevel'] = self::LEVEL_OTHER;
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        return "The user with id '$this->userid' imported '{$this->other['importedThisImport']}' users into the course with id '$this->courseid'. In total the user imported '{$this->other['countImportedUsers']}' users in the past '{$this->other['timeframe']}' minutes.";
    }
}