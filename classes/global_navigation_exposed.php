<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/navigationlib.php');

use global_navigation;

/**
 * The only purpose of this class is to give you access to protected members of the class
 * global_navigation.
 */
class global_navigation_exposed extends global_navigation {

    /**
     * Returns the given field value of the global_navigation.
     *
     * @param global_navigation $nav       the navigation structure
     * @param string            $fieldname name of the field
     * @return mixed a reference to the field value
     */
    public static function get_property(global_navigation $nav, string $fieldname) {
        return $nav->$fieldname;
    }

}