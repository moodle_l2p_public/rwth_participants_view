<?php
// This file is part of mod_grouptool for Moodle - http://moodle.org/
//
// It is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// It is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die();

use context_course;

require_once($CFG->libdir.'/formslib.php');

class groupimport_confirm_form extends \moodleform {

    protected function definition() {
        global $PAGE;
        $mform = $this->_form;
        $courseid = $this->_customdata['id'];
        $groupid = $this->_customdata['groupid'];
        $notfound = $this->_customdata['notfound'];
        $unenrolled = $this->_customdata['unenrolled'];
        $enrolled = $this->_customdata['enrolled'];

        $context = context_course::instance($courseid);

        $mform->addElement('hidden', 'id');
        $mform->setDefault('id', $courseid);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'groupid');
        $mform->setDefault('groupid', $groupid);
        $mform->setType('groupid', PARAM_INT);

        if (!empty($unenrolled)) {
            $mform->addElement('advcheckbox', 'sendenrolmentemails', get_string('enrol_confirmationemails', 'local_rwth_participants_view'));
            $mform->setDefault('sendenrolmentemails', '1');
            $mform->setType('sendenrolmentemails', PARAM_BOOL);
            $mform->addHelpButton('sendenrolmentemails', 'enrol_confirmationemails', 'local_rwth_participants_view');
            if (!has_capability('local/rwth_participants_view:enrol_candisablemail', $context)) {
                $mform->freeze('sendenrolmentemails');
            }

            $count = count($unenrolled);
            $mform->addElement('header', 'unenrolled', get_string('groupimport_unenrolled', 'local_rwth_participants_view', $count));
            $mform->setExpanded('unenrolled', $count <= 10);
            $mform->addElement('html', '<table id="groupimport_unenrolled" class="generaltable">');
            $mform->addElement('html', '<thead>');
            $mform->addElement('html', '<tr>');
            $mform->addElement('html', '<th class="header c0" scope="col">');
            $mform->addElement('checkbox', 'select_all', get_string('groupimport_heading_enrol', 'local_rwth_participants_view'));
            $mform->setDefault('select_all', 1);
            $mform->addElement('html', '</th>');
            $mform->addElement('html', '<th class="header c1 lastcol" scope="col">'.get_string('groupimport_heading_regnumber', 'local_rwth_participants_view').'</th>');
            $mform->addElement('html', '</tr>');
            $mform->addElement('html', '</thead>');
            $mform->addElement('html', '<tbody>');
            foreach ($unenrolled as $user) {
                $mform->addElement('html', '<tr>');
                $mform->addElement('html', '<td class="cell c0">');
                $mform->addElement('checkbox', 'enrol['.$user->id.']', '');
                $mform->setDefault('enrol['.$user->id.']', 1);
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '<td class="cell c1 lastcol">');
                $mform->addElement('html', $user->idnumber);
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '</tr>');
            }
            $mform->addElement('html', '</tbody>');
            $mform->addElement('html', '</table>');
            $PAGE->requires->js_call_amd('local_rwth_participants_view/selectall', 'init', ['groupimport_unenrolled']);
        }

        if (!empty($enrolled)) {
            $count = count($enrolled);
            $mform->addElement('header', 'enrolled', get_string('groupimport_enrolled', 'local_rwth_participants_view', $count));
            $mform->setExpanded('enrolled', false);
            $mform->addElement('html', '<table id="groupimport_enrolled" class="generaltable">');
            $mform->addElement('html', '<thead>');
            $mform->addElement('html', '<tr>');
            $mform->addElement('html', '<th class="header c0 lastcol" scope="col">'.get_string('groupimport_heading_regnumber', 'local_rwth_participants_view').'</th>');
            $mform->addElement('html', '</tr>');
            $mform->addElement('html', '</thead>');
            $mform->addElement('html', '<tbody>');
            foreach ($enrolled as $user) {
                $mform->addElement('hidden', 'import['.$user->id.']', $user->id);
                $mform->setType('import['.$user->id.']', PARAM_INT);
                $mform->addElement('html', '<tr>');
                $mform->addElement('html', '<td class="cell c0 lastcol">');
                $mform->addElement('html', $user->idnumber);
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '</tr>');
            }
            $mform->addElement('html', '</tbody>');
            $mform->addElement('html', '</table>');
        }

        if (!empty($notfound)) {
            $count = count($notfound);
            $mform->addElement('header', 'notfound', get_string('groupimport_notfound', 'local_rwth_participants_view', $count));
            $mform->setExpanded('notfound', false);
            $mform->addElement('html', '<table id="groupimport_enrolled" class="generaltable">');
            $mform->addElement('html', '<thead>');
            $mform->addElement('html', '<tr>');
            $mform->addElement('html', '<th class="header c0 lastcol" scope="col">'.get_string('groupimport_heading_searchterm', 'local_rwth_participants_view').'</th>');
            $mform->addElement('html', '</tr>');
            $mform->addElement('html', '</thead>');
            $mform->addElement('html', '<tbody>');
            foreach ($notfound as $searchterm) {
                $mform->addElement('html', '<tr>');
                $mform->addElement('html', '<td class="cell c0 lastcol">');
                $mform->addElement('html', $searchterm);
                $mform->addElement('html', '</td>');
                $mform->addElement('html', '</tr>');
            }
            $mform->addElement('html', '</tbody>');
            $mform->addElement('html', '</table>');
        }

        $buttonarray = [];
        $buttonarray[] = &$mform->createElement('submit', 'confirm', get_string('groupimport_confirm_button', 'local_rwth_participants_view'));
        $buttonarray[] = &$mform->createElement('cancel');
        $mform->addGroup($buttonarray, 'buttonar', '', [' '], false);
        $mform->closeHeaderBefore('buttonar');
    }
}

