<?php
// This file is part of mod_grouptool for Moodle - http://moodle.org/
//
// It is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// It is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');

class groupimport_search_form extends \moodleform {

    protected function definition() {
        $mform = $this->_form;
        $courseid = $this->_customdata['id'];
        $groupid = $this->_customdata['groupid'];

        $mform->addElement('hidden', 'id');
        $mform->setDefault('id', $courseid);
        $mform->setType('id', PARAM_INT);

        $groups = groups_get_all_groups($courseid);
        $groupmenu = groups_list_to_menu($groups);
        $mform->addElement('select', 'group', get_string('group'), $groupmenu);
        $mform->setType('group', PARAM_INT);
        if ($groupid) {
            $mform->setDefault('group', $groupid);
        }

        $threshold = get_config('rwth_participants_view', 'groupimport_threshold');
        $mform->addElement('textarea', 'data', get_string('groupimport_registration_numbers', 'local_rwth_participants_view'), [
            'wrap' => 'virtual',
            'rows' => '20',
            'placeholder' => get_string('groupimport_registration_numbers_placeholder', 'local_rwth_participants_view', $threshold)
        ]);
        $mform->addHelpButton('data', 'groupimport_registration_numbers', 'local_rwth_participants_view');

        $mform->addElement('submit', 'submitbutton', get_string('continue'));
    }
}

