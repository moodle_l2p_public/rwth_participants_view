<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2018 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die;

use context_course;
use core_user;
use moodle_exception;

/*
 * Another user's name/email address is hidden if:
 *
 *   current user's role is not a manager or tutor role
 *   and (
 *     ( // this is already taken care of by group filters
 *       groupmode == SEPARATEGROUPS
 *       and current user doesn't share a group
 *     )
 *     or (
 *       maildisplay == MAILDISPLAY_HIDE
 *       and (
 *         groupmode == NOGROUPS
 *         or (
 *           groupmode == VISIBLEGROUPS
 *           and current user doesn't share a group
 *         )
 *       )
 *     )
 *   )
 */

class hiddentutor_helper {

    /**
     * Check if we can see the name/email address of the user with the given userid.
     * If the user is a manager or tutor whose name/email address should never be hidden,
     * this will always return false.
     */
    public static function hide_tutor($parts, $userid) {
        global $USER, $COURSE;

        if ($userid == $USER->id) {
            // User can always see himself.
            return false;
        }

        $vuser = $parts->users[$userid];
        $ctx = context_course::instance($parts->course->id);

        $roles = get_user_roles($ctx, $userid, false);
        if (\is_siteadmin($userid)) {
            return false;
        } else if (!has_capability('local/rwth_participants_view:hiddenfromstudents', $ctx, $vuser)) {
            return false;
        } else if (has_capability('local/rwth_participants_view:viewhiddentutors', $ctx, $USER)) {
            return false;
        }
        return true;
    }
}
