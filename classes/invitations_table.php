<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/accesslib.php');
require_once($CFG->libdir.'/outputcomponents.php');
require_once($CFG->libdir.'/setuplib.php');
require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->libdir.'/weblib.php');

use stdClass;

use coding_exception;
use context;
use flexible_table;
use html_writer;
use moodle_url;
use local_rwth_participants_view\user_helper;

use local_rwth_participants_view\data;
use tool_sdapi\sitedirectory_api;

/**
 * A table for listing course invitations.
 */
class invitations_table extends flexible_table {

    private $course;
    private $canassign;
    private $bulkoperations;

    // Only have valid values after setup() has been called.
    private $invitationlist = null;
    private $totalcount = 0;

    /**
     * @param data\course_invitations_view_factory $invitationfactory factory for invitation display data
     * @param context    $context the context of the course
     * @param moodle_url $baseurl the URL of the site this table is displayed on
     * @param stdClass   $course  database row of course
     */
    public function __construct(context $context, moodle_url $baseurl, stdClass $course) {
        parent::__construct("rwth-invitations-$course->id");

        $this->define_baseurl($baseurl);
        $this->course = $course;
        $this->canassign = has_capability('moodle/role:assign', $context);
        $this->bulkoperations = has_capability('moodle/course:bulkmessaging', $context);
    }

    /**
     * Only call after setup() has been called.
     */
    private function get_sort_column() {
        return key($this->get_sort_internal());
    }

    /**
     * Only call after setup() has been called.
     *
     * possible values:
     *   SORT_DESC (3)
     *   SORT_ASC (4)
     */
    private function get_sort_direction() {
        return current($this->get_sort_internal());
    }

    /**
     * Only call after setup() has been called.
     */
    private function get_sort_internal() {
        global $SESSION;
        if (isset($SESSION->flextable[$this->uniqueid])) {
            $prefs = $SESSION->flextable[$this->uniqueid];
        } else if (!$prefs = json_decode(get_user_preferences('flextable_' . $this->uniqueid), true)) {
            return '';
        }

        $sortcol = $prefs['sortby'];
        if (empty($sortcol)) {
            $sortcol = '';
        }
        return $sortcol;
    }

    public function get_total_count() {
        return $this->totalcount;
    }

    function start_html() {
        // Render the dynamic table header.
        echo $this->get_dynamic_table_html_start();

        if (in_array(TABLE_P_TOP, $this->showdownloadbuttonsat)) {
            echo $this->download_buttons();
        }

        $this->wrap_html_start();
        // Start of main data table.

        if ($this->bulkoperations) {
            echo '<form action="statuschange.php" method="post" id="invitationsform">';
            echo '<div class="d-inline-block">';
            echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
            echo '<input type="hidden" name="id" value="'.$this->course->id.'" />';
            echo '<input type="hidden" name="returnto" value="'.s($this->baseurl->out(false)).'" />';
            echo $this->render_reset_button();
        }

        echo html_writer::start_tag('div', array('class' => 'no-overflow'));
        echo html_writer::start_tag('table', $this->attributes);
    }

    function finish_html() {
        global $PAGE;

        if (!$this->started_output) {
            // No data has been added to the table.
            echo $this->render_reset_button();
            $this->print_nothing_to_display();
            return;
        }

        parent::finish_html();
        if ($this->bulkoperations) {
            echo '<div class="buttons">';

            // With JavaScript enabled hide bulk operations bar by default.
            echo '<div class="bulk-operations-bar jsonly hidden">';
            echo $this->render_bulk_operations_bar(true);
            echo '</div>';
            echo '<noscript>';
            echo $this->render_bulk_operations_bar(false);
            echo '</noscript>';

            echo '</div></div>';
            echo '</form>';
            if ($this->canassign) {
                $PAGE->requires->js_call_amd('local_rwth_participants_view/bulkoperations',
                    'init', ['invitations', 'invitationselected', 'invitationsselected']);
            }
        }
    }

    /**
     * Renders the bulk operations bar.
     *
     * @param bool $js true for display in JavaScript enabled browsers, false otherwise
     * @return string html
     */
    private function render_bulk_operations_bar($js): string {
        $html = '';
        if ($js) {
            $html .= html_writer::tag('span', get_string('invitationselected', 'local_rwth_participants_view'), ['class' => 'selectioncounter']);
        }

        $html .= html_writer::start_tag('span', [
            'class' => 'bulk-operations-bar-btns'
        ]);
        $html .= html_writer::empty_tag('input', [
            'type' => 'hidden',
            'name' => 'formaction',
            'value' => 'statuschange.php'
        ]);
        $html .= html_writer::empty_tag('input', [
            'type' => 'button',
            'role' => 'button',
            'class' => 'btn btn-secondary clear-selection-btn',
            'value' => get_string('clearselection', 'local_rwth_participants_view')
        ]);
        $html .= html_writer::empty_tag('input', [
            'type' => 'submit',
            'role' => 'button',
            'name' => 'action',
            'class' => 'btn btn-primary revoke_invitation-btn',
            'value' => get_string('revokeinvitation', 'local_rwth_participants_view')
        ]);
        $html .= html_writer::empty_tag('input', [
            'type' => 'submit',
            'role' => 'button',
            'name' => 'action',
            'class' => 'btn btn-primary renew_invitation-btn',
            'value' => get_string('renewinvitation', 'local_rwth_participants_view')
        ]);
        $html .= html_writer::end_tag('span');
        return $html;
    }


    /**
     * initializes $this->invitationlist
     */
    public function setup() {
        $tablecolumns = array();
        $tableheaders = array();

        if ($this->bulkoperations && $this->canassign) {
            $tablecolumns[] = 'select';
            $tableheaders[] = html_writer::empty_tag('input', array('type' => 'checkbox', 'class' => 'bulkoperation', 'name' => 'all'));
        }

        $tablecolumns[] = 'email';
        $tableheaders[] = get_string('email');

        $tablecolumns[] = 'role';
        $tableheaders[] = get_string('role');

        $tablecolumns[] = 'invited_by';
        $tableheaders[] = get_string('invited_by', 'local_rwth_participants_view');

        $tablecolumns[] = 'status';
        $tableheaders[] = get_string('status');

        if ($this->canassign) {
            $tablecolumns[] = 'delete';
            $tableheaders[] = '';
        }

        $this->define_columns($tablecolumns);
        $this->define_headers($tableheaders);

        $this->sortable(true, 'email', SORT_ASC); // Default sort column.
        $this->no_sorting('select');
        $this->no_sorting('delete');

        $this->set_attribute('cellspacing', '0');
        $this->set_attribute('class', 'generaltable generalbox invitations');
        $this->set_attribute('data-region', 'invitations-table');
        $this->set_attribute('data-table-uniqueid', $this->uniqueid);

        $this->set_control_variables([
            TABLE_VAR_SORT  => 'isort',
            TABLE_VAR_RESET => 'ireset',
        ]);

        $this->baseurl->set_anchor('invitations');

        parent::setup();
    }

    /*
     * Gets the users, active initials, match count and total user count.
     */
    protected function get_data() {
        $factory = new data\course_invitations_view_factory($this->course->id);

        $sortcol = $this->get_sort_column();
        switch ($sortcol) {
            case 'email':
                $sortcol = data\course_invitations_view_factory::SORT_COL_EMAIL;
                break;
            case 'role':
                $sortcol = data\course_invitations_view_factory::SORT_COL_ROLE;
                break;
            case 'status':
                $sortcol = data\course_invitations_view_factory::SORT_COL_INVITESTATUS;
                break;
            case 'invited_by':
                $sortcol = data\course_invitations_view_factory::SORT_COL_CREATEDBY;
                break;
            default:
                throw new coding_exception("unknown sort column $sortcol");
        }
        $factory->set_sort_column($sortcol);

        // Sort direction.
        $sortdir = $this->get_sort_direction();
        switch ($sortdir) {
            case SORT_DESC:
                $sortdir = data\course_invitations_view_factory::SORT_DIR_DESC;
                break;
            case SORT_ASC:
                $sortdir = data\course_invitations_view_factory::SORT_DIR_ASC;
                break;
            default:
                throw new coding_exception("unknown sort direction $sortdir");
        }
        $factory->set_sort_direction($sortdir);

        // Count total invitations matching all filters.
        $totalcounter = new data\invitationcount_filter();
        $factory->add_filter($totalcounter);

        // Apply filters etc.
        $this->invitationlist = $factory->create();

        $this->totalcount = $totalcounter->get();
    }

    public function render(): string {
        global $DB;
        $this->get_data();

        ob_start();

        foreach ($this->invitationlist as $invitation) {
            global $OUTPUT;

            $data = [];
            // Selection checkbox.
            if ($this->canassign && $this->bulkoperations) {
                    $data[] = '<input type="checkbox" class="bulkoperation" name="invitation'.$invitation->id.'"/>';
            }

            $data[] = $invitation->email;
            $data[] = $invitation->rolename;

            if (!empty($invitation->createdby)) {
                $createdby = $DB->get_record('user', ['id' => $invitation->createdby]);
                $data[] = fullname($createdby);
            } else {
                $data[] = get_string('unknown', 'local_rwth_participants_view');
            }

            if ($invitation->status == sitedirectory_api::INVITATION_STATUS_OPEN) {
                $expires = userdate($invitation->expires, get_string('strftimedatefullshort', 'langconfig'));
                $data[] = get_string('invitestatus'.$invitation->status, 'local_rwth_participants_view', $expires);
            } else {
                $data[] = get_string('invitestatus'.$invitation->status, 'local_rwth_participants_view');
            }

            // Delete button.
            if ($this->canassign) {
                $deletebuttons = '';
                if ($invitation->status == sitedirectory_api::INVITATION_STATUS_OPEN) {
                    $strunassign = get_string('revokeinvitation', 'local_rwth_participants_view');
                    $icon = $OUTPUT->pix_icon('t/delete', $strunassign);
                    $url = new moodle_url($this->baseurl, ['action' => 'revokeinvitation', 'invitation' => $invitation->id]);
                    $deletebuttons .= html_writer::tag('div',
                        html_writer::link($url, $icon, ['class' => 'revokeinvitationlink', 'title' => $strunassign]),
                        ['class' => 'invitation']);
                }
                $data[] = $deletebuttons;
            }

            $this->add_data($data);
        }
        $this->finish_html();

        $html = ob_get_clean();
        return $html;
    }

    /**
     * Generate the HTML for the table preferences reset button.
     *
     * @return string HTML fragment, empty string if no need to reset
     */
    protected function render_reset_button() {

        if (!$this->can_be_reset()) {
            return '';
        }

        $url = $this->baseurl->out(false, [$this->request[TABLE_VAR_RESET] => 1]);

        $html  = html_writer::start_div('resettable mdl-right');
        $html .= html_writer::link($url, get_string('resettable', 'local_rwth_participants_view'));
        $html .= html_writer::end_div();

        return $html;
    }
}
