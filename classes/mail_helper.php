<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2021 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view;

use context_course;
use core\message\message;
use core_user\fields;
use moodle_url;
use stdClass;

defined('MOODLE_INTERNAL') || die;

class mail_helper {

    public static function send_booking_confirmations(array $users, int $courseid, int $roleid) {
        global $CFG, $DB, $SESSION, $SITE, $USER;
        $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
        $context = context_course::instance($courseid, MUST_EXIST);
        $rolenames = role_fix_names(get_all_roles($context), $context, ROLENAME_ALIAS, true);
        $messagestrings = new stdClass();
        $messagestrings->sitename = $SITE->fullname;

        $issetforcelang = isset($SESSION->forcelang);
        if ($issetforcelang) {
            $originalforcelang = $SESSION->forcelang;
        }
        $SESSION->forcelang = 'en';
        $messagestrings->coursename_en = format_string($course->fullname);
        $SESSION->forcelang = 'de';
        $messagestrings->coursename_de = format_string($course->fullname);
        if ($issetforcelang) {
            $SESSION->forcelang = $originalforcelang;
        } else {
            unset($SESSION->forcelang);
        }

        $messagestrings->courseidnumber = $course->idnumber;
        $messagestrings->rolename = $rolenames[$roleid];
        $messagestrings->enrolling_person_fullname = $USER->firstname.' '.$USER->lastname;
        $messagestrings->enrolling_person_email = $USER->email;
        $siteurl = new moodle_url('/');
        $messagestrings->siteurl = $siteurl->out();
        $courseurl = new moodle_url('/course/view.php', ['id' => $course->id]);
        $messagestrings->courseurl = $courseurl->out();
        $messagestrings->support = 'servicedesk@itc.rwth-aachen.de';
        foreach ($users as $receiver) {
            // Make Moodle shut up about missing additional name fields.
            if ($CFG->debugdeveloper) {
                $allnames = fields::get_name_fields();
                foreach ($allnames as $allname) {
                    if (!property_exists($receiver, $allname)) {
                        $receiver->$allname = '';
                    }
                }
            }

            $receiverfullname = $receiver->firstname.' '.$receiver->lastname;
            $messagestrings->enrolled_person_fullname = $receiverfullname;

            $message = new message();
            $message->courseid = $course->id;
            $message->component = 'local_rwth_participants_view';
            $message->name = 'enrolment_confirmations';
            $message->userfrom = $USER;
            $message->userto = $receiver;
            $message->subject = get_string('enrolmentconfirmationtitle', 'local_rwth_participants_view', 'RWTHmoodle');
            $message->fullmessage = get_string_manager()->get_string('enrolmentconfirmationmessage', 'local_rwth_participants_view', $messagestrings);
            $message->fullmessagehtml = preg_replace('/\\r?\\n/', '<br />', $message->fullmessage);
            $message->fullmessageformat = FORMAT_HTML;

            message_send($message);
        }
    }

    public static function send_invitation(string $receiver, string $couponcode, int $courseid, int $roleid, int $expires) {
        global $DB, $SESSION, $SITE, $USER;
        $course = $DB->get_record('course', ['id' => $courseid], '*', MUST_EXIST);
        $context = context_course::instance($courseid, MUST_EXIST);
        $a = new stdClass();
        $a->sitename = $SITE->fullname;
        $a->couponcode = $couponcode;
        $a->expirationdate_en = userdate($expires, get_string_manager()->get_string('strftimedatefullshort', 'langconfig', null, 'en'));
        $a->expirationdate_de = userdate($expires, get_string_manager()->get_string('strftimedatefullshort', 'langconfig', null, 'de'));

        $issetforcelang = isset($SESSION->forcelang);
        if ($issetforcelang) {
            $originalforcelang = $SESSION->forcelang;
        }
        $SESSION->forcelang = 'en';
        $a->coursename_en = format_string($course->fullname);
        $rolenames = role_fix_names(get_all_roles($context), $context, ROLENAME_ALIAS, true);
        $a->rolename_en = $rolenames[$roleid];
        $SESSION->forcelang = 'de';
        $a->coursename_de = format_string($course->fullname);
        $rolenames = role_fix_names(get_all_roles($context), $context, ROLENAME_ALIAS, true);
        $a->rolename_de = $rolenames[$roleid];
        if ($issetforcelang) {
            $SESSION->forcelang = $originalforcelang;
        } else {
            unset($SESSION->forcelang);
        }

        $a->courseidnumber = $course->idnumber;
        $a->enrolling_person_fullname = $USER->firstname.' '.$USER->lastname;
        $a->enrolling_person_email = $USER->email;
        $siteurl = new moodle_url('/');
        $a->siteurl = $siteurl->out();
        $courseurl = new moodle_url('/course/view.php', ['id' => $course->id]);
        $a->courseurl = $courseurl->out();
        $a->support = 'servicedesk@itc.rwth-aachen.de';

        $receivinguser = new stdClass();
        $receivinguser->id = 1;
        $receivinguser->email = $receiver;
        $receivinguser->deleted = false;
        $receivinguser->mailformat = 1; // 0 = textformat, 1 = htmlformat
        $subject = get_string('invitationtitle', 'local_rwth_participants_view', $a);
        $messagetext = ''; // We only send HTML mails.
        $messagehtml = get_string('invitationmessage', 'local_rwth_participants_view', $a);
        $messagehtml = preg_replace('/\\r?\\n/', '<br />', $messagehtml);

        email_to_user($receivinguser, $USER, $subject, $messagetext, $messagehtml);
    }
}
