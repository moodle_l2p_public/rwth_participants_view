<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2018 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die;

use context_course;
use core_user;
use moodle_exception;

/*
 * Another user's name/email address is hidden if:
 *
 *   current user's role is not a manager or tutor role
 *   and (
 *     ( // this is already taken care of by group filters
 *       groupmode == SEPARATEGROUPS
 *       and current user doesn't share a group
 *     )
 *     or (
 *       maildisplay == MAILDISPLAY_HIDE
 *       and (
 *         groupmode == NOGROUPS
 *         or (
 *           groupmode == VISIBLEGROUPS
 *           and current user doesn't share a group
 *         )
 *       )
 *     )
 *   )
 */

class maildisplay_helper {

    /**
     * Cache whether maildisplay should be ignored for users who have a role.
     * format: [roleid => true/false]
     */
    private static $ignoremaildisplay = [];

    private static function build_cache($parts) {
        $roleidsmailvisible = explode(',', get_config('rwth_participants_view', 'roles_with_visible_user_info'));
        foreach ($parts->roles as $role) {
            self::$ignoremaildisplay[$role->id] = in_array($role->id, $roleidsmailvisible);
        }
    }

    /**
     * Check if we can see the name/email address of the user with the given userid.
     * If the user is a manager or tutor whose name/email address should never be hidden,
     * this will always return false.
     */
    public static function hide_mail($parts, $userid) {

        // If user has at least on role for which maildisplay is ignored, we will not
        // hide his mail address.
        if ($parts->user2role != null && array_key_exists($userid, $parts->user2role)) {
            foreach ($parts->user2role[$userid] as $roledata) {
                $rid = $roledata->roleid;
                if (!array_key_exists($rid, self::$ignoremaildisplay)) {
                    // Build cache.
                    self::build_cache($parts);
                }
                if (self::$ignoremaildisplay[$rid]) {
                    return false;
                }
            }
        }
        return self::user_mail_is_hidden($parts, $userid);
    }

    /**
     * Check if we can see the name/email address of the user with the given userid.
     * 
     * NOTE: This doesn't check whether the user is a manager or tutor whose name/email
     *       address should never be hidden.
     */
    private static function user_mail_is_hidden($parts, $userid) {
        global $USER,$DB;
        if ($userid == $USER->id) {
            // User can always see himself.
            return false;
        }

        $coursecontext = context_course::instance($parts->course->id);
        if (has_capability('moodle/site:viewuseridentity', $coursecontext)) {
            // We have permission to see hidden names/email addresses.
            return false;
        }

        $user = $parts->users[$userid];
        if ($user->maildisplay != core_user::MAILDISPLAY_HIDE) {
            // User doesn't hide his name/email address.
            return false;
        }

        if (!has_capability('local/rwth_participants_view:viewgroupmemberdata', $coursecontext)) {
            return true;
        }

        // Name/email address is hidden.
        // return true;

        // TODO Add specialized handling for groups that are used in aggregation.
        //      For now we just disable name/email visibility for group members.
        return !self::user_shares_a_group($parts, $userid);
    }

    private static function user_shares_a_group($parts, $userid) {
        global $USER, $DB;

        $useringroups = $parts->group2user;

        if (is_null($parts->user2group) || !array_key_exists($userid, $parts->user2group)) {
            return false;
        }

        $usergroups = $parts->user2group[$userid];

        try {
            $self = user_helper::get_user_with_id($parts->course->id, $USER->id);
        } catch (moodle_exception $e) {
            // Could happen if we aren't enrolled in the course ourself.
            return false;
        }
        $owngroups = array_keys($self->groups);
        $commongroups = array_intersect($usergroups, $owngroups);

        if (empty($commongroups)) {
            // We don't share a group.
            return false;
        }

        $maxgroupsize = get_config('rwth_participants_view', 'groupsize_visibility');

        foreach ($commongroups as $group) {
            if ($DB->record_exists('rwth_sd_sync_group', array('moodle_groupid' => $group))) {
                return false;
            }
            $length = count($useringroups[$group]);
            if ($length <= $maxgroupsize) { 
                return true;
            }
        }
        // We share a group but the group has too many students in it.
        return false;
    }
}