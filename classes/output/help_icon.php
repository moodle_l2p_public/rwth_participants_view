<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2018 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\output;

defined('MOODLE_INTERNAL') || die;

use moodle_url;
use pix_icon;
use renderer_base;

class help_icon extends \help_icon {

    /**
     * @var mixed Extra content for the help text
     */
    public $a = null;

    /**
     * Constructor
     *
     * @param string $identifier string for help page title,
     *  string with _help suffix is used for the actual help text.
     *  string with _link suffix is used to create a link to further info (if it exists)
     * @param string $component
     * @param mixed  $a dynamic content to replace {$a} with in the help text
     */
    public function __construct($identifier, $component, $a) {
        parent::__construct($identifier, $component);
        $this->a = $a;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output Used to do a final render of any components that need to be rendered for export.
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        global $CFG;

        $title = get_string($this->identifier, $this->component);

        if (empty($this->linktext)) {
            $alt = get_string('helpprefix2', '', trim($title, ". \t"));
        } else {
            $alt = get_string('helpwiththis');
        }

        $data = get_formatted_help_string($this->identifier, $this->component, false, $this->a);

        $data->alt = $alt;
        $data->icon = (new pix_icon('help', $alt, 'core', ['class' => 'iconhelp']))->export_for_template($output);
        $data->linktext = $this->linktext;
        $data->title = get_string('helpprefix2', '', trim($title, ". \t"));
        $data->url = (new moodle_url('/help.php', [
            'component' => $this->component,
            'identifier' => $this->identifier,
            'lang' => current_language()
        ]))->out(false);

        $data->ltr = !right_to_left();
        return $data;
    }
}