<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\output;

defined('MOODLE_INTERNAL') || die;

use renderable;
use renderer_base;
use templatable;
use stdClass;

class page implements renderable, templatable {

    public $participantstitle;
    public $participantshtml;
    public $invitationstitle;
    public $invitationshtml;
    public $addparticipantshtml;

    public function __construct($viewparticipants, $participantstitle, $participantshtml, $viewinvitations, $invitationstitle, $invitationshtml, $addparticipantshtml) {
        $this->viewparticipants = $viewparticipants;
        $this->participantstitle = $participantstitle;
        $this->participantshtml = $participantshtml;
        $this->viewinvitations = $viewinvitations;
        $this->invitationstitle = $invitationstitle;
        $this->invitationshtml = $invitationshtml;
        $this->addparticipantshtml = $addparticipantshtml;
    }

    public function export_for_template(renderer_base $output) {
        $data = new stdClass();
        $data->viewparticipants = $this->viewparticipants;
        $data->participantstitle = $this->participantstitle;
        $data->participantshtml = $this->participantshtml;
        $data->viewinvitations = $this->viewinvitations;
        $data->invitationstitle = $this->invitationstitle;
        $data->invitationshtml = $this->invitationshtml;
        $data->addparticipantshtml = $this->addparticipantshtml;
        return $data;
    }
}
