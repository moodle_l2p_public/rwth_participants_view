<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view\output;

defined('MOODLE_INTERNAL') || die;

use Exception;

use core;
use core\output\notification;
use templatable;
use html_writer;
use plugin_renderer_base;
use rwth_initials_bar;
use rwth_single_select;
use enrol_user_button;
use paging_bar;

class renderer extends plugin_renderer_base {

    protected function render_page($page) {
        $data = $page->export_for_template($this);
        return parent::render_from_template('local_rwth_participants_view/page', $data);
    }

    /**
     * Internal implementation of better initials bar rendering.
     *
     * @param rwth_initials_bar $initialsbar
     * @return string
     */
    protected function render_rwth_initials_bar($initialsbar) {
        $data = $initialsbar->export_for_template($this);
        return $this->render_from_template('local_rwth_participants_view/rwth_initials_bar', $data);
    }

    protected function render_filters($filters) {
        $data = $filters->export_for_template($this);
        return parent::render_from_template('local_rwth_participants_view/filters', $data);
    }

    /**
     * Internal implementation of single_select allows to disable js autosubmit.
     *
     * @param rwth_single_select $singleselect
     * @return string
     */
    protected function render_rwth_single_select($singleselect) {
        $data = $singleselect->export_for_template($this);
        return parent::render_from_template('local_rwth_participants_view/rwth_single_select', $data);
    }

    /**
     * Renderers the enrol_user_button.
     *
     * The only thing this function does differently from the one in
     * enrol/renderer.php is that it doesn't add the class 'm-y-1'
     * to the button, i.e. it removes the vertical spacing.
     *
     * @param enrol_user_button $button
     * @return string XHTML
     */
    protected function render_rwth_enrol_user_button(enrol_user_button $button): string {
        $attributes = array('type'     => 'submit',
                            'value'    => $button->label,
                            'disabled' => $button->disabled ? 'disabled' : null,
                            'title'    => $button->tooltip,
                            'class'    => 'btn btn-primary');

        if ($button->actions) {
            $id = html_writer::random_id('single_button');
            $attributes['id'] = $id;
            foreach ($button->actions as $action) {
                $this->add_action_handler($action, $id);
            }
        }
        $button->initialise_js($this->page);

        // First the input element ...
        $output = html_writer::empty_tag('input', $attributes);

        // ... then hidden fields.
        $params = $button->url->params();
        if ($button->method === 'post') {
            $params['sesskey'] = sesskey();
        }
        foreach ($params as $var => $val) {
            $output .= html_writer::empty_tag('input', array('type' => 'hidden', 'name' => $var, 'value' => $val));
        }

        // ... then div wrapper for xhtml strictness.
        $output = html_writer::tag('div', $output);

        // ... now the form itself around it.
        if ($button->method === 'get') {
            $url = $button->url->out_omit_querystring(true); // Url without params, the anchor part allowed.
        } else {
            $url = $button->url->out_omit_querystring();     // Url without params, the anchor part not allowed.
        }
        if ($url === '') {
            $url = '#'; // There has to be always some action.
        }
        $attributes = array('method' => $button->method,
                            'action' => $url,
                            'id'     => $button->formid);
        $output = html_writer::tag('form', $output, $attributes);

        // ... and finally one more wrapper with class.
        return html_writer::tag('div', $output, array('class' => $button->class));
    }

    /**
     * Renders an mform element from a template.
     *
     * @param HTML_QuickForm_element $element element
     * @param bool $required if input is required field
     * @param bool $advanced if input is an advanced field
     * @param string $error error message to display
     * @param bool $ingroup True if this element is rendered as part of a group
     * @return mixed string|bool
     */
    public function mform_element($element, $required, $advanced, $error, $ingroup) {
        global $OUTPUT;

        $type = $element->getType();
        if ($type !== 'rwth_autocomplete') {
            return $OUTPUT->mform_element($element, $required, $advanced, $error, $ingroup);
        }

        $templatename = 'local_rwth_participants_view/element-' . $type;
        if ($ingroup) {
            $templatename .= "-inline";
        }
        try {
            // We call this to generate a file not found exception if there is no template.
            // We don't want to call export_for_template if there is no template.
            core\output\mustache_template_finder::get_template_filepath($templatename);

            if ($element instanceof templatable) {
                $elementcontext = $element->export_for_template($this);

                $helpbutton = '';
                if (method_exists($element, 'getHelpButton')) {
                    $helpbutton = $element->getHelpButton();
                }
                $label = $element->getLabel();
                $text = '';
                if (method_exists($element, 'getText')) {
                    // There currently exists code that adds a form element with an empty label.
                    // If this is the case then set the label to the description.
                    if (empty($label)) {
                        $label = $element->getText();
                    } else {
                        $text = $element->getText();
                    }
                }

                $context = array(
                    'element' => $elementcontext,
                    'label' => $label,
                    'text' => $text,
                    'required' => $required,
                    'advanced' => $advanced,
                    'helpbutton' => $helpbutton,
                    'error' => $error
                );
                return $this->render_from_template($templatename, $context);
            }
        } catch (Exception $e) {
            // No template for this element.
            return false;
        }
    }

    public function notification_without_closebutton($message, $type) {
        $notification = new notification($message, $type);
        $notification->set_show_closebutton(false);
        return $this->render_from_template($notification->get_template_name(), $notification->export_for_template($this));
    }

    /**
     * Returns HTML to display the paging bar.
     *
     * @param paging_bar $pagingbar
     * @return string the HTML to output.
     */
    protected function render_paging_bar(paging_bar $pagingbar) {
        // Any more than 10 is not usable and causes weird wrapping of the pagination.
        $pagingbar->maxdisplay = 10;
        return $this->render_from_template('local_rwth_participants_view/paging_bar', $pagingbar->export_for_template($this));
    }
}