<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die;

/**#@+
 * These constants relate to the table's handling of URL parameters.
 */
define('TABLE_VAR_ROLE',     9);
define('TABLE_VAR_GROUP',   10);
define('TABLE_VAR_SEARCH',  11);
define('TABLE_VAR_PERPAGE', 12);
/**#@-*/

require_once($CFG->libdir.'/accesslib.php');
require_once($CFG->libdir.'/grouplib.php');
require_once($CFG->libdir.'/outputcomponents.php');
require_once($CFG->libdir.'/setuplib.php');
require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->libdir.'/weblib.php');

use stdClass;

use coding_exception;
use context;
use flexible_table;
use html_writer;
use moodle_url;
use DateTime;

use local_rwth_participants_view\data;
use local_rwth_participants_view\output;
use core_user\output\status_field;
use local_rwth_participants_view\output\filters;
use moodle_exception;
use paging_bar;
use pix_icon;
use context_course;

/** A table for listing course participants. */
class participants_table extends flexible_table {

    const DEFAULT_PAGE_SIZE = 20;

    const COLUMN_PICTURE = 1;

    private $context;
    private $course;
    private $visibleroles;
    private $assignableroles;
    private $canassign;
    private $canenrol;
    private $aag;
    private $hasgroups;

    private $showoptionalcolumns = [self::COLUMN_PICTURE => false];
    private $downloading = false;
    private $filenamewithoutextension = '';
    private $showfullname = true;
    private $prefs = [];

    // Only have valid values after setup() has been called.
    private $userlist = null;
    private $filteredroles = null;
    private $ifirstenabled = null;
    private $ilastenabled = null;
    private $totalcount = 0;
    private $matchcount = 0;

    /**
     * @param data\course_participants_view_factory $userfactory factory for user display data
     * @param context    $context the context of the course
     * @param moodle_url $baseurl the URL of the site this table is displayed on
     * @param stdClass   $course  database row of course
     */
    public function __construct(context $context, moodle_url $baseurl, stdClass $course) {
        parent::__construct("rwth-participants-$course->id");
        $this->request = [
            TABLE_VAR_SORT    => 'sort',
            TABLE_VAR_HIDE    => 'hide',
            TABLE_VAR_SHOW    => 'show',
            TABLE_VAR_IFIRST  => 'ifirst',
            TABLE_VAR_ILAST   => 'ilast',
            TABLE_VAR_PAGE    => 'page',
            TABLE_VAR_RESET   => 'reset',
            TABLE_VAR_DIR     => 'dir',
            TABLE_VAR_ROLE    => 'role',
            TABLE_VAR_GROUP   => 'group',
            TABLE_VAR_SEARCH  => 'search',
            TABLE_VAR_PERPAGE => 'perpage',
        ];

        $this->context = $context;
        $this->define_baseurl($baseurl);
        $this->course = $course;
        if ($this->course->groupmode == NOGROUPS) {
            $this->course->groupmode = VISIBLEGROUPS;
        }
        $this->visibleroles = role_helper::get_visible_roles($course->id);
        $this->assignableroles = get_assignable_roles($context);
        $this->canassign = has_capability('moodle/role:assign', $context);
        $this->canenrol = has_capability('local/rwth_participants_view:enrol', $context);
        $this->aag = has_capability('moodle/site:accessallgroups', $context);
        $this->hasgroups = !empty(groups_get_all_groups($course->id));
    }

    /**
     * whether an optional column should be shown
     *
     * [chainable]
     */
    public function set_show_optional_column($columnnr, bool $enabled = true) {
        $this->showoptionalcolumns[$columnnr] = $enabled;
        return $this;
    }

    /**
     * whether we are viewing the table or exporting it (for download)
     *
     * [chainable]
     */
    public function set_downloading (bool $enabled = true, string $filenamewithoutextension = '') {
        $this->downloading = $enabled;
        $this->filenamewithoutextension = $filenamewithoutextension;
        return $this;
    }

    /**
     * whether to use one column for the full name instead of using one for the first and one for the last name
     *
     * [chainable]
     */
    public function set_show_fullname(bool $showfullname) {
        $this->showfullname = $showfullname;
        return $this;
    }

    /**
     * Only call after setup() has been called.
     */
    private function get_sort_column() {
        return key($this->get_sort_internal());
    }

    /**
     * Only call after setup() has been called.
     *
     * possible values:
     *   SORT_DESC (3)
     *   SORT_ASC (4)
     */
    private function get_sort_direction() {
        return current($this->get_sort_internal());
    }

    /**
     * Only call after setup() has been called.
     */
    private function get_sort_internal() {
        global $SESSION;
        if (isset($SESSION->flextable[$this->uniqueid])) {
            $prefs = $SESSION->flextable[$this->uniqueid];
        } else if (!$prefs = json_decode(get_user_preferences('flextable_' . $this->uniqueid), true)) {
            return '';
        }

        $sortcol = $prefs['sortby'];
        if (empty($sortcol)) {
            $sortcol = '';
        }
        return $sortcol;
    }

    public function get_initial_first() {
        $ifirst = parent::get_initial_first();
        if (empty($ifirst)) {
            $ifirst = '';
        }
        return $ifirst;
    }

    public function get_initial_last() {
        $ilast = parent::get_initial_last();
        if (empty($ilast)) {
            $ilast = '';
        }
        return $ilast;
    }

    public function get_total_count() {
        return $this->totalcount;
    }

    public function get_match_count() {
        return $this->matchcount;
    }

    /**
     * Has been modified to disable initials for which no entries exist.
     *
     * @return void
     */
    public function render_initials_bars() {
        global $PAGE;

        $prefixfirst = $this->request[TABLE_VAR_IFIRST];
        $prefixlast = $this->request[TABLE_VAR_ILAST];

        $firstinitialsbar = new output\rwth_initials_bar($this->get_initial_first(), 'firstinitial',
                get_string('firstname'), $prefixfirst, $this->baseurl, self::get_initials(),
                $this->ifirstenabled);
        $lastinitialsbar = new output\rwth_initials_bar($this->get_initial_last(), 'lastinitial',
                get_string('lastname'), $prefixlast, $this->baseurl, self::get_initials(),
                $this->ilastenabled);

        $output = $PAGE->get_renderer('local_rwth_participants_view');
        return $output->render($firstinitialsbar) . $output->render($lastinitialsbar);
    }

    /**
     * Get initials for initialsbar.
     *
     * @return array initials
     */
    public static function get_initials(): array {
        // If you want to have umlauts (Ä, Ö, Ü) even though the display language normally doesn't have them,
        // change the following value (in this example we add umlauts for English):
        // Site administration
        //   Language
        //     Language customisation
        //       English (en)
        //         core
        //           langconfig.php
        //             alphabet
        //               default value: A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z
        //               new value:     A,Ä,B,C,D,E,F,G,H,I,J,K,L,M,N,O,Ö,P,Q,R,S,T,U,Ü,V,W,X,Y,Z
        return explode(',', get_string('alphabet', 'langconfig'));
    }

    private function render_filters() {
        global $PAGE;
        $initialsbars = $this->render_initials_bars();
        $groupmenu = $this->render_group_menu();
        $searchfield = $this->render_search_field();

        //Check mapping settings
        $shownroles = [];
        foreach ($this->visibleroles as $role) {
            $a = get_config('rwth_participants_view', 'rolemapping_'.$role->shortname);
            if (get_config('rwth_participants_view', 'rolemapping_'.$role->shortname) === "0") {
                $shownroles [] = $role;
            }
        }
        $filters = new filters($initialsbars, $shownroles, $groupmenu, $searchfield);
        $output = $PAGE->get_renderer('local_rwth_participants_view');
        echo $output->render($filters);
    }

    private function render_group_menu() {
        global $USER;
        if (!$this->hasgroups) {
            // There are no groups in this course.
            return '';
        }

        // Special handling for when using separate groups, current user is in only one group
        // and can't access all groups. We don't want to display "Separate groups: Group X"
        // but rather just "Group X".
        if ($this->course->groupmode == SEPARATEGROUPS and !$this->aag) {
            $allowedgroups = groups_get_all_groups($this->course->id, $USER->id);
            if (empty($allowedgroups)) {
                // If user has no groups, don't display anything in place of group menu.
                return '';
            } else if (count($allowedgroups) == 1) {
                $group = reset($allowedgroups);
                $groupname = format_string($group->name);
                return html_writer::div($groupname, 'groupselector groupselector-plain');
            }
        }

        return self::groups_print_course_menu($this->course, $this->baseurl, true);
    }

    private function render_search_field() {
        global $OUTPUT;
        $searchbox = html_writer::start_tag('form', ['action' => 'participants.php', 'class' => 'd-inline-block searchform']);
        $searchbox .= html_writer::empty_tag('input', ['type' => 'hidden', 'name' => 'id', 'value' => $this->course->id]);
        $searchbox .= html_writer::start_tag('input', ['type' => 'text', 'id' => 'search', 'name' => $this->request[TABLE_VAR_SEARCH], 'value' => s($this->prefs['search']), 'class' => 'form-control me-2']);
        if (!empty($this->prefs['search'])) {
            $cancelsearchurl = clone $this->baseurl;
            $cancelsearchurl->param($this->request[TABLE_VAR_SEARCH], '');
            $searchbox .= $OUTPUT->action_icon(
                    $cancelsearchurl,
                    new pix_icon('e/cancel', get_string('cancelsearch', 'local_rwth_participants_view')),
                    null,
                    ['class' => 'cancelsearch']);
        }
        $searchbox .= html_writer::end_tag('input');
        $searchbox .= '&nbsp;';
        $searchbox .= html_writer::empty_tag('input', ['type' => 'submit', 'value' => get_string('search'), 'class' => 'btn btn-primary']);
        $searchbox .= html_writer::end_tag('form');
        return $searchbox;
    }

    /**
     * Copied from flexible_table but we put role and group filter buttons between the initials
     * bars and the participants table. Also we don't display the paging bar above the table.
     */
    function start_html() {

        // Render the dynamic table header.
        echo $this->get_dynamic_table_html_start();

        if (in_array(TABLE_P_TOP, $this->showdownloadbuttonsat)) {
            echo $this->download_buttons();
        }

        $this->wrap_html_start();
        // Start of main data table

        if (!$this->downloading) {
            $this->render_filters();
            echo '<form action="action_redir.php" method="post" id="participantsform">';
            echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
            echo '<input type="hidden" name="id" value="'.$this->course->id.'" />';
            echo '<input type="hidden" name="returnto" value="'.s($this->baseurl->out(false)).'" />';
            echo '<div class="d-inline-block">';
            echo $this->render_reset_button();
        }

        echo html_writer::start_tag('div', array('class' => 'no-overflow'));
        echo html_writer::start_tag('table', $this->attributes);
    }

    function finish_html() {
        global $PAGE;

        if (!$this->started_output) {
            // No data has been added to the table.

            $this->render_filters();
            echo '<form action="action_redir.php" method="post" id="participantsform">';
            echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
            echo '<input type="hidden" name="returnto" value="'.s($this->baseurl->out(false)).'" />';
            echo '<div class="d-inline-block">';
            echo $this->render_reset_button();

            $this->print_nothing_to_display();

            echo '</div>';
            echo '</form>';
            return;
        }

        // Print empty rows to fill the table to the current pagesize.
        // This is done so the header aria-controls attributes do not point to
        // non existant elements.
        $emptyrow = array_fill(0, count($this->columns), '');
        while ($this->currentrow < $this->pagesize) {
            $this->print_row($emptyrow, 'emptyrow');
        }

        echo html_writer::end_tag('tbody');
        echo html_writer::end_tag('table');
        echo html_writer::end_tag('div');
        $this->wrap_html_finish();

        if (in_array(TABLE_P_BOTTOM, $this->showdownloadbuttonsat)) {
            echo $this->download_buttons();
        }

        // Render the dynamic table footer.
        echo $this->get_dynamic_table_html_end();

        if ($this->canassign) {
            echo '<div class="buttons">';

            // With JavaScript enabled hide bulk operations bar by default.
            echo '<div class="bulk-operations-bar jsonly hidden">';
            echo $this->render_bulk_operations_bar(true);
            echo '</div>';
            echo '<noscript>';
            echo $this->render_bulk_operations_bar(false);
            echo '</noscript>';

            echo '</div>';
        }

        // Paging bar.
        if ($this->use_pages) {
            $pagingbar = new paging_bar($this->totalrows, $this->currpage, $this->pagesize, $this->baseurl);
            $pagingbar->pagevar = $this->request[TABLE_VAR_PAGE];
            $output = $PAGE->get_renderer('local_rwth_participants_view');
            echo $output->render($pagingbar);
        }

        echo '</div>';
        echo '</form>';
        if ($this->canassign) {
            $PAGE->requires->js_call_amd('local_rwth_participants_view/bulkoperations',
                    'init', ['participants', 'participantselected', 'participantsselected']);
        }
        $PAGE->requires->js_call_amd('local_rwth_participants_view/statusfield', 'init');

        echo $this->render_perpage_select();
    }

    /**
     * Renders the bulk operations bar.
     *
     * @param bool $js true for display in JavaScript enabled browsers, false otherwise
     * @return string html
     */
    private function render_bulk_operations_bar($js): string {
        $html = "";
        if ($js) {
            $html .= html_writer::tag('span', get_string('participantselected', 'local_rwth_participants_view'), ['class' => 'selectioncounter']);
        }

        $html .= html_writer::start_tag('span', [
            'class' => 'bulk-operations-bar-btns'
        ]);
        $html .= html_writer::empty_tag('input', [
            'type' => 'hidden',
            'name' => 'formaction',
            'value' => 'roleremove.php'
        ]);
        $html .= html_writer::empty_tag('input', [
            'type' => 'button',
            'role' => 'button',
            'class' => 'btn btn-secondary clear-selection-btn',
            'value' => get_string('clearselection', 'local_rwth_participants_view')
        ]);
        $html .= html_writer::empty_tag('input', [
            'type' => 'submit',
            'role' => 'button',
            'class' => 'btn btn-primary remove-role-btn',
            'value' => get_string('removerole', 'local_rwth_participants_view')
        ]);
        $html .= html_writer::end_tag('span');
        return $html;
    }

    /**
     * Copied from flexible_table but we initialize role, group and search filter preferences as well.
     */
    protected function initialise_table_preferences(): void {
        global $SESSION, $USER;

        parent::initialise_table_preferences();

        // Load any existing user preferences.
        if (isset($SESSION->flextable_extended[$this->uniqueid])) {
            $this->prefs = $SESSION->flextable_extended[$this->uniqueid];
            $this->prefs['groupfilter'] = (int) groups_get_course_group($this->course);
            $oldprefs = $this->prefs;
        }

        // Set up default preferences if needed.
        if (!$this->prefs) {
            $this->prefs['rolefilter'] = $this->get_default_role_filter();
            $this->prefs['groupfilter'] = (int) groups_get_course_group($this->course);
            $this->prefs['search'] = '';
            $this->prefs['perpage'] = self::DEFAULT_PAGE_SIZE;
        }

        // Reset preferences.
        if ($this->is_resetting_preferences()) {
            // Reset selected group.
            unset($SESSION->activegroup[$this->course->id]);

            $this->prefs['rolefilter'] = $this->get_default_role_filter();
            $this->prefs['groupfilter'] = (int) groups_get_course_group($this->course);
            $this->prefs['search'] = '';
        }

        if (!isset($oldprefs)) {
            $oldprefs = $this->prefs;
        }

        // Role filter.
        if (($role = optional_param($this->request[TABLE_VAR_ROLE], -1, PARAM_INT)) !== -1) {
            $this->prefs['rolefilter'] = $role;
        }
        // Group filter.
        if (($group = optional_param($this->request[TABLE_VAR_GROUP], -1, PARAM_INT)) !== -1) {
            // Don't allow students in courses with separate groups to see students who are not
            // in their groups.
            $allowedgroups = groups_get_all_groups($this->course->id, $USER->id);
            if ($this->course->groupmode == VISIBLEGROUPS || $this->aag ||
                    ($group != 0 && array_key_exists($group, $allowedgroups))) {
                $this->prefs['groupfilter'] = $group;
            }
        }
        // Search.
        if (($search = optional_param($this->request[TABLE_VAR_SEARCH], null, PARAM_RAW)) !== null) {
            $this->prefs['search'] = $search;
        }
        // Users per page.
        if (($perpage = optional_param($this->request[TABLE_VAR_PERPAGE], null, PARAM_RAW)) !== null) {
            $this->prefs['perpage'] = $perpage;
        }

        $this->save_extended_preferences($oldprefs);
    }

    protected function save_extended_preferences($oldprefs): void {
        global $SESSION;

        if ($this->prefs != $oldprefs) {
            $SESSION->flextable_extended[$this->uniqueid] = $this->prefs;
        }
        unset($oldprefs);
    }

    protected function can_be_reset() {
        global $SESSION;
        if (parent::can_be_reset()) {
            return true;
        }
        foreach ($this->prefs as $key => $val) {
            switch ($key) {
                case 'perpage':
                    // Perpage doesn't count for resetting table preferences.
                    break;
                case 'groupfilter':
                    if (isset($SESSION->activegroup[$this->course->id])) {
                        // Temporarily unset the activegroup to be able to check what
                        // the default activegroup would be.
                        $backup = $SESSION->activegroup[$this->course->id];
                        unset($SESSION->activegroup[$this->course->id]);
                        $defaultgroup = (int) groups_get_course_group($this->course);
                        $SESSION->activegroup[$this->course->id] = $backup;
                        if ($val != $defaultgroup) {
                            return true;
                        }
                    }
                    break;
                case 'rolefilter':
                    if ($val != $this->get_default_role_filter()) {
                        return true;
                    }
                    break;
                default:
                    if (!empty($val)) {
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    /**
     * initializes $this->userlist
     */
    public function setup() {
        $tablecolumns = array();
        $tableheaders = array();

        if (!$this->downloading && $this->canassign) {
            $tablecolumns[] = 'select';
            $tableheaders[] = html_writer::empty_tag('input', array('type' => 'checkbox', 'class' => 'bulkoperation', 'name' => 'all'));
        }
        if (!$this->downloading && $this->showoptionalcolumns[self::COLUMN_PICTURE]) {
            $tablecolumns[] = 'userpic';
            $tableheaders[] = get_string('userpic');
        }

        if ($this->showfullname) {
            $tablecolumns[] = 'fullname';
            $tableheaders[] = get_string('fullnameuser');
        } else {
            $tablecolumns[] = 'firstname';
            $tableheaders[] = get_string('firstname');
            $tablecolumns[] = 'lastname';
            $tableheaders[] = get_string('lastname');
        }

        if (!$this->downloading) {
            $tablecolumns[] = 'email';
            $tableheaders[] = get_string('email');
        }

        if ($this->downloading) {
            $tablecolumns[] = 'idnumber';
            $tableheaders[] = get_string('idnumber');
        }

        $tablecolumns[] = 'roles';
        $tableheaders[] = get_string('roles');

        if ($this->hasgroups) {
            $tablecolumns[] = 'groups';
            $tableheaders[] = get_string('groups');
        }

        $canreviewenroltime = has_capability('local/rwth_participants_view:viewenroltime', $this->context);
        if (!$this->downloading && $canreviewenroltime) {
            $tablecolumns[] = 'enroltime';
            $tableheaders[] = get_string('enroltime', 'local_rwth_participants_view');
        }

        if (enrolment_helper::uses_self_enrolment($this->course->id) && $this->canassign) {
            $tablecolumns[] = 'status';
            $tableheaders[] = get_string('status');
        }

        if (!$this->downloading && $this->canassign) {
            $tablecolumns[] = 'delete';
            $tableheaders[] = '';
        }

        $this->define_columns($tablecolumns);
        $this->define_headers($tableheaders);

        $this->sortable(true, 'lastname', SORT_ASC); // Default sort column.
        $this->no_sorting('select');
        $this->no_sorting('status');
        $this->no_sorting('delete');

        $this->set_attribute('cellspacing', '0');
        $this->set_attribute('class', 'generaltable generalbox participants');
        $this->set_attribute('data-region', 'participants-table');
        $this->set_attribute('data-table-uniqueid', $this->uniqueid);

        $this->initialbars(true);
        $this->set_default_per_page(self::DEFAULT_PAGE_SIZE);

        parent::setup();

        $filteredroles = [];

        foreach ($this->visibleroles as $role) {
            $count = 0;
            if ($this->prefs['rolefilter'] == 0 || $this->prefs['rolefilter'] == $role->id) {
                $filteredroles[] = $role;
            }
            foreach ($this->visibleroles as $roletoinclude) {
                if (get_config('rwth_participants_view', 'rolemapping_'.$roletoinclude->shortname) === $role->id && $this->prefs['rolefilter'] == $role->id) {
                    $filteredroles [] = $roletoinclude;
                    $count += $this->role_count($roletoinclude);
                } else if (get_config('rwth_participants_view', 'rolemapping_'.$roletoinclude->shortname) === $role->id) {
                    $count += $this->role_count($roletoinclude);
                }
            }
            $role->filtering = $this->prefs['rolefilter'] == $role->id;
            $role->count = $this->role_count($role) + $count;
            $role->filterurl = $this->role_url($role);
        }
        $this->filteredroles = $filteredroles;

        $this->get_data();
    }

    /*
     * Gets the users, active initials, match count and total user count.
     */
    protected function get_data() {
        $factory = new data\course_participants_view_factory($this->course->id);

        if (!$this->downloading) {
            // Perpage.
            $perpageint = $this->get_perpage_int();
            $factory->set_limit($perpageint);

            // Offset.
            $offset = 0;
            if ($perpageint !== data\course_participants_view_factory::LIMIT_NONE) {
                $offset = $this->currpage * $perpageint;
            }
            $factory->set_offset($offset);
        }

        // Sort column.
        $sortcol = $this->get_sort_column();
        switch ($sortcol) {
            case 'firstname':
                $sortcol = data\course_participants_view_factory::SORT_COL_FIRSTNAME;
                break;
            case 'lastname':
                $sortcol = data\course_participants_view_factory::SORT_COL_LASTNAME;
                break;
            case 'email':
                $sortcol = data\course_participants_view_factory::SORT_COL_EMAIL;
                break;
            case 'roles':
                $sortcol = data\course_participants_view_factory::SORT_COL_ROLE;
                break;
            case 'groups':
                $sortcol = data\course_participants_view_factory::SORT_COL_GROUP;
                break;
            case 'enroltime':
                $sortcol = data\course_participants_view_factory::SORT_COL_ENROLTIME;
                break;
            default:
                throw new coding_exception("unknown sort column $sortcol");
        }
        $factory->set_sort_column($sortcol);

        // Sort direction.
        $sortdir = $this->get_sort_direction();
        switch ($sortdir) {
            case SORT_DESC:
                $sortdir = data\course_participants_view_factory::SORT_DIR_DESC;
                break;
            case SORT_ASC:
                $sortdir = data\course_participants_view_factory::SORT_DIR_ASC;
                break;
            default:
                throw new coding_exception("unknown sort direction $sortdir");
        }
        $factory->set_sort_direction($sortdir);

        // Important note for filtering:
        // Filters are applied in the order they are added to the factory. The initials_filter also counts
        // whether a user exists for each given initial. In order for it to only work on users already
        // matching the other filters, the initials_filter must be added last.

        // Filter by group.
        if ($this->prefs['groupfilter'] > 0) {
            $factory->add_filter(new data\group_filter($this->prefs['groupfilter'], $this->context));
        }

        // Count users for role and group.
        $totalcounter = new data\usercount_filter();
        $factory->add_filter($totalcounter);

        // Filter by role(s).
        if ($this->prefs['rolefilter'] != 0) {
            $rolesfilter = new data\roles_filter($this->filteredroles);
            $factory->add_filter($rolesfilter);
        }

        // Filter by search term.
        if (!empty($this->prefs['search'])) {
            $factory->add_filter(new data\search_filter($this->prefs['search']));
        }

        // Filtering by initials.
        $initialfilter = new data\initials_filter($this->get_initial_first(), $this->get_initial_last());
        $initialfilter->enable_search_users_existing_per_initial(self::get_initials());
        $factory->add_filter($initialfilter);

        // Count users matching all filters.
        $matchcounter = new data\usercount_filter();
        $factory->add_filter($matchcounter);

        // Apply filters etc.
        $this->userlist = $factory->create();

        $this->ifirstenabled = $initialfilter->get_users_existing_per_initial_firstname();
        $this->ilastenabled = $initialfilter->get_users_existing_per_initial_lastname();
        $this->totalcount = $totalcounter->get();
        $this->matchcount = $matchcounter->get();
    }

    public function render(): string {
        global $CFG, $USER, $PAGE, $OUTPUT;

        ob_start();

        if ($this->downloading) {
            $this->set_table_exporting_as_csv('export-'.$this->course->shortname);
        }

        $matchcount = $this->get_match_count();
        $perpage = $this->prefs['perpage'];
        if ($perpage === 'all' || $perpage > $matchcount) {
            $perpage = $matchcount;
        }
        $this->pagesize($perpage, $matchcount);

        foreach ($this->userlist as $user) {
            $data = array();

            // Check which of the assigned roles are manually (un)assignable.
            // Grouped by enrolment plugin: enrol => array of (roleid => role)
            $assignableroles = [];
            foreach ($user->roles as $role) {
                if (array_key_exists($role->id, $this->assignableroles) || array_key_exists(get_config('rwth_participants_view', 'rolemapping_'.$role->shortname), $this->assignableroles)) {
                    $assigningcomponent = $user->roles[$role->id]->assigning_component;
                    $enrol = empty($assigningcomponent) ? 'manual' : preg_replace('/^enrol_/', '', $assigningcomponent);
                    if (!isset($assignableroles[$enrol])) {
                        $assignableroles[$enrol] = [];
                    }
                    $assignableroles[$enrol][$role->id] = $role;
                }
            }

            // Selection checkbox.
            if (!$this->downloading && $this->canassign) {
                $hasremovablerole = !empty($assignableroles['manual']);
                $unenrollable = false;
                if (empty($assignableroles) && $this->canenrol) {
                    foreach ($user->userenrolments as $ue) {
                        if (!array_key_exists($ue->enrolid, $user->enrolments)) {
                            // Shouldn't happpen.
                            continue;
                        }
                        $enrolinstance = $user->enrolments[$ue->enrolid];
                        if ($enrolinstance->enrol == 'manual') {
                            $unenrollable = true;
                            break;
                        }
                    }
                }
                if ($hasremovablerole || $unenrollable) {
                    $data[] = '<input type="checkbox" class="bulkoperation" name="user'.$user->id.'"/>';
                } else {
                    $data[] = '';
                }
            }

            // Picture.
            if (!$this->downloading && $this->showoptionalcolumns[self::COLUMN_PICTURE]) {
                $stduser = (object) get_object_vars($user); // Cast to stdClass.
                $data[] = $OUTPUT->user_picture($stduser, array('size' => 35, 'courseid' => $this->course->id));
            }

            // Name.
            $showname = true;
            if (get_config('rwth_participants_view', 'emaildisplaycontrolsnamedisplay')) {
                // We (ab)use the maildisplay setting to control display of names aswell.
                $showname = !$user->mailhidden;
            }
            if ($user->tutorhidden) {
                $showname = !$user->tutorhidden;
            }
            if ($showname) {
                $first = $user->firstname;
                $last = $user->lastname;
                $fullname = user_helper::get_fullname($user);

                if (!$this->downloading) {
                    if ($USER->id == $user->id || has_capability('moodle/user:viewdetails', $this->context)) {
                        $linkformat = '<a href="'.$CFG->wwwroot.'/user/view.php?id='.$user->id.'&amp;course='.$this->course->id.'">%s</a>';
                        $first = sprintf($linkformat, $first);
                        $last = sprintf($linkformat, $last);
                        $fullname = sprintf($linkformat, $fullname);
                    }
                    $boldformat = '<strong>%s</strong>';
                    $first = sprintf($boldformat, $first);
                    $last = sprintf($boldformat, $last);
                    $fullname = sprintf($boldformat, $fullname);
                }
            } else {
                $hiddenusertext = '<strong>'.get_string('hiddenusername', 'local_rwth_participants_view').'</strong>';
                $first = $hiddenusertext;
                $last = '';
                $fullname = $hiddenusertext;
            }
            if ($this->showfullname) {
                $data[] = $fullname;
            } else {
                $data[] = $first;
                $data[] = $last;
            }

            // Email address.
            $showemail = !$user->mailhidden;
            $showtutor = !$user->tutorhidden;
            if (!$this->downloading) {
                // Show email address only when not downloading.
                $data[] = $showemail && $showtutor ? $user->email : get_string('hiddenemailaddress', 'local_rwth_participants_view');
            }

            // ID number.
            if ($this->downloading) {
                // When downloading show idnumber instead of email address.
                $data[] = $user->idnumber;
            }

            // Roles.
            $roles = array();
            foreach ($user->roles as $role) {
                $roles[] = $this->downloading ? $role->shortname : $role->guiname;
            }
            $data[] = implode(', ', $roles);

            // Groups.
            if ($this->hasgroups) {
                $groupnames = [];
                foreach ($user->groups as $group) {
                    $groupnames[] = format_string($group->name);
                }
                asort($groupnames, SORT_NATURAL);
                $data[] = implode(', ', $groupnames);
            }

            // Time of enrolment.
            $canreviewenroltime = has_capability('local/rwth_participants_view:viewenroltime', $this->context);
            if (!$this->downloading && $canreviewenroltime) {
                $enrolments = array();
                foreach ($user->userenrolments as $ue) {
                    $enrolments[] = $ue->timecreated;
                }
                $data[] = date('d.m.Y H:i:s', min($enrolments));
            }

            // Status.
            if (enrolment_helper::uses_self_enrolment($this->course->id) && $this->canassign) {
                require_once($CFG->dirroot . '/enrol/locallib.php');
                $manager = new \course_enrolment_manager($PAGE, $this->course);
                $userenrolments = $manager->get_user_enrolments($user->id);
                $statusdata = '';
                $downloadstatus = array();
                foreach ($userenrolments as $ue) {
                    $timeenrolled = $ue->timecreated;
                    $actions = array();
                    $instancename = $ue->enrolmentinstancename;
                    $timestart = $ue->timestart;
                    $timeend = $ue->timeend;
                    $timeenrolled = $ue->timecreated;
                    switch ($ue->status) {
                        case ENROL_USER_ACTIVE:
                            $status = get_string('participationactive', 'enrol');
                            $statusval = status_field::STATUS_ACTIVE;
                            $currentdate = new DateTime();
                            $now = $currentdate->getTimestamp();
                            $isexpired = $timestart > $now || ($timeend > 0 && $timeend < $now);
                            $enrolmentdisabled = $ue->enrolmentinstance->status == ENROL_INSTANCE_DISABLED;
                            // If user enrolment status has not yet started/already ended or the enrolment instance is disabled.
                            if ($isexpired || $enrolmentdisabled) {
                                $status = get_string('participationnotcurrent', 'enrol');
                                $statusval = status_field::STATUS_NOT_CURRENT;
                            }
                            break;
                        case ENROL_USER_SUSPENDED:
                            $status = get_string('participationsuspended', 'enrol');
                            $statusval = status_field::STATUS_SUSPENDED;
                            break;
                    }
                    $statusfield = new status_field($instancename, format_string($this->course->fullname),
                                                    user_helper::get_fullname($user), $status, $timestart, $timeend, $actions, $timeenrolled);
                    $statusfielddata = $statusfield->set_status($statusval)->export_for_template($OUTPUT);
                    $statusdata .= $OUTPUT->render_from_template('core_user/status_field', $statusfielddata);
                    array_push($downloadstatus, $status);
                }
                if ($this->downloading) {
                    $data[] = implode(', ', $downloadstatus);
                } else {
                    $data[] = $statusdata;
                }
            }

            // Delete button.
            if (!$this->downloading && $this->canassign) {
                $deletebuttons = '';
                if (!empty($assignableroles)) {
                    // User has at least one role.
                    if (!empty($assignableroles['manual'])) {
                        foreach ($assignableroles['manual'] as $role) {
                            $isadmin = has_capability('moodle/site:config', $PAGE->context);
                            if ($role->shortname === 'l2pmanagerexternal' && !has_capability('moodle/site:config', $PAGE->context)) {
                                continue;
                            }
                            $strunassign = get_string('unassignarole', 'role', $role->guiname);
                            $icon = $OUTPUT->pix_icon('t/delete', $strunassign);
                            $url = new moodle_url($this->baseurl, array('action' => 'unassign',
                                    'roleid' => $role->id, 'user' => $user->id));
                            $deletebuttons .= html_writer::tag('div', html_writer::link($url, $icon,
                                    array('class' => 'unassignrolelink', 'rel' => $role->id, 'title' => $strunassign)),
                                    array('class' => 'role role_'.$role->id));
                        }
                    } else if (!empty($assignableroles['database'])) {
                        $deletebuttons = $OUTPUT->help_icon('dbenrolment', 'local_rwth_participants_view');
                    } else {
                        // Role assigned by unhandled enrol plugin. Display info that you can't
                        // manually unassign the role.
                        reset($assignableroles);
                        $enrol = key($assignableroles);
                        $icon = new output\help_icon('otherroleassignment', 'local_rwth_participants_view', $enrol);
                        $icon->diag_strings();
                        $deletebuttons = $OUTPUT->render($icon);
                    }
                } else {
                    // User has no roles in this course.

                    foreach ($user->userenrolments as $userenrolment) {
                        if (!array_key_exists($userenrolment->enrolid, $user->enrolments)) {
                            // Shouldn't happen normally.
                            continue;
                        }
                        $enrolment = $user->enrolments[$userenrolment->enrolid];
                        if ($enrolment->enrol == 'manual') {
                            // Manual enrolment can be removed.
                            $strunenrol = get_string('unenrol', 'enrol_manual');
                            $icon = $OUTPUT->pix_icon('t/delete', $strunenrol);
                            $url = new moodle_url($this->baseurl, ['action' => 'unenrol', 'user' => $user->id,
                                    'enrol' => $enrolment->id]);
                            $deletebuttons .= html_writer::tag('div', html_writer::link($url, $icon,
                                    ['class' => 'unenrollink', 'title' => $strunenrol]));
                        } else {
                            $icon = new output\help_icon('otherenrolment', 'local_rwth_participants_view', $enrolment->enrol);
                            $icon->diag_strings();
                            $deletebuttons .= $OUTPUT->render($icon);
                        }
                    }
                }
                $data[] = $deletebuttons;
            }

            $this->add_data($data);
        }

        if (!$this->downloading) {
            $this->finish_html();
        }

        $html = ob_get_clean();
        return $html;
    }

    /**
     * Tells this table that it's being downloaded as csv.
     *
     * Needs to be called after $table->setup() and before adding any data to the table.
     */
    private function set_table_exporting_as_csv() {
        $this->is_downloading('csv', $this->get_export_filename());

        $dataformat = table_dataformat_export_format_exposed::get_property($this->export_class_instance(),
                'dataformat');
        $csvwriter = csv_writer_exposed::get_property($dataformat, 'writer');
        $csvwriter->setFieldDelimiter(';');
    }

    private function get_export_filename(): string {
        $filename = $this->filenamewithoutextension;
        if (!empty($filename)) {
            // User supplied filename.
            return $filename;
        }
        $coursename = format_string($this->course->shortname);
        $filename = 'export'
                .'-'.preg_replace('/[[:^alnum:]]+/', '-', mb_strtolower($coursename))
                .'-'.implode('-and-', array_column($this->filteredroles, 'shortname'));

        if ($this->prefs['groupfilter'] > 0) {
            $group = groups_get_group($this->group);
            $groupname = format_string($group->name);
            $filename .= '-group='.mb_strtolower($groupname);
        }

        if (!empty($this->prefs['search'])) {
            $filename .= '-search='.$this->prefs['search'];
        }

        $ifirst = mb_strtolower($this->get_initial_first());
        if (!empty($ifirst)) {
            $filename .= "-ifirst=$ifirst";
        }

        $ilast = mb_strtolower($this->get_initial_last());
        if (!empty($ilast)) {
            $filename .= "-ilast=$ilast";
        }

        return $filename;
    }

    /**
     * Get list of hidden user fields.
     *
     * If the current user has the capability moodle/course:viewhiddenuserfields in the given context
     * this function will return an empty array.
     *
     * @return array hidden user fields
     */
    private function get_hidden_fields(): array {
        global $CFG;
        if (has_capability('moodle/course:viewhiddenuserfields', $this->context)) {
            return array();  // Teachers and admins are allowed to see everything.
        } else {
            return array_flip(explode(',', $CFG->hiddenuserfields));
        }
    }

    protected function get_perpage_int() {
        $perpagestr = $this->prefs['perpage'];
        if (is_numeric($perpagestr)) {
            $perpageint = intval($perpagestr);
        } else if ($perpagestr === 'all') {
            $perpageint = data\course_participants_view_factory::LIMIT_NONE;
        } else {
            $perpageint = self::DEFAULT_PAGE_SIZE;
        }
        return $perpageint;
    }

    protected function role_count($role) {
        $factory = new data\course_participants_view_factory($this->course->id);

        // Filter by role(s).
        $rolesfilter = new data\roles_filter([$role]);
        $factory->add_filter($rolesfilter);

        // Filter by group.
        if ($this->prefs['groupfilter'] > 0) {
            $factory->add_filter(new data\group_filter($this->prefs['groupfilter'], $this->context));
        }

        // Count users for role and group.
        $totalcounter = new data\usercount_filter();
        $factory->add_filter($totalcounter);

        // Filter by search term.
        if (!empty($this->prefs['search'])) {
            $factory->add_filter(new data\search_filter($this->prefs['search']));
        }

        // Filtering by initials.
        $initialfilter = new data\initials_filter($this->get_initial_first(), $this->get_initial_last());
        $initialfilter->enable_search_users_existing_per_initial(self::get_initials());
        $factory->add_filter($initialfilter);

        // Count users matching all filters.
        $matchcounter = new data\usercount_filter();
        $factory->add_filter($matchcounter);

        // Apply filters etc.
        $this->userlist = $factory->create();

        $totalcount = $totalcounter->get();
        $matchcount = $matchcounter->get();
        if ($totalcount === $matchcount) {
            $rolecount = $totalcount;
        } else {
            $rolecount = $matchcount.'/'.$totalcount;
        }
        return $rolecount;
    }

    protected function role_url($role) {
        if ($this->prefs['rolefilter'] == $role->id) {
            $param = 0;
        } else {
            $param = $role->id;
        }
        $params = [
            $this->request[TABLE_VAR_ROLE] => $param,
        ];
        return $this->baseurl->out(false, $params);
    }

    /**
     * Generate the HTML for the table preferences reset button.
     *
     * @return string HTML fragment, empty string if no need to reset
     */
    protected function render_reset_button() {

        if (!$this->can_be_reset()) {
            return '';
        }

        $url = $this->baseurl->out(false, array($this->request[TABLE_VAR_RESET] => 1));

        $html  = html_writer::start_div('resettable mdl-right');
        $html .= html_writer::link($url, get_string('resettable', 'local_rwth_participants_view'));
        $html .= html_writer::end_div();

        return $html;
    }

    /**
     * Renders the control that lets you change how many users are displayed per page.
     *
     * @return string html
     */
    protected function render_perpage_select(): string {
        global $OUTPUT;

        $perpage = $this->prefs['perpage'];
        $maxperpage = max($this->get_match_count(), self::DEFAULT_PAGE_SIZE);
        if ($perpage !== 'all') {
            $maxperpage = max($maxperpage, $perpage);
        }

        // Example: If we have 123 students in a course, display only these options: 10, 20, 50, 100, all.
        $perpageoptions = array();
        for ($i = 10; $i <= $maxperpage; $i *= 10) {
            $j = $i;
            if ($j <= $maxperpage) {
                $perpageoptions[$j] = $j;
            }

            $j = $i * 2;
            if ($j <= $maxperpage) {
                $perpageoptions[$j] = $j;
            }

            $j = $i * 5;
            if ($j <= $maxperpage) {
                $perpageoptions[$j] = $j;
            }
        }
        $perpageoptions['all'] = get_string('all', 'local_rwth_participants_view');

        // Add current $perpage value to options aswell if it's not in there yet.
        if (!array_key_exists($perpage, $perpageoptions)) {
            $perpageoptions[$perpage] = $perpage;
        }
        uksort($perpageoptions, "self::perpage_sort");

        $select = new output\rwth_single_select($this->baseurl, 'perpage', $perpageoptions, $perpage, null,
                'perpageoptions');
        $perpageselect = get_string('participantsperpage', 'local_rwth_participants_view', $OUTPUT->render($select));
        return html_writer::tag('div', $perpageselect, array('id' => 'perpageselect'));
    }

    /**
     * Sorts entries of the 'perpage' dropdown menu.
     *
     * Sorts numbers in ascending order and puts non-numeric text at the end.
     *
     * @param string $a first element
     * @param string $b second element
     * @return int positive number := $a > $b,
     *             negative number := $a < $b,
     *             0 := $a == $b
     */
    protected static function perpage_sort($a, $b): int {
        if (is_numeric($a) && is_numeric($b)) {
            return (int)$a - (int)$b;
        } else if (!is_numeric($a) && !is_numeric($b)) {
            return strcmp($a, $b);
        } else if (is_numeric($a)) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * This function is not part of the public api.
     */
    function print_nothing_to_display() {
        global $OUTPUT;

        // Render the dynamic table header.
        echo $this->get_dynamic_table_html_start();

        echo $OUTPUT->heading(get_string('nothingtodisplay'));

        // Render the dynamic table footer.
        echo $this->get_dynamic_table_html_end();
    }

    /**
     * Different roles should filter the list by a different role by default.
     */
    protected function get_default_role_filter() {
        global $USER;
        $allroles = role_helper::get_all_roles($this->course->id);
        try {
            $user = user_helper::get_user_with_id($this->course->id, $USER->id);

            // Get role of current user.
            if (empty($user->roles)) {
                // Current user is enrolled without a role.
                return 0;
            }
            $toproleid = array_keys($user->roles)[0];
            if (is_role_switched($this->course->id)) {
                $toproleid = $USER->access['rsw'][$this->context->path];
            }
            $toprole = $allroles[$toproleid]->shortname;

            // Load default active role setting.
            $defaultrolefilter = get_config('rwth_participants_view', 'default_rolefilter_'.$toprole);

            if (!array_key_exists($defaultrolefilter, $this->visibleroles)) {
                // Role is not visible. Just display the first visible one.
                $defaultrolefilter = array_keys($this->visibleroles)[0];
            }
        } catch (moodle_exception $e) {
            $defaultrolefilter = 0;
        }
        return $defaultrolefilter;
    }

    /**
     * Print group menu selector for course level.
     * (Borrowed from lib/grouplib) + Modified for use of custom single_select.
     *
     * @category group
     * @param stdClass $course course object
     * @param mixed $urlroot return address. Accepts either a string or a moodle_url
     * @param bool $return return as string instead of printing
     * @param bool $enablejs allows to disable autosubmission js
     * @return mixed void or string depending on $return param
     */
    private function groups_print_course_menu($course, $urlroot, $return=false) {
        global $USER, $OUTPUT;

        if (!$groupmode = $course->groupmode) {
            if ($return) {
                return '';
            } else {
                return;
            }
        }

        $context = context_course::instance($course->id);
        $aag = has_capability('moodle/site:accessallgroups', $context);

        $usergroups = array();
        if ($groupmode == VISIBLEGROUPS or $aag) {
            $allowedgroups = groups_get_all_groups($course->id, 0, $course->defaultgroupingid);
            // Get user's own groups and put to the top.
            $usergroups = groups_get_all_groups($course->id, $USER->id, $course->defaultgroupingid);
        } else {
            $allowedgroups = groups_get_all_groups($course->id, $USER->id, $course->defaultgroupingid);
        }

        $activegroup = groups_get_course_group($course, true, $allowedgroups);

        $groupsmenu = array();
        if (!$allowedgroups or $groupmode == VISIBLEGROUPS or $aag) {
            $groupsmenu[0] = get_string('allparticipants');
        }

        $groupsmenu += groups_sort_menu_options($allowedgroups, $usergroups);

        if ($groupmode == VISIBLEGROUPS) {
            $grouplabel = get_string('groupsvisible');
        } else {
            $grouplabel = get_string('groupsseparate');
        }

        if ($aag and $course->defaultgroupingid) {
            if ($grouping = groups_get_grouping($course->defaultgroupingid)) {
                $grouplabel = $grouplabel . ' (' . format_string($grouping->name) . ')';
            }
        }

        if (count($groupsmenu) == 1) {
            $groupname = reset($groupsmenu);
            $output = $grouplabel.': '.$groupname;
        } else {
            $select = new output\rwth_single_select(new moodle_url($urlroot), 'group',
                      $groupsmenu, $activegroup, null, 'selectgroup');
            $select->label = $grouplabel;
            $output = $OUTPUT->render($select);
        }

        $output = '<div class="groupselector">'.$output.'</div>';

        if ($return) {
            return $output;
        } else {
            echo $output;
        }
    }
}
