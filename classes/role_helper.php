<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2018 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view;

use context_course;
use moodle_url;

defined('MOODLE_INTERNAL') || die;

class role_helper {

    public static function get_all_roles(int $courseid = SITEID) {
        $parts = data\course_participants::get($courseid);
        $roles = $parts->roles;
        $rolenames = $parts->role2guirolename;

        foreach ($roles as $role) {
            if (array_key_exists($role->id, $rolenames)) {
                $role->guiname = $rolenames[$role->id];
            }
        }

        return $roles;
    }

    /**
     * Gives you all roles visible in course participant views.
     * 
     * Also adds the property 'guiname'.
     *
     * @param int $courseid ID of the course (needed due to possible role name customization)
     *
     * @return array visible roles
     */
    public static function get_visible_roles(int $courseid = SITEID) {
        $roles = self::get_all_roles($courseid);

        // Filter out only the visible roles.
        $roleidsvisible = explode(',', get_config('rwth_participants_view', 'visible_roles'));
        foreach ($roles as $role) {
            if (!in_array($role->id, $roleidsvisible)) {
                // Role is irrelevant for the participants view.
                unset($roles[$role->id]);
            }
        }

        // For students who are not in a group in a course using "separate groups" mode don't show other students.
        if (!self::can_see_other_students($courseid)) {
            $studentroleids = explode(',', get_config('rwth_participants_view', 'student_roles'));
            foreach ($roles as $role) {
                if (in_array($role->id, $studentroleids)) {
                    unset($roles[$role->id]);
                }
            }
        }

        return $roles;
    }

    /**
     * Gives you the role name for a certain role ID. If you provide a course ID as well, you
     * will git the roleid including possible course-specific role name customizations.
     *
     * @param int $roleid ID of the role
     * @param int $courseid ID of the course (needed due to possible role name customization)
     *
     * @return string role name
     */
    public static function get_rolename(int $roleid, int $courseid = SITEID) {
        $roles = self::get_all_roles($courseid);
        return $roles[$roleid]->guiname;
    }

    /**
     * Checks whether the current user can see other students in general. There is only one
     * case where he can't: being a student without a group in a course using "separate
     * groups" mode.
     *
     * @param int $courseid ID of the course
     *
     * @return bool whether the current user can see students at all
     */
    public static function can_see_other_students(int $courseid) {
        global $USER;
        $course = get_course($courseid);
        if ($course->groupmode != SEPARATEGROUPS) {
            return true;
        }
        $context = context_course::instance($courseid);
        if (has_capability('moodle/site:accessallgroups', $context)) {
            return true;
        }
        $groupsofuser = groups_get_all_groups($courseid, $USER->id);
        if (!empty($groupsofuser)) {
            return true;
        }
        return false;
    }
}
