<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/enrol/manual/lib.php');

use coding_exception;
use context_course;
use course_enrolment_manager;
use enrol_manual_plugin;
use enrol_user_button;
use moodle_url;

class rwth_enrol_manual_plugin extends enrol_manual_plugin {

    /**
     * Returns name of this enrol plugin
     * @return string
     */
    public function get_name() {
        return 'manual';
    }

    public function get_manual_enrol_participants_button(
        course_enrolment_manager $manager,
        int $courseid,
        string $btnclassname
    ) {
        global $PAGE;

        /* When JS is disabled clicking the enrol button will only reload the page. The previous behaviour
         * was: you were lead to a seperate page for enrolment where you had an unrestricted user search. */
        $link = new moodle_url($PAGE->url);

        $str = "";
        if($btnclassname == 'participant') {
            $str = get_string('participant', 'local_rwth_participants_view');
        } else if($btnclassname == 'participants') {
            $str = get_string('participants', 'local_rwth_participants_view');
        }
        $button = new rwth_enrol_user_button($link, $str, $btnclassname, 'get');
        $button->class .= ' enrol_manual_plugin';

        $context = context_course::instance($courseid);
        $arguments = array(
            'contextid' => $context->id,
            'btnclassname' => $btnclassname
        );

        if ($btnclassname == 'participant') {
            $PAGE->requires->js_call_amd('local_rwth_participants_view/singleenrolment', 'init', array($arguments));
        } else if($btnclassname == 'participants') {
            $PAGE->requires->js_call_amd('local_rwth_participants_view/multipleenrolment', 'init', array($arguments));
        }
        

        return $button;
    }

    public function can_add_instance($courseid) {
        global $DB;

        $context = context_course::instance($courseid, MUST_EXIST);
        // Don't require the original 'moodle/course:enrolconfig' capability.
        if (!has_capability('enrol/manual:config', $context)) {
            return false;
        }

        if ($DB->record_exists('enrol', ['courseid' => $courseid, 'enrol' => 'manual'])) {
            // Multiple instances not supported.
            return false;
        }

        return true;
    }

}