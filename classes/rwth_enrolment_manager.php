<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/enrol/locallib.php');

use context_course;
use core_user\fields;
use course_enrolment_manager;
use moodle_exception;
use stdClass;
use tool_sdapi\sitedirectory_api;
use user_picture;

class rwth_enrolment_manager extends course_enrolment_manager {

    const MAX_AMOUNT_SEARCH_RESULTS = 5;

    /**
     * Helper method used by {@link get_potential_users()} and {@link search_other_users()}.
     *
     * @param string $search the search term, if any.
     * @param bool $searchanywhere Can the search term be anywhere, or must it be at the start.
     * @return array with three elements:
     *     string list of fields to SELECT,
     *     string possible database joins for user fields
     *     string contents of SQL WHERE clause,
     *     array query params. Note that the SQL snippets use named parameters.
     */
    protected function get_basic_search_conditions($search, $searchanywhere) {
        global $DB, $CFG;

        // Get custom user field SQL used for querying all the fields we need (identity, name, and
        // user picture).
        $userfields = fields::for_identity($this->get_context(), false)->with_name()->with_userpic()
                ->excluding('username', 'lastaccess', 'maildisplay');
        ['selects' => $fieldselects, 'joins' => $fieldjoins, 'params' => $params, 'mappings' => $mappings] =
                (array)$userfields->get_sql('u', true, '', '', false);

        // Searchable fields are only the identity and name ones (not userpic, and without exclusions).
        $searchablefields = fields::for_identity($this->get_context(), false)->with_name()
                ->excluding('firstnamephonetic', 'lastnamephonetic', 'middlename', 'alternatename')->including('email');
        $searchable = array_fill_keys($searchablefields->get_required_fields(), true);

        // Add some additional sensible conditions
        $tests = array("u.id <> :guestid", 'u.deleted = 0', 'u.confirmed = 1', 'u.firstname NOT LIKE "anonfirstname%"', 'u.lastname NOT LIKE "anonlastname%"');
        $params['guestid'] = $CFG->siteguest;
        if (!empty($search)) {
            $words = preg_split('/\s|,+/', $search, -1, PREG_SPLIT_NO_EMPTY);
            foreach ($words as $i => $word) {

                // Include identity and name fields as conditions.
                $conditions = [];
                foreach ($mappings as $fieldname => $fieldsql) {
                    if (array_key_exists($fieldname, $searchable)) {
                        $conditions[] = $fieldsql;
                    }
                }
                if ($searchanywhere) {
                    $searchparam = '%' . $word . '%';
                } else {
                    $searchparam = $word . '%';
                }
                foreach ($conditions as $key => $condition) {
                    $conditions[$key] = $DB->sql_like($condition, ":con{$i}_{$key}", false);
                    if ($condition == 'u.idnumber' || $condition == 'u.email') {
                        // Registration numbers and email adresses must not be partially matched.
                        $params["con{$i}_{$key}"] = $word;
                    } else {
                        $params["con{$i}_{$key}"] = $searchparam;
                    }
                }
                $tests[] = '(' . implode(' OR ', $conditions) . ')';
            }
        }
        $wherecondition = implode(' AND ', $tests);

        $selects = $fieldselects . ', u.username, u.lastaccess, u.maildisplay';
        return [$selects, $fieldjoins, $params, $wherecondition];
    }

    /**
     * Gets an array of the users that can be enrolled in this course.
     *
     * @param int $enrolid
     * @param int $contextid
     * @param int $roleid
     * @param string $search
     * @return array users matching the search term
     */
    public function get_potential_users_rwth($enrolid, $contextid, $roleid, $search) {
        global $DB;

        [$ufields, $joins, $params, $wherecondition] = $this->get_basic_search_conditions($search, true);

        $fields = 'SELECT '.$ufields
                .",
                EXISTS(
                    SELECT ra.roleid
                    FROM {role_assignments} ra
                    WHERE ra.userid = u.id AND
                        ra.contextid = :contextid AND
                        ra.roleid = :roleid
                ) as has_role_already";
        $params['contextid'] = $contextid;
        $params['roleid'] = $roleid;
        $sql = "FROM {user} u
                      $joins
            LEFT JOIN {user_enrolments} ue ON (ue.userid = u.id AND ue.enrolid = :enrolid)
                WHERE $wherecondition";
        $params['enrolid'] = $enrolid;

        // Return one more than the max number of search results so that the caller can differentiate between
        // cases where the number of search results is the max number and those where it's above the max.
        $page = 0;
        $perpage = self::MAX_AMOUNT_SEARCH_RESULTS + 1;
        list($sort, $sortparams) = users_order_by_sql('u', $search, $this->get_context());
        $sql = "$fields $sql ORDER BY $sort";
        $params = array_merge($params, $sortparams);
        return $DB->get_records_sql($sql, $params, $page * $perpage, $perpage);
    }

    /**
     * Searches for one or more users.
     *
     * @param string $search search terms (separated by newlines or semicolons)
     * @param int $roleid roleid for which to check if the user has that role already
     * @return object search results separated by search outcome. example:
     * {
     *     enrollable: [
     *         {
     *             id: 123,
     *             fullname: 'Max Mustermann',
     *             email: 'max@mustermann.de',
     *             isemailunique: true,
     *             hasrolealready: false,
     *             idnumber: '',
     *             profileimageurl: 'https://moodle.rwth-aachen.de/theme/image.php/boost_campus_rwth/core/123456/u/f1',
     *             profileimageurlsmall: 'https://moodle.rwth-aachen.de/theme/image.php/boost_campus_rwth/core/123456/u/f2'
     *         }
     *     ],
     *     invitable: [
     *         {
     *              email: 'erika@musterfrau.de',
     *              isalreadyinvited: false
     *         }
     *     ],
     *     toomanyhits: [
     *         'max',
     *         'erika'
     *     ]
     *     notfound: [
     *         'maks',
     *         'ericka'
     *     ]
     * }
     */
    public function search_users_rwth($search, $roleid) {
        global $PAGE, $USER;
        $context = context_course::instance($this->course->id);
        require_capability('local/rwth_participants_view:enrol', $context);
        $caninvite = has_capability('local/rwth_participants_view:manageinvitations', $context);
        $enrol = enrolment_helper::ensure_manual_enrolment_instance_exists($this->course->id);

        $enrollable = [];
        $invitable = [];
        $toomanyhits = [];
        $notfound = [];

        $searches = preg_split('/(?:\r\n)|\r|\n|;/', $search);

        // Checks if the search terms exceed the threshold.
        $threshold = get_config('rwth_participants_view', 'enrol_threshold');
        if ($threshold < count($searches)) {
            $a = new stdClass();
            $a->numsearches = count($searches);
            $a->maxnumsearches = $threshold;
            throw new moodle_exception('enrolusers:confirmtoomanysearches', 'local_rwth_participants_view', '', $a);
        }

        $showregnum = []; // Keep track of the users whose registration number we can show.
        foreach ($searches as $searchterm) {
            $searchterm = trim($searchterm);
            if (empty($searchterm)) {
                continue;
            }

            $users = $this->get_potential_users_rwth($enrol->id, $context->id, $roleid, $searchterm);
            $hits = [];
            $countunassigneduser = 0;
            foreach ($users as $user) {
                if (!is_siteadmin($USER->id) && is_siteadmin($user->id)) {
                    // When searching for users to enrol only show admins if the searching user is an admin.
                    continue;
                }
                $isregnum = preg_match('/^([0-9]{1,6})$/', $searchterm) && $searchterm == $user->idnumber;
                if (array_key_exists($user->id, $showregnum)) {
                    $showregnum[$user->id] = $showregnum[$user->id] || $isregnum;
                } else {
                    $showregnum[$user->id] = $isregnum;
                }

                $userdetails = new stdClass();
                $userdetails->id = $user->id;
                $userdetails->fullname = trim(user_helper::get_fullname($user));
                $userdetails->email = trim($user->email);
                $userdetails->hasrolealready = $user->has_role_already;
                if (!$userdetails->hasrolealready) {
                    $countunassigneduser += 1;
                }
                $userdetails->idnumber = $user->idnumber;
                $pic = new user_picture($user);
                $pic->size = 1; // Size f1.
                $userdetails->profileimageurl = $pic->get_url($PAGE)->out(false);
                $pic->size = 0; // Size f2.
                $userdetails->profileimageurlsmall = $pic->get_url($PAGE)->out(false);

                $hits[] = $userdetails;
            }
            $numhits = count($hits);
            $maxhits = self::MAX_AMOUNT_SEARCH_RESULTS;
            if (0 < $numhits && $numhits <= $maxhits) {
                foreach ($hits as $hit) {
                    $uniquehit = $countunassigneduser === 1;
                    if (array_key_exists($hit->id, $enrollable)) {
                        $uniquehit = $uniquehit || $enrollable[$hit->id]->uniquehit;
                    }
                    $hit->uniquehit = $uniquehit;
                    $enrollable[$hit->id] = $hit;
                }
            } else if ($numhits === 0) {
                $email = filter_var($searchterm, FILTER_VALIDATE_EMAIL);
                if ($email && $caninvite) {
                    $extuser = new stdClass();
                    $extuser->email = $email;
                    $invitable[$email] = $extuser;
                } else {
                    $notfound[$searchterm] = $searchterm;
                }
            } else {
                $toomanyhits[$searchterm] = $searchterm;
            }
        }

        // Detect if there are duplicate accounts (= different accounts with same email address).
        $emailcount = [];
        foreach ($enrollable as $user) {
            if (array_key_exists($user->email, $emailcount)) {
                $emailcount[$user->email]++;
            } else {
                $emailcount[$user->email] = 1;
            }
        }
        foreach ($enrollable as $uid => $user) {
            $enrollable[$uid]->isemailunique = $emailcount[$user->email] == 1;
        }

        // If we searched by registration number, hide email address. Otherwise hide registration number.
        foreach ($enrollable as $uid => $user) {
            if ($showregnum[$user->id]) {
                $enrollable[$uid]->email = '';
            } else {
                $enrollable[$uid]->idnumber = '';
            }
        }

        // Detect users that have already been invited to this course.
        $sdapi = null;
        if (!empty($invitable)) {
            $sdapi = new sitedirectory_api();
        }
        foreach ($invitable as $key => $extuser) {
            $invitations = $sdapi->get_invitations_for_email($extuser->email, $this->course->id);
            $hasopeninvitation = false;
            foreach ($invitations as $invitation) {
                if ($invitation->status == sitedirectory_api::INVITATION_STATUS_OPEN) {
                    $hasopeninvitation = true;
                    break;
                }
            }
            $invitable[$key]->isalreadyinvited = $hasopeninvitation;
        }

        $result = new stdClass();
        $result->enrollable = $enrollable;
        $result->invitable = $invitable;
        $result->toomanyhits = $toomanyhits;
        $result->notfound = $notfound;
        return $result;
    }
}
