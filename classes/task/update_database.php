<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A scheduled task.
 *
 * @package    local_rwth_participants_view
 * @copyright  2021 Hendrik Donath, RWTH Aachen University <Donath@itc.rwth-aachen.de>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace local_rwth_participants_view\task;

use core\task\scheduled_task;

/**
 * 
 * @copyright  2021 Hendrik Donath, RWTH Aachen University <Donath@itc.rwth-aachen.de>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class update_database extends scheduled_task {

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('update_database_description', 'local_rwth_participants_view');
    }

    /**
     * Delete Entrys from the group_import table if the timemodified attribute is no longer in the timeframe for the maximum import.
     * 
     */
    public function execute() {
        global $DB;

        $records = $DB->get_records('rwth_group_import');
        $timeframe = get_config('rwth_participants_view', 'groupimport_timeframe');

        foreach($records as $record) {
            if (time()-$record->timemodified > $timeframe*60) {
                $DB->delete_records('rwth_group_import', array('id' => $record->id));
            }
        }

    }

}
