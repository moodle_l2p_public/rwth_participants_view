<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2018 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_rwth_participants_view;

defined('MOODLE_INTERNAL') || die;

use coding_exception;

class user_helper {

    public static function get_user_with_id(int $courseid, int $uid): data\course_participant_view {
        $factory = new data\course_participants_view_factory($courseid);
        $users = $factory->add_filter(new data\userid_filter($uid))->create();
        if (count($users) !== 1) {
            throw new coding_exception(count($users).' users with id '.$uid.' found in course. Expected exactly one.');
        }
        return $users[key($users)];
    }

    public static function get_fullname($user) {
        global $CFG;

        // Disable debugging for call to fullname(). The function expects all name fields
        // but since we only use firstname and lastname just suppress it.
        $debugbackup = $CFG->debug;
        if (isset($CFG->debugusers)) {
            $debugusersbackup = $CFG->debugusers;
            unset($CFG->debugusers);
        }
        unset($CFG->debug);
        unset($CFG->debugusers);

        $fullname = fullname($user);

        $CFG->debug = $debugbackup;
        if (isset($debugusersbackup)) {
            $CFG->debugusers = $debugusersbackup;
        }
        return $fullname;
    }
}