<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = [

        'local/rwth_participants_view:view' => [
                'riskbitmask' => RISK_PERSONAL,
                'captype' => 'read',
                'contextlevel' => CONTEXT_COURSE,
                'archetypes' => [
                        'manager' => CAP_ALLOW,
                        'editingteacher' => CAP_ALLOW,
                        'teacher' => CAP_ALLOW,
                        'student' => CAP_ALLOW
                ]
        ],
        'local/rwth_participants_view:enrol' => [
                'riskbitmask' => RISK_PERSONAL,
                'captype' => 'write',
                'contextlevel' => CONTEXT_COURSE,
                'archetypes' => [
                        'manager' => CAP_ALLOW,
                        'editingteacher' => CAP_PREVENT,
                        'teacher' => CAP_PREVENT,
                        'student' => CAP_PREVENT
                ]
        ],
        'local/rwth_participants_view:exportstudents' => [
                'riskbitmask' => RISK_PERSONAL,
                'captype' => 'read',
                'contextlevel' => CONTEXT_COURSE,
                'archetypes' => [
                        'manager' => CAP_ALLOW,
                        'editingteacher' => CAP_ALLOW,
                        'teacher' => CAP_ALLOW,
                        'student' => CAP_PREVENT
                ]
        ],
        'local/rwth_participants_view:api_canseeuserids' => [
                'riskbitmask' => RISK_PERSONAL,
                'captype' => 'read',
                'contextlevel' => CONTEXT_COURSE,
                'archetypes' => [
                        'manager' => CAP_ALLOW
                ]
        ],
        'local/rwth_participants_view:api_canqueryuserprofilefields' => [
                'riskbitmask' => RISK_PERSONAL,
                'captype' => 'read',
                'contextlevel' => CONTEXT_COURSE,
                'archetypes' => [
                        'manager' => CAP_ALLOW
                ]
        ],
        'local/rwth_participants_view:groupimport' => [
                'riskbitmask' => RISK_PERSONAL,
                'captype' => 'write',
                'contextlevel' => CONTEXT_COURSE,
                'archetypes' => [
                        'manager' => CAP_ALLOW
                ]
        ],
        'local/rwth_participants_view:enrol_candisablemail' => [
                'riskbitmask' => RISK_PERSONAL,
                'captype' => 'write',
                'contextlevel' => CONTEXT_COURSE,
                'archetypes' => []
        ],
        'local/rwth_participants_view:viewgroupmemberdata' => [
                'riskbitmask' => RISK_PERSONAL,
                'captype' => 'read',
                'contextlevel' => CONTEXT_COURSE,
                'archetypes' => [
                        'student' => CAP_ALLOW
                ]
        ],
        'local/rwth_participants_view:viewenroltime' => [
                'riskbitmask' => RISK_PERSONAL,
                'captype' => 'read',
                'contextlevel' => CONTEXT_COURSE,
                'archetypes' => [
                        'manager' => CAP_ALLOW,
                        'editingteacher' => CAP_PREVENT,
                        'teacher' => CAP_PREVENT,
                        'student' => CAP_PREVENT
                ]
        ],
        'local/rwth_participants_view:manageinvitations' => [
                'riskbitmask' => RISK_PERSONAL,
                'captype' => 'write',
                'contextlevel' => CONTEXT_COURSE,
                'archetypes' => [
                        'manager' => CAP_ALLOW,
                        'editingteacher' => CAP_PREVENT,
                        'teacher' => CAP_PREVENT,
                        'student' => CAP_PREVENT
                ]
        ],
        'local/rwth_participants_view:viewhiddentutors' => [
                'riskbitmask' => RISK_PERSONAL,
                'captype' => 'read',
                'contextlevel' => CONTEXT_COURSE,
                'archetypes' => [
                        'manager' => CAP_ALLOW,
                        'editingteacher' => CAP_ALLOW,
                        'teacher' => CAP_ALLOW,
                        'student' => CAP_PREVENT
                ]
        ],
        'local/rwth_participants_view:hiddenfromstudents' => [
                'riskbitmask' => RISK_PERSONAL,
                'captype' => 'read',
                'contextlevel' => CONTEXT_COURSE,
                'archetypes' => [
                        'manager' => CAP_PROHIBIT,
                        'editingteacher' => CAP_PROHIBIT,
                        'teacher' => CAP_PREVENT,
                        'student' => CAP_PROHIBIT
                ]
        ],
];
