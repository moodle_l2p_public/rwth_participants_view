<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

$observers = [
    [
        'eventname' => '\core\event\group_created',
        'callback' => '\local_rwth_participants_view\event\callback::course_event'
    ],
    [
        'eventname' => '\core\event\group_updated',
        'callback' => '\local_rwth_participants_view\event\callback::course_event'
    ],
    [
        'eventname' => '\core\event\group_deleted',
        'callback' => '\local_rwth_participants_view\event\callback::course_event'
    ],
    [
        'eventname' => '\core\event\group_member_added',
        'callback' => '\local_rwth_participants_view\event\callback::course_event'
    ],
    [
        'eventname' => '\core\event\group_member_removed',
        'callback' => '\local_rwth_participants_view\event\callback::course_event'
    ],
    [
        'eventname' => '\core\event\user_updated',
        'callback' => '\local_rwth_participants_view\event\callback::user_event'
    ],
    [
        'eventname' => '\core\event\user_enrolment_created',
        'callback' => '\local_rwth_participants_view\event\callback::course_event'
    ],
    [
        'eventname' => '\core\event\user_enrolment_deleted',
        'callback' => '\local_rwth_participants_view\event\callback::course_event'
    ],
    [
        'eventname' => '\core\event\user_enrolment_updated',
        'callback' => '\local_rwth_participants_view\event\callback::course_event'
    ],
    [
        'eventname' => '\core\event\role_assigned',
        'callback' => '\local_rwth_participants_view\event\callback::course_event'
    ],
    [
        'eventname' => '\core\event\role_unassigned',
        'callback' => '\local_rwth_participants_view\event\callback::course_event'
    ],
    [
        'eventname' => '\core\event\course_updated',
        'callback' => '\local_rwth_participants_view\event\callback::course_event'
    ],
];