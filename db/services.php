<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$functions = [
    'local_rwth_participants_view_add_users' => [
        'classname' => 'local_rwth_participants_view_external',
        'methodname' => 'add_users',
        'classpath' => 'local/rwth_participants_view/externallib.php',
        'description' => 'Add/invite users to the course',
        'ajax' => true,
        'type' => 'read',
        'capabilities' => 'local/rwth_participants_view:enrol'
    ]
];

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = [
    'RWTH participants view service' => [
            'functions' => ['local_rwth_participants_view_add_users'],
            'restrictedusers' => 0,
            'enabled' => 1
    ]
];
