<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/enrol/externallib.php');

use local_rwth_participants_view\data\course_invitations;
use local_rwth_participants_view\enrolment_helper;
use local_rwth_participants_view\rwth_enrolment_manager;
use local_rwth_participants_view\mail_helper;
use tool_sdapi\sitedirectory_api;

class local_rwth_participants_view_external extends external_api {

    public static function add_users_parameters() {
        return new external_function_parameters(
            [
                'params' => new external_single_structure(
                    [
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'roleid' => new external_value(PARAM_INT, 'role id'),
                        'userids' => new external_multiple_structure(
                            new external_value(PARAM_INT, 'id of user to add')
                        ),
                        'emails' => new external_multiple_structure(
                            new external_value(PARAM_EMAIL, 'email address of user to invite')
                        ),
                        'sendconfirmation' => new external_value(PARAM_BOOL, 'send booking confirmations', VALUE_OPTIONAL, true),
                    ]
                )
            ]
        );
    }

    public static function add_users($params) {
        global $PAGE, $DB;

        $params = self::validate_parameters(
            self::add_users_parameters(), ['params' => $params]
        );
        $params = $params['params'];

        // Safety checks.
        if ($params['courseid'] == SITEID) {
            throw new moodle_exception('invalidcourse');
        }
        require_login($params['courseid']);
        $context = context_course::instance($params['courseid'], MUST_EXIST);
        require_capability('local/rwth_participants_view:enrol', $context);
        $assignableroles = get_assignable_roles($context);
        if (!array_key_exists($params['roleid'], $assignableroles)) {
            throw new moodle_exception('role with id '.$params['roleid'].' is not assignable');
        }
        if (!has_capability('local/rwth_participants_view:enrol_candisablemail', $context)) {
            $params['sendconfirmation'] = true;
        }

        // Enrol internal users (i.e. those who already have a Moodle account).
        $enrolinstance = enrolment_helper::ensure_manual_enrolment_instance_exists($params['courseid']);
        foreach ($params['userids'] as $userid) {
            $users[] = $DB->get_record('user', array('id' => $userid), '*', MUST_EXIST);
        }
        if (!$startdate = get_config('enrol_manual', 'enrolstart')) {
            // Default to now if there is no system setting.
            $startdate = 4;
        }
        switch ($startdate) {
            case 4:
                // We mimic get_enrolled_sql round(time(), -2) but always floor as we want users to always access their
                // courses once they are enrolled.
                $timestart = intval(substr(time(), 0, 8) . '00') - 1;
                break;
            default:
                $today = time();
                $today = make_timestamp(date('Y', $today), date('m', $today), date('d', $today), 0, 0, 0);
                $timestart = $today;
                break;
        }
        $timeend = 0;
        $recovergrades = true;
        $course = $DB->get_record('course', ['id' => $params['courseid']], '*', MUST_EXIST);
        $manager = new rwth_enrolment_manager($PAGE, $course);
        $plugins = $manager->get_enrolment_plugins(true); // Do not allow actions on disabled plugins.
        if (!isset($plugins[$enrolinstance->enrol])) {
            throw new enrol_ajax_exception('enrolnotpermitted');
        }
        $plugin = $plugins[$enrolinstance->enrol];
        if ($plugin->allow_enrol($enrolinstance) && has_capability('enrol/'.$plugin->get_name().':enrol', $context)) {
            foreach ($users as $user) {
                $plugin->enrol_user($enrolinstance, $user->id, $params['roleid'], $timestart, $timeend, null, $recovergrades);
            }
        } else {
            throw new enrol_ajax_exception('enrolnotpermitted');
        }

        // Send confirmations to enrolled internal users.
        if ($params['sendconfirmation'] && !empty($users)) {
            mail_helper::send_booking_confirmations($users, $params['courseid'], $params['roleid']);
        }

        // Invite external users (i.e. those who don't have a Moodle account yet).
        $sdapi = null;
        if (!empty($params['emails'])) {
            $sdapi = new sitedirectory_api();
        }
        $role = $DB->get_record('role', ['id' => $params['roleid']], '*', MUST_EXIST);
        foreach ($params['emails'] as $email) {
            $couponcode = $sdapi->add_invitation($email, $course->id, $role->id, $role->name);
            $invitation = $sdapi->get_invitation($couponcode, $course->id);
            mail_helper::send_invitation($email, $couponcode, $course->id, $role->id, $invitation->expires);
        }
        course_invitations::clear_cache($course->id);

        return null;
    }

    public static function add_users_returns() {
        return null;
    }

}
