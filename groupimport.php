<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

define('NO_OUTPUT_BUFFERING', true);

require_once(__DIR__.'/../../config.php');

use core\output\notification;
use local_rwth_participants_view\groupimport_search_form;
use local_rwth_participants_view\groupimport_confirm_form;
use local_rwth_participants_view\data\course_participants;
use local_rwth_participants_view\mail_helper;

$courseid = required_param('id', PARAM_INT);
$confirmed = optional_param('confirm', 0, PARAM_BOOL);

$course = $DB->get_record('course', ['id' => $courseid], '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

require_course_login($course, false);
require_capability('local/rwth_participants_view:groupimport', $context);

$PAGE->set_url('/local/rwth_participants_view/groupimport.php', ['id' => $courseid]);
$PAGE->set_context($context);

$PAGE->set_title("$course->shortname: ".get_string('groupimport', 'local_rwth_participants_view'));
$PAGE->set_heading($course->fullname);
$PAGE->set_pagelayout('incourse');

$output = $PAGE->get_renderer('local_rwth_participants_view');
echo $output->header();

$output = $PAGE->get_renderer('local_rwth_participants_view');

// Should use this variable so that we don't break stuff every time a variable is added or changed.
$baseurl = new moodle_url('/local/rwth_participants_view/groupimport.php', ['id' => $course->id]);

core_php_time_limit::raise(HOURSECS);
raise_memory_limit(MEMORY_HUGE);

if ($confirmed) {
    $groupid = required_param('groupid', PARAM_INT);
    $unenrolled = optional_param_array('enrol', [], PARAM_INT);
    $enrolled = optional_param_array('import', [], PARAM_INT);
    $sendmails = 1;
    $countImportedUsers = count($unenrolled);
    if (has_capability('local/rwth_participants_view:enrol_candisablemail', $context)) {
        $sendmails = optional_param('sendenrolmentemails', 1, PARAM_BOOL);
    }

    // Check how many users the user imported to this course in the last hour and update the database record.
    $importhistory = $DB->get_records('rwth_group_import', array('userid' => $USER->id));
    $timeframe = get_config('rwth_participants_view', 'groupimport_timeframe');
    if (!empty($importhistory)) {
        foreach($importhistory as $import) {
            if(time() - $import->timemodified <= $timeframe*60) {
                $countImportedUsers += $import->numberimported;
            }
        }
    }
    
    $params = [
        'courseid' => $courseid,
        'context' => $context,
        'userid' => $USER->id,  
        'other' => array('countImportedUsers' => $countImportedUsers, 'timeframe' => $timeframe, 'importedThisImport' => count($unenrolled)),
    ];

    $event = \local_rwth_participants_view\event\users_imported::create($params);
    $event->trigger();

    // If more than 3000 users got imported show an error.
    $isadmin = has_capability('moodle/site:config', $context);
    $maximum = get_config('rwth_participants_view', 'groupimport_maximum');
    if ($countImportedUsers > $maximum && !$isadmin) {
        $admins = get_admins();
        $noreplyuser = core_user::get_noreply_user();
        $subject = "Zu großer Gruppenimport";
        $message = "Userid: ". $USER->id . 
        "\nZeitpunkt: " . userdate(time()) . 
        "\nKurs: " . $courseid;
        
        foreach($admins as $admin) {
            email_to_user($admin,$noreplyuser,$subject,$message);
        }  

        echo $output->notification_without_closebutton(get_string('groupimport_too_many_imports', 'local_rwth_participants_view', $timeframe), notification::NOTIFY_ERROR);
        echo $output->box_start('generalbox centerpara boxwidthnormal boxaligncenter');
        $courseurl = new moodle_url('/course/view.php', ['id' => $courseid]);
        echo $output->single_button($courseurl, get_string('groupimport_continue_to_course', 'local_rwth_participants_view'), 'get');
        echo $output->box_end();
        echo $output->footer();
        return;
    }

    $import = new stdClass();
    $import->userid = $USER->id;
    $import->numberimported = count($unenrolled);
    $import->timemodified = time();
    $DB->insert_record('rwth_group_import', $import);
    
    $starttime = microtime();

    if (!empty($unenrolled)) {
        $enrolmanual = enrol_get_plugin('manual');
        $instance = $DB->get_record('enrol', ['courseid' => $courseid, 'enrol' => 'manual']);
        if (!$instance) {
            $instanceid = $enrolmanual->add_default_instance($course);
            $instance = $DB->get_record('enrol', ['id' => $instanceid], '*', MUST_EXIST);
        }
        list($sql, $params) = $DB->get_in_or_equal(array_keys($unenrolled));
        $userfields = 'id, idnumber, firstname, lastname';
        if ($sendmails) {
            $userfields .= ', username, email, auth, suspended, deleted, emailstop';
        }
        $users = $DB->get_records_sql('SELECT '.$userfields.' FROM {user} WHERE id '.$sql
                .' AND suspended = 0 AND deleted = 0', $params);
        $roleid = get_config('rwth_participants_view', 'groupimport_role');
        foreach ($users as $user) {
            $a = $user->idnumber;

            $enrolmanual->enrol_user($instance, $user->id, $roleid);
            echo $output->notification_without_closebutton(get_string('groupimport_added_to_course', 'local_rwth_participants_view', $a), notification::NOTIFY_INFO);
            if ($sendmails) {
                mail_helper::send_booking_confirmations([$user], $courseid, $roleid);
            }

            $success = groups_add_member($groupid, $user->id);
            if ($success) {
                echo $output->notification_without_closebutton(get_string('groupimport_added_to_group', 'local_rwth_participants_view', $a), notification::NOTIFY_SUCCESS);
            } else {
                echo $output->notification_without_closebutton(get_string('groupimport_failed_to_add_to_group', 'local_rwth_participants_view', $a), notification::NOTIFY_ERROR);
            }
        }
        unset($users);
    }

    if ($enrolled) {
        $participants = course_participants::get($courseid);
        $users = array_intersect_key($participants->users, $enrolled);
        foreach ($users as $user) {
            $a = $user->idnumber;

            $success = groups_add_member($groupid, $user->id);
            if ($success) {
                echo $output->notification_without_closebutton(get_string('groupimport_added_to_group', 'local_rwth_participants_view', $a), notification::NOTIFY_SUCCESS);
            } else {
                echo $output->notification_without_closebutton(get_string('groupimport_failed_to_add_to_group', 'local_rwth_participants_view', $a), notification::NOTIFY_ERROR);
            }
        }
    }

    $difftime = microtime_diff($starttime, microtime());
    echo $output->notification_without_closebutton(get_string('groupimport_finished', 'local_rwth_participants_view', $difftime), notification::NOTIFY_SUCCESS);
    echo $output->box_start('generalbox centerpara boxwidthnormal boxaligncenter');
    $importurl = new moodle_url('/local/rwth_participants_view/groupimport.php', ['id' => $courseid, 'groupid' => $groupid]);
    $courseurl = new moodle_url('/course/view.php', ['id' => $courseid]);
    echo $output->single_button($importurl, get_string('groupimport_import_more', 'local_rwth_participants_view'), 'get');
    echo $output->single_button($courseurl, get_string('groupimport_continue_to_course', 'local_rwth_participants_view'), 'get');
    echo $output->box_end();
} else {
    $groupid = optional_param('groupid', 0, PARAM_INT);
    $form = new groupimport_search_form(null, ['id' => $courseid, 'groupid' => $groupid]);

    if ($fromform = $form->get_data()) {
        $participants = course_participants::get($courseid);

        $groupid = $fromform->group;
        $group = $participants->groups[$groupid];
        $groupname = format_string($group->name);

        $fromform->data = str_replace("\xc2\xa0", ' ', $fromform->data); // Unicode \xc2\xa0 == non-breaking space.
        $regnumbers = preg_split("/[ ,;\t\n\r]+/", $fromform->data, -1, PREG_SPLIT_NO_EMPTY);
        $regnumbers = array_unique($regnumbers);

        // Determine who is already enrolled, who isn't and who doesn't exist.
        $idnumbermap = array_column($participants->users, 'id', 'idnumber'); // Map of idnumber -> id.
        $enrolled = [];
        $notfound = [];
        foreach ($regnumbers as $regnumber) {
            if (array_key_exists($regnumber, $idnumbermap)) {
                $userid = $idnumbermap[$regnumber];
                $user = $participants->users[$userid];
                $enrolled[$regnumber] = $user;
            } else {
                $notfound[] = $regnumber;
            }
        }

        $unenrolled = [];
        if (!empty($notfound)) {
            list($sql, $params) = $DB->get_in_or_equal($notfound);
            $users = $DB->get_records_sql('SELECT id, idnumber FROM {user} WHERE idnumber '.$sql
                    .' AND suspended = 0 AND deleted = 0', $params);
            foreach ($users as $user) {
                $unenrolled[] = $user;
            }
            $unenrolledmatrnrs = array_column($unenrolled, 'idnumber');
            $notfound = array_diff($notfound, $unenrolledmatrnrs);
        }

        // TODO Two unhandled edge cases:
        // What if there are multiple users with the same registration number in the course?
        // What if there are multiple users with the same registration number, some of which are in the course and some aren't?

        $formdata = [
                'id' => $courseid,
                'groupid' => $groupid,
                'notfound' => $notfound,
                'unenrolled' => $unenrolled,
                'enrolled' => $enrolled,
        ];
        $confirmform = new groupimport_confirm_form($PAGE->url, $formdata);

        echo $OUTPUT->heading(get_string('groupimport_confirm', 'local_rwth_participants_view', $groupname));
        $confirmform->display();

    } else {
        echo $output->heading(get_string('groupimport', 'local_rwth_participants_view'));
        echo $output->box(get_string('groupimport_comment', 'local_rwth_participants_view'));
        $form->display();
    }
}

echo $output->footer();
