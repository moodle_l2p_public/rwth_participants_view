<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'RWTH-Teilnehmeransicht';

// Error codes.
$string['dbvendornotimplemented'] = 'Funktionalität ist für {$a}-Datenbanken nicht implementiert.';

// Capabilites.
$string['rwth_participants_view:view'] = 'Teilnehmerliste ansehen';
$string['rwth_participants_view:enrol'] = 'Nutzer*innen hinzufügen';
$string['rwth_participants_view:exportstudents'] = 'Studierendentabelle exportieren';
$string['rwth_participants_view:api_canseeuserids'] = 'Kann die Nutzerids sehen.';
$string['rwth_participants_view:api_canqueryuserprofilefields'] = 'Kann Nutzerprofil abfragen.';
$string['rwth_participants_view:managegroups'] = 'Gruppen verwalten';
$string['rwth_participants_view:groupimport'] = 'Studierende in Gruppe importieren';
$string['rwth_participants_view:enrol_candisablemail'] = 'Kann beim Import von Studierenden in Gruppe Buchungsbestätigungs-E-Mails deaktivieren';
$string['rwth_participants_view:viewgroupmemberdata'] = 'Studierende dürfen die Namen und Mails ihrer Gruppenmitglieder sehen';
$string['rwth_participants_view:viewenroltime'] = 'Sieht den Einschreibezeitpunkt von Teilnehmer*innen';
$string['rwth_participants_view:manageinvitations'] = 'Kann Nutzer*innen anhand von E-Mail-Adressen nach Moodle einladen und diese Einladungen verwalten';
$string['rwth_participants_view:hiddenfromstudents'] = 'Wird in der Teilnehmerliste vor Studierenden und Extra Nutzern versteckt.';
$string['rwth_participants_view:viewhiddentutors'] = 'Darf Nutzer mit der Capability rwth_participants_view:hiddenfromstudents sehen.';

// Settings.
$string['generalsettings'] = 'RWTH-Teilnehmeransicht';
$string['show_columns'] = 'Spalten anzeigen';
$string['show_columns_help'] = 'Welche optionalen Spalten in der Teilnehmerliste angezeigt werden sollen.';
$string['column_picture'] = 'Nutzerbild';
$string['emaildisplay_controls_namedisplay'] = 'E-Mail-Anzeige steuert Namensanzeige';
$string['emaildisplay_controls_namedisplay_help'] = 'Ob die Profil-Einstellung \'emaildisplay\' neben der E-Mail-Adresse auch den Namen verstecken soll.';
$string['restrict_group_filter_to_students'] = 'Gruppenfilter betrifft nur Studierende';
$string['restrict_group_filter_to_students_help'] = 'Wenn die Teilnehmerliste nach einer Gruppe gefiltert wird, sollen nur die Studierendenrollen nach dieser Gruppe gefiltert werden, d.h. sollen Manager-/Betreuerrollen unbeeinflusst vom Gruppenfilter bleiben?';
$string['visible_roles'] = 'Sichtbare Rollen';
$string['visible_roles_help'] = 'Welche Rollen angezeigt werden sollen.
<br /><b>Beachte:</b> Wenn hier neue Rollen ausgewählt werden, muss man diese Änderung zunächst speichern, bevor man den Standard-Reiter für diese weiter unten ändern kann.';
$string['roles_with_visible_user_info'] = 'Nutzer für alle sichtbar';
$string['roles_with_visible_user_info_help'] = 'Wessen Namen und E-Mail-Adressen sollen für alle Kursteilnehmer*innen sichtbar sein?';
$string['student_roles'] = 'Studierenden-Rollen';
$string['student_roles_help'] = 'Wähle alle Rollen aus, die als Studierende angesehen werden. Das legt u.a. fest, wer exportiert wird, wenn man auf den Export-Knopf drückt.';
$string['role_default_rolefilter'] = 'Standard-Rollenfilter für {$a}';
$string['role_default_rolefilter_help'] = 'Nach welcher Rolle standardmäßig für Nutzer*innen mit der Rolle {$a} gefiltert werden soll.';
$string['role_no_mapping'] = 'Kein Mapping';
$string['role_mapping'] = 'Rollen Mapping Einstellung für {$a}';
$string['role_mapping_help'] = 'Die Rolle {$a} sollte folgender Rolle zugeordent werden.';
$string['groupsize_visibility'] = 'Gruppengröße für Datensichtbarkeit';
$string['groupsize_visibility_help'] = 'Bis zu welcher Gruppengröße die Namen und Mailadressen anderer Gruppenmitglieder sichtbar sein dürfen.';
$string['enrol_settings_header'] = 'Einschreibeeinstellungen';
$string['enrol_default_role'] = 'Standardrolle für neue Nutzer';
$string['enrol_default_role_help'] = 'Wenn man jemanden zum Kurs hinzufügen möchte, ist diese Rolle standardmäßig ausgewählt.';
$string['enrol_threshold'] = 'Maximale Anzahl an Suchbegriffen';
$string['enrol_threshold_help'] = 'Die maximale Anzahl an Suchbegriffen pro Suche. Die Zahl wird im Platzhaltertext des Nutzersucheingabefelds angezeigt.';
$string['groupimport_settings_header'] = 'Import in Gruppe';
$string['groupimport_role'] = 'Rolle für neue Nutzer';
$string['groupimport_role_help'] = 'Wenn man jemanden in eine Gruppe importieren möchte und der-/diejenige noch nicht im Kurs ist, kann man ihn/sie mit dieser Rolle zum Kurs hinzufügen.';
$string['groupimport_threshold'] = 'Empfohlene Obergrenze';
$string['groupimport_threshold_help'] = 'Wie viele Nutzer sollte man maximal auf einmal importieren? Die Zahl wird im Platzhaltertext des Matrikelnummereingabefelds angezeigt.';

// Group import.
// Step 1.
$string['groupimport'] = 'Importiere Studierende in eine Gruppe';
$string['groupimport_comment'] = 'Mit dem Import für Gruppenmitglieder können auch Teilnehmende in den Lernraum gebucht werden.<br>Bitte nutzen Sie für die Buchung von Teilnehmenden primär die Anmeldeverfahren in RWTHonline.';
$string['groupimport_registration_numbers'] = 'Liste von Matrikelnummern';
$string['groupimport_registration_numbers_help'] = 'Liste von Matrikelnummern durch eines oder mehrere der folgenden Zeichen getrennt<ul><li>[,] Beistrich</li><li>[;] Strichpunkt</li><li>[ ] Leerzeichen</li><li>[\\n] Zeilensprung</li><li>[\\r] Wagenrücklauf</li><li>[\\t] Tabulator</li></ul>';
$string['groupimport_registration_numbers_placeholder'] = 'Bitte importieren Sie nicht mehr als {$a} Nutzer auf einmal.';
// Step 2.
$string['groupimport_confirm'] = 'Bestätige Import in Gruppe "{$a}"';
$string['enrol_confirmationemails'] = 'Buchungsbestätigung senden';
$string['enrol_confirmationemails_help'] = 'Sendet eine Buchungsbestätigung per E-Mail an alle hinzugefügten Nutzer*innen, die bislang nicht im Kurs sind.';
$string['groupimport_notfound'] = 'Nutzer nicht gefunden: {$a}';
$string['groupimport_unenrolled'] = 'Nutzer nicht im Kurs: {$a}';
$string['groupimport_enrolled'] = 'Nutzer im Kurs: {$a}';
$string['groupimport_heading_enrol'] = 'Zu Kurs hinzufügen?';
$string['groupimport_heading_regnumber'] = 'Matrikelnummer';
$string['groupimport_heading_searchterm'] = 'Suchbegriff';
$string['groupimport_confirm_button'] = 'Importieren';
// Step 3.
$string['groupimport_cannot_enrol_users'] = 'Kann keine Nutzer zum Kurs hinzufügen.';
$string['groupimport_added_to_course'] = '{$a} wurde zum Kurs hinzugefügt.';
$string['groupimport_added_to_group'] = '{$a} wurde zur Gruppe hinzugefügt.';
$string['groupimport_failed_to_add_to_group'] = '{$a} konnte nicht zur Gruppe hinzugefügt werden.';
$string['groupimport_finished'] = 'Import abgeschlossen ({$a} Sekunden)';
$string['groupimport_import_more'] = 'Mehr importieren';
$string['groupimport_continue_to_course'] = 'Weiter';
$string['groupimport_too_many_imports'] = 'Das Importieren wurde abgebrochen, weil Sie in den letzten {$a} Minuten zu viele Nutzer importiert haben.';
$string['groupimport_maximum'] = 'Maximale Importgröße';
$string['groupimport_maximum_help'] = 'Bestimmt die maximale Anzahl an Nutzern die importiert werden dürfen über einen bestimmten Zeitraum';
$string['groupimport_timeframe'] = 'Zeitspanne für den maximalen Import';
$string['groupimport_timeframe_help'] = 'Bestimmt für welchen Zeitraum der maximale Import nicht überschritten werden darf (Angabe in Minuten)';

// Bulk operations.
$string['participantselected'] = '1 Teilnehmer*in ausgewählt';
$string['participantsselected'] = '{$a} Teilnehmer*innen ausgewählt';
$string['invitationselected'] = '1 Einladung ausgewählt';
$string['invitationsselected'] = '{$a} Einladungen ausgewählt';
$string['clearselection'] = 'Auswahl aufheben';
$string['removerole'] = 'Rollen entziehen';
$string['renewinvitation'] = 'Einladung erneuern';
$string['revokeinvitation'] = 'Einladung zurückziehen';
$string['invitestatuschange'] = 'Einladungsstatus ändern';
$string['courseremoverole'] = 'Rolle(n) Kursteilnehmer*innen entziehen';
$string['bulkunassignconfirm'] = 'Wollen Sie wirklich den folgenden Teilnehmer*innen diese Rollen entziehen?';
$string['bulkrenewinvitationconfirm'] = 'Wollen Sie die folgenden Einladungen wirklich erneuern?';
$string['bulkrevokeinvitationconfirm'] = 'Wollen Sie die folgenden Einladungen wirklich zurückziehen?';

// Enrolment.
$string['messageprovider:enrolment_confirmations'] = 'Bestätigungen von Kurseinschreibungen';
// $string['enrolmentconfirmationtitle']
// $string['enrolmentconfirmationmessage']
$string['enrolusers'] = 'Teilnehmer*innen hinzufügen';
$string['enrolusers:usersearch'] = 'Teilnehmer*innen suchen';
$string['enrolusers:usersearch_help'] = 'Der Inhalt des Suchfelds wird anhand von Zeilenumbrüchen und Semikola in einzelne Suchausdrücke unterteilt. Diese können ihrerseits aus mehreren Wörtern bestehen. Für jeden Suchausdruck werden all die Nutzer*innen gefunden, bei denen jedes der Suchwörter in mindestens einem ihrer Profilfelder vorkommt. Gesucht wird in den Feldern Vorname, Nachname, E-Mail-Adresse und Matrikelnummer. Bei E-Mail-Adressen und Matrikelnummern muss jedoch die vollständige Adresse bzw. Ziffernfolge in die Suche eingegeben werden. Teilweise Übereinstimmungen werden für diese Felder nicht unterstützt.<br /><br />So wird beispielsweise der Suchbegriff "Max Mustermann" auch einen Nutzer mit Vornamen "Max Philipp" und Nachnamen "Mustermann" finden, da das Suchwort "Max" im Vornamen "Max Philipp" enthalten ist. Die Sucheingabe "max.mustermann@rwth" führt jedoch zu keinem Treffer, da die E-Mail-Adresse unvollständig ist.';
$string['enrolusers:usersearchhint'] = 'Externe Personen können anhand ihrer E-Mail-Adresse hinzugefügt werden.';
$string['enrolusers:usersearchplaceholder'] = 'Suche nach Namen, E-Mailadressen oder Matrikelnummern.
Ein Suchbegriff pro Zeile oder getrennt durch Semikolon.
Maximal {$a} Suchbegriffe.';
$string['enrolusers:confirmaddable'] = 'Die folgenden <strong>{$a->num}</strong> Personen können als "{$a->role}" zum Kurs hinzugefügt werden.';
$string['enrolusers:confirmaddablesingular'] = 'Die folgende Person kann als "{$a}" zum Kurs hinzugefügt werden.';
$string['enrolusers:confirmaddableorinvitable'] = 'Die folgenden <strong>{$a->num}</strong> Personen können als "{$a->role}" zum Kurs hinzugefügt oder eingeladen werden.<br/>Mit dem Coupon aus der Einladungs-Mail kann ein Account für RWTHmoodle erstellt werden.';
$string['enrolusers:confirmaddableorinvitablesingular'] = 'Die folgende Person kann als "{$a}" zum Kurs hinzugefügt oder eingeladen werden.<br/>Mit dem Coupon aus der Einladungs-Mail kann ein Account für RWTHmoodle erstellt werden.';
$string['enrolusers:heading_name'] = 'Teilnehmer*in';
$string['enrolusers:heading_action'] = 'Aktion';
$string['enrolusers:action_add'] = 'hinzufügen';
$string['enrolusers:action_invite'] = 'einladen';
$string['enrolusers:alreadyadded'] = 'bereits hinzugefügt';
$string['enrolusers:alreadyinvited'] = 'bereits eingeladen';
$string['enrolusers:duplicateaccount'] = 'doppelter Account';
$string['enrolusers:confirmtoomanyhits'] = 'Für die folgenden <strong>{$a}</strong> Suchbegriffe wurden jeweils zu viele Personen gefunden.';
$string['enrolusers:confirmtoomanyhitssingular'] = 'Für den folgenden Suchbegriff wurden zu viele Personen gefunden.';
$string['enrolusers:confirmtoomanysearches'] = 'Es wurden {$a->numsearches} Suchbegriffe eingegeben, es sind aber nur {$a->maxnumsearches} pro Suche erlaubt.';
$string['enrolusers:confirmnotfound'] = 'Die folgenden <strong>{$a}</strong> Personen wurden nicht gefunden.';
$string['enrolusers:confirmnotfoundsingular'] = 'Die folgende Person wurden nicht gefunden.';
$string['enrolusers:confirmnotfoundhint'] = 'Externe Personen können nur anhand ihrer E-Mail-Adresse zum Kurs eingeladen werden.';
$string['enrolusers:confirmaddselected'] = 'Ausgewählte hinzufügen';
$string['enrolusers:useraddedorinvited'] = '<strong>1 Teilnehmer*in</strong> wurde erfolgreich hinzugefügt oder eingeladen.';
$string['enrolusers:usersaddedorinvited'] = '<strong>{$a} Teilnehmer*innen</strong> wurden erfolgreich hinzugefügt oder eingeladen.';
$string['enrolusers:bookingconfirmationsent'] = 'Buchungsbestätigung wurde gesendet.';
$string['enrolusers:bookingconfirmationssent'] = 'Buchungsbestätigungen wurden gesendet.';

$string['all'] = 'alle';
$string['exportstudents'] = 'Studierende exportieren';
$string['participantsperpage'] = 'zeige {$a} Teilnehmer*innen pro Seite';
$string['cachedef_course_participants'] = 'Zwischengespeicherte Kursteilnehmer';
$string['cachedef_course_invitations'] = 'Zwischengespeicherte Einladungen';
$string['hiddenusername'] = '&lt;Name versteckt&gt;';
$string['hiddenemailaddress'] = '&lt;E-Mail-Adresse versteckt&gt;';
$string['dbenrolment'] = 'Rollenzuweisung';
$string['dbenrolment_help'] = 'Diese Rollenzuweisung wird von RWTHonline synchronisiert. Falls Sie die Rolle entfernen wollen, müssen Sie das in RWTHonline tun und warten bis die Änderung in Moodle ankommt (kann bis zu zwei Stunden dauern).';
$string['otherroleassignment'] = 'Rollenzuweisung';
$string['otherroleassignment_help'] = 'Diese Rolle wurde durch das Plugin "{$a}" zugewiesen. Sie kann nur durch dieses wieder entfernt werden.';
$string['otherenrolment'] = 'Einschreibung';
$string['otherenrolment_help'] = 'Diese Person wurde durch das Plugin "{$a}" zum Kurs hinzugefügt. Sie kann nur durch dieses wieder entfernt werden.';
$string['cancelsearch'] = 'Suche leeren';

$string['privacy:metadata'] = 'Dieses Plugin ändert nur das Aussehen der Teilnehmerliste.';

// Tasks
$string['update_database_description'] = 'Veraltete Einträge aus der group_import Tabelle löschen.';

$string['sent_invitations'] = 'Gesendete Einladungen';
$string['participants_gendered'] = 'Teilnehmer*innen';

$string['resettable'] = 'Einstellungen zurücksetzen';
$string['enroltime'] = 'Hinzugefügt am';
$string['notingroup'] = 'Aufgrund der Kurseinstellungen sind nicht alle Kursteilnehmer*innen sichtbar.';
$string['invited_by'] = 'Eingeladen durch';
$string['unknown'] = 'Unbekannt';

$string['invitestatusopen'] = 'Läuft ab am {$a}';
$string['invitestatusrevoked'] = 'Zurückgezogen';
$string['invitestatusexpired'] = 'Abgelaufen';
$string['invitestatuscompleted'] = 'Abgeschlossen';

$string['sdfail_banner'] = 'Die Teilnehmerliste konnte nicht mit RWTHonline synchronisiert werden.<br>Sie können den Status versendeter Einladungen nicht einsehen und keine Teilnehmer*innen einladen.';
$string['sdfail_inv_title'] = '<i>Einladungen nicht verfügbar</i>';
