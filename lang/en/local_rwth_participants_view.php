<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'RWTH Participants view';

// Error codes.
$string['dbvendornotimplemented'] = 'Functionality is not implemented for {$a} Databases.';

// Capabilites.
$string['rwth_participants_view:view'] = 'View participants list';
$string['rwth_participants_view:enrol'] = 'Enrol users';
$string['rwth_participants_view:exportstudents'] = 'Export student table';
$string['rwth_participants_view:managegroups'] = 'Manage groups';
$string['rwth_participants_view:api_canseeuserids'] = 'Can see User IDs.';
$string['rwth_participants_view:api_canqueryuserprofilefields'] = 'Can query user profile fields.';
$string['rwth_participants_view:groupimport'] = 'Import students into group';
$string['rwth_participants_view:enrol_candisablemail'] = 'Can disable booking confirmation email when importing students into group';
$string['rwth_participants_view:viewgroupmemberdata'] = 'Students can see the name and email of groupmembers';
$string['rwth_participants_view:viewenroltime'] = 'Can see the enrolment times of participants';
$string['rwth_participants_view:manageinvitations'] = 'Can invite users to Moodle using only their email addresses and can manage these invitations';
$string['rwth_participants_view:hiddenfromstudents'] = 'Will be hidden from students or extra users viewing the participants list.';
$string['rwth_participants_view:viewhiddentutors'] = 'Is abled to view users with capability rwth_participants_view:hiddenfromstudents.';


// Settings.
$string['generalsettings'] = 'RWTH Participants view';
$string['show_columns'] = 'Show columns';
$string['show_columns_help'] = 'Which optional columns should be shown in the participants list.';
$string['column_picture'] = 'User picture';
$string['emaildisplay_controls_namedisplay'] = 'Email display controls name display';
$string['emaildisplay_controls_namedisplay_help'] = 'Whether the \'emaildisplay\' user profile setting should hide the name aswell as the email address.';
$string['restrict_group_filter_to_students'] = 'Group filter only applies to students';
$string['restrict_group_filter_to_students_help'] = 'When the participants list is filtered by a group, should only the student roles be filtered by that role, i.e. should the manager/tutor roles be unaffected by the group filter?';
$string['visible_roles'] = 'Visible roles';
$string['visible_roles_help'] = 'Which roles should be displayed.
<br /><b>Note:</b> When checking any new roles here you need to save this change first before you can set the default visible tab for them below.';
$string['roles_with_visible_user_info'] = 'Users visible to all';
$string['roles_with_visible_user_info_help'] = 'Whose names and email addresses should be visible to all other course participants.';
$string['student_roles'] = 'Student roles';
$string['student_roles_help'] = 'Select all roles that are considered students. This determines e.g. who gets exported when clicking the export button.';
$string['role_default_rolefilter'] = 'Default role filter for {$a}';
$string['role_default_rolefilter_help'] = 'By which role we should filter by default for users with the role {$a}.';
$string['role_no_mapping'] = 'No Mapping';
$string['role_mapping'] = 'Role mapping settings for {$a}';
$string['role_mapping_help'] = 'The role {$a} should be included in which role.';
$string['groupsize_visibility'] = 'Groupsize for data visibility';
$string['groupsize_visibility_help'] = 'Until which groupsize should the names and mail adresses of other groupmembers be visible.';
$string['enrol_settings_header'] = 'Enrol settings';
$string['enrol_default_role'] = 'Default role for new users';
$string['enrol_default_role_help'] = 'When you want to add someone to the course, this is the role that is selected by default.';
$string['enrol_threshold'] = 'Maximum amount of search terms';
$string['enrol_threshold_help'] = 'The maximum number of search terms in a single search. This is displayed in the placeholder text of the user search input field.';
$string['groupimport_settings_header'] = 'Import into group';
$string['groupimport_role'] = 'Role for new users';
$string['groupimport_role_help'] = 'When you want to import someone into a group and that person is not yet in the course, you can add him/her to the course giving him/her this role.';
$string['groupimport_threshold'] = 'Recommended upper limit';
$string['groupimport_threshold_help'] = 'What\'s the maximum number of users to import at once? This is displayed in the placeholder text of the registration number input field.';

// Group import.
// Step 1.
$string['groupimport'] = 'Import students into a group';
$string['groupimport_comment'] = 'With the import for group members, participants can also be added to the course room.<br>Please primarily use the registration procedures in RWTHonline for adding participants.';
$string['groupimport_registration_numbers'] = 'List of registration numbers';
$string['groupimport_registration_numbers_help'] = 'List of registration numbers separated by one or more of the following characters <ul><li>[,] comma</li><li>[;] semicolon</li><li>[ ] space</li><li>[\n] newline</li><li>[\r] carriage return</li><li>[\t] tabulator</li></ul>';
$string['groupimport_registration_numbers_placeholder'] = 'Please don\'t import more than {$a} users at once.';
// Step 2.
$string['groupimport_confirm'] = 'Confirm import into group "{$a}"';
$string['enrol_confirmationemails'] = 'Send booking confirmation';
$string['enrol_confirmationemails_help'] = 'Send a booking confirmation email to all newly added users that aren\'t yet in this course.';
$string['groupimport_notfound'] = 'Users not found: {$a}';
$string['groupimport_unenrolled'] = 'Users not in this course: {$a}';
$string['groupimport_enrolled'] = 'Users in this course: {$a}';
$string['groupimport_heading_enrol'] = 'Add to course?';
$string['groupimport_heading_regnumber'] = 'Registration number';
$string['groupimport_heading_searchterm'] = 'Search term';
$string['groupimport_confirm_button'] = 'Import';
// Step 3.
$string['groupimport_cannot_enrol_users'] = 'Can\'t add users to the course.';
$string['groupimport_added_to_course'] = '{$a} was added to the course.';
$string['groupimport_added_to_group'] = '{$a} was added to the group.';
$string['groupimport_failed_to_add_to_group'] = '{$a} could not be added to the group.';
$string['groupimport_finished'] = 'Import finished ({$a} seconds)';
$string['groupimport_import_more'] = 'Import more';
$string['groupimport_continue_to_course'] = 'Continue';
$string['groupimport_too_many_imports'] = 'The import was canceled because you imported too many users in the last {$a} minutes.';
$string['groupimport_maximum'] = 'Maximum import size';
$string['groupimport_maximum_help'] = 'Determines the maximum number of users that can be imported over a certain period of time.';
$string['groupimport_timeframe'] = 'Time span for the maximum import';
$string['groupimport_timeframe_help'] = 'Determines for which period the maximum import may not be exceeded (specified in minutes).';

// Bulk operations.
$string['participantselected'] = '1 participant selected';
$string['participantsselected'] = '{$a} participants selected';
$string['invitationselected'] = '1 invitation selected';
$string['invitationsselected'] = '{$a} invitations selected';
$string['clearselection'] = 'Clear selection';
$string['removerole'] = 'Revoke roles';
$string['renewinvitation'] = 'Renew invitation';
$string['revokeinvitation'] = 'Revoke invitation';
$string['invitestatuschange'] = 'Change invite status';
$string['courseremoverole'] = 'Revoke role(s) from course users';
$string['bulkunassignconfirm'] = 'Do you really want to unassign the following roles from these users?';
$string['bulkrenewinvitationconfirm'] = 'Do you really want to renew the following invitations?';
$string['bulkrevokeinvitationconfirm'] = 'Do you really want to revoke the following invitations?';

// Enrolment.
$string['messageprovider:enrolment_confirmations'] = 'Confirmation of course enrolments';
$string['enrolmentconfirmationtitle'] = '[{$a}] Buchungsbestätigung / Booking Confirmation';
$string['enrolmentconfirmationmessage'] = 'See below for an English version.

-- DEUTSCH ---------------------------------------------------

Guten Tag {$a->enrolled_person_fullname},

Sie wurden in einen Lernraum von {$a->sitename}, der Lehr- und Lernplattform der RWTH Aachen, gebucht.

Veranstaltung: {$a->coursename_de}
Veranstaltungskennung: {$a->courseidnumber}
Ihre Rolle: {$a->rolename}
Hinzugefügt von: {$a->enrolling_person_fullname} ({$a->enrolling_person_email})

Sie erreichen den Lernraum über die {$a->sitename}-Startseite <a href="{$a->siteurl}">{$a->siteurl}</a> oder direkt über den Link <a href="{$a->courseurl}">{$a->courseurl}</a>.

Diese E-Mail wurde von {$a->sitename} automatisch versendet. Bei Fragen wenden Sie sich bitte an <a href="mailto:{$a->support}">{$a->support}</a>.

-- ENGLISH ---------------------------------------------------

Dear {$a->enrolled_person_fullname},

You have been booked into a course room of RWTH Aachen University\'s eLearning and eTeaching platform {$a->sitename}.

Course: {$a->coursename_en}
Course ID: {$a->courseidnumber}
Your role: {$a->rolename}
Added by: {$a->enrolling_person_fullname} ({$a->enrolling_person_email})

You can access the course room either via the {$a->sitename} home page <a href="{$a->siteurl}">{$a->siteurl}</a> or directly via the following link <a href="{$a->courseurl}">{$a->courseurl}</a>.

This email was generated automatically by {$a->sitename}. In case you have questions please write to <a href="mailto:{$a->support}">{$a->support}</a>.';
$string['invitationtitle'] = '[{$a->sitename}] Einladung zum Lernraum {$a->coursename_de} / [{$a->sitename}] Invitation to course room {$a->coursename_en}';
$string['invitationmessage'] = 'See below for an English version.


-- DEUTSCH ---------------------------------------------------


Guten Tag,

Sie wurden zu einem Lernraum in {$a->sitename}, der Lehr- und Lernplattform der RWTH Aachen, eingeladen.

Veranstaltung: {$a->coursename_de}
Veranstaltungskennung: {$a->courseidnumber}
Ihre Rolle: {$a->rolename_de}
Eingeladen von: {$a->enrolling_person_fullname} (<a href="mailto:{$a->enrolling_person_email}">{$a->enrolling_person_email}</a>)


Bitte klicken Sie auf den folgenden Link, um die Einladung anzunehmen. Bitte beachten Sie: Es kann bis zu 30 Minuten dauern, bis die Einladung angenommen bzw. der Coupon eingelöst werden kann.

<a href="https://idm.rwth-aachen.de/connectMe/redeemcoupon?3&couponCode={$a->couponcode}">https://idm.rwth-aachen.de/connectMe/redeemcoupon?3&couponCode={$a->couponcode}</a>

Sollte das Eingabefeld auf der Zielseite "Coupon einlösen" leer sein, geben Sie dort bitte den folgenden Coupon selbst ein:

{$a->couponcode}

Nach Eingabe des Coupons werden Sie Schritt für Schritt durch die Anwendung geführt. Die Einladung ist bis einschließlich {$a->expirationdate_de} gültig oder bis zur erfolgreichen Annahme der Einladung.


Wenn Sie bereits einen Account für den RWTH Single Sign-On besitzen (Muster: ab123456), akzeptieren Sie die Einladung bitte mit diesem Account.

Wenn Sie noch keinen solchen Account besitzen, können Sie über den Link einen neuen Account im Identity Management der RWTH Aachen erstellen.

Nachdem Sie die Einladung akzeptiert und gegebenenfalls einen neuen Account erstellt haben, werden Sie innerhalb der nächsten 60 Minuten automatisch dem oben genannten Lernraum hinzugefügt und haben Zugriff auf alle darin befindlichen Materialien und Aktivitäten. Sie erhalten eine Benachrichtigung per E-Mail, sobald Sie Zugriff haben.


Sie erreichen den Lernraum über die {$a->sitename}-Startseite <a href="{$a->siteurl}">{$a->siteurl}</a> oder direkt über den Link <a href="{$a->courseurl}">{$a->courseurl}</a>.
Diese E-Mail wurde von {$a->sitename} automatisch versendet. Bei Fragen wenden Sie sich bitte an <a href="mailto:{$a->support}">{$a->support}</a>.


Mit freundlichen Grüßen
Ihr IT Center der RWTH Aachen University
--
IT Center
RWTH Aachen University
Seffenter Weg 23
52074 Aachen
Tel.: + 49 241 80 24 68 0

Öffnungszeiten und Erreichbarkeit:
www.itc.rwth-aachen.de/sdkontakt


-- ENGLISH ---------------------------------------------------


Dear Sir or Madam,

You have been invited to a course room on RWTH Aachen University\'s learning and teaching platform {$a->sitename}.

Course: {$a->coursename_de}
Course ID: {$a->courseidnumber}
Your role: {$a->rolename_de}
Invited by: {$a->enrolling_person_fullname} (<a href="mailto:{$a->enrolling_person_email}">{$a->enrolling_person_email}</a>)

Please click on the following link to accept the invitation. Please note: It can take up to 30 minutes until the invitation can be accepted or the coupon can be redeemed.

<a href="https://idm.rwth-aachen.de/connectMe/redeemcoupon?3&couponCode={$a->couponcode}">https://idm.rwth-aachen.de/connectMe/redeemcoupon?3&couponCode={$a->couponcode}</a>

If the entry field on target page "Redeem Coupon" is emtpy, please enter the following coupon:

{$a->couponcode}

After entering the coupon, you will be guided through the procedure step by step. The invitation is valid up to and including {$a->expirationdate_en} or until it has been accepted successfully.

If you already have an account for the RWTH Single Sign-On (sample: ab123456), please accept the invitation with this account.

If you do not yet have such an account, you can use the link to create a new account in the Identity Management of RWTH Aachen University.

After you have accepted the invitation and, if necessary, created a new account, you will automatically be added to the above course room within the next 60 minutes and will have access to all the materials and activities therein. You will receive an email notification as soon as you are granted access.


You can access the course room either via the {$a->sitename} start page <a href="{$a->siteurl}">{$a->siteurl}</a> or directly via the following link <a href="{$a->courseurl}">{$a->courseurl}</a>.
This email was generated automatically by {$a->sitename}. In case you have questions please write to <a href="mailto:{$a->support}">{$a->support}</a>.


Kind regards
Your IT Center, RWTH Aachen University
--
IT Center
RWTH Aachen University
Seffenter Weg 23
52074 Aachen
Phone: + 49 241 80 24 68 0

Opening hours:
www.itc.rwth-aachen.de/sdkontakt
';
$string['enrolusers'] = 'Add participants';
$string['enrolusers:usersearch'] = 'Search participants';
$string['enrolusers:usersearch_help'] = 'The content of the search field is divided into individual search expressions using line breaks and semicolons. These can consist of several words. For each search expression, all users are found for whom each of the search words appears in at least one of their profile fields. Searches are performed in the fields first name, last name, e-mail address and registration number. For e-mail addresses and registration numbers, however, the complete address or sequence of digits must be entered in the search. Partial matches are not supported for these fields.<br /><br />For example, the search term "Max Mustermann" will also find a user with first name "Max Philipp" and last name "Mustermann", because the search word "Max" is contained in the first name "Max Philipp". The search input "max.mustermann@rwth" does not lead to a hit, because the e-mail address is incomplete.';
$string['enrolusers:usersearchhint'] = 'External persons can be added only by their e-mail address.';
$string['enrolusers:usersearchplaceholder'] = 'Search by name, email address or registration number.
One search term per line or separated by semicolon.
Maximum of {$a} search terms.';
$string['enrolusers:confirmaddable'] = 'The following <strong>{$a->num}</strong> people can be added to the course room as "{$a->role}".';
$string['enrolusers:confirmaddablesingular'] = 'The following person can be added to the course room as "{$a}".';
$string['enrolusers:confirmaddableorinvitable'] = 'The following <strong>{$a->num}</strong> people can be added or invited to the course room as "{$a->role}".<br/>With the coupon from the invitation e-mail, an account for RWTHmoodle can be created.';
$string['enrolusers:confirmaddableorinvitablesingular'] = 'The following person can be added or invited to the course room as "{$a}".<br/>With the coupon from the invitation e-mail, an account for RWTHmoodle can be created.';
$string['enrolusers:heading_name'] = 'Participant';
$string['enrolusers:heading_action'] = 'Action';
$string['enrolusers:action_add'] = 'add';
$string['enrolusers:action_invite'] = 'invite';
$string['enrolusers:alreadyadded'] = 'already added';
$string['enrolusers:alreadyinvited'] = 'already invited';
$string['enrolusers:duplicateaccount'] = 'duplicate account';
$string['enrolusers:confirmtoomanyhits'] = 'The following <strong>{$a}</strong> search terms matched too many people each.';
$string['enrolusers:confirmtoomanyhitssingular'] = 'The following search term matched too many people.';
$string['enrolusers:confirmtoomanysearches'] = '{$a->numsearches} search terms were entered, but only {$a->maxnumsearches} are allowed per search.';
$string['enrolusers:confirmnotfound'] = 'The following <strong>{$a}</strong> people could not be found.';
$string['enrolusers:confirmnotfoundsingular'] = 'The following person could not be found.';
$string['enrolusers:confirmnotfoundhint'] = 'External people can only be invited to the course room by e-mail address.';
$string['enrolusers:confirmaddselected'] = 'Add selected';
$string['enrolusers:useraddedorinvited'] = '<strong>1 participant</strong> was added or invited successfully.';
$string['enrolusers:usersaddedorinvited'] = '<strong>{$a} participants</strong> were added or invited successfully.';
$string['enrolusers:bookingconfirmationsent'] = 'A booking confirmation was sent.';
$string['enrolusers:bookingconfirmationssent'] = 'Booking confirmations were sent.';

$string['all'] = 'all';
$string['exportstudents'] = 'Export students';
$string['participantsperpage'] = 'show {$a} participants per page';
$string['cachedef_course_participants'] = 'Cached course participants';
$string['cachedef_course_participants'] = 'Cached course invitationss';
$string['hiddenusername'] = '&lt;Name hidden&gt;';
$string['hiddenemailaddress'] = '&lt;Email address hidden&gt;';
$string['dbenrolment'] = 'role assignment';
$string['dbenrolment_help'] = 'This role assignment is synchronized from RWTHonline. If you want to remove it, you need to do that in RWTHonline and wait for that change to propagate (can take up to two hours).';
$string['otherroleassignment'] = 'role assignment';
$string['otherroleassignment_help'] = 'This role was assigned through the "{$a}" plugin. It has to be unassigned through it, too.';
$string['otherenrolment'] = 'enrolment';
$string['otherenrolment_help'] = 'This person was added to the course through the "{$a}" plugin. The person has to be removed through it, too.';
$string['cancelsearch'] = 'Clear search';

$string['privacy:metadata'] = 'This plugin only changes how the participants list looks.';

// Tasks
$string['update_database_description'] = 'Delete obsolete entries from the group_import table.';

$string['sent_invitations'] = 'Sent invitations';
$string['participants_gendered'] = 'Participants';

$string['resettable'] = 'reset preferences';
$string['enroltime'] = 'Enrolled on';
$string['notingroup'] = 'Due to course settings not all participants are visible.';
$string['invited_by'] = 'Invited by';
$string['unknown'] = 'Unknown';

$string['invitestatusopen'] = 'Expires {$a}';
$string['invitestatusrevoked'] = 'Revoked';
$string['invitestatusexpired'] = 'Expired';
$string['invitestatuscompleted'] = 'Completed';

$string['sdfail_banner'] = 'The list of participants could not be synchronized with RWTHonline.<br>You cannot review sent invitations or invite users.';
$string['sdfail_inv_title'] = '<i>Invitations unavailable</i>';
