<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once(__DIR__ . '/../../config.php');
use local_rwth_participants_view\global_navigation_exposed;
use local_rwth_participants_view\data\course_participants_view_factory;
use local_rwth_participants_view\data\group_filter;
use local_rwth_participants_view\enrol_confirm_form;
use local_rwth_participants_view\invitations_table;
use local_rwth_participants_view\participants_table;
use local_rwth_participants_view\role_helper;
use local_rwth_participants_view\rwth_enrolment_manager;

define('LOCAL_RWTH_PARTICIPANTS_VIEW_API_HIDDEN_INT', -1);
define('LOCAL_RWTH_PARTICIPANTS_VIEW_API_HIDDEN_STRING', 'hidden');

/**
 * Changes the default 'participants' page link to point to this plugin's 'participants' page.
 *
 * @param navigation_node $parent     the parent navigation/settings node
 * @param mixed           $courseid
 */
function local_rwth_participants_view_replace_participants_link(navigation_node $parent, $courseid) {
    global $PAGE;
    if (!is_numeric($courseid)) {
        return;
    }

    // Remove old entry.
    $children = [];
    $participants = $parent->find('participants', global_navigation::TYPE_CONTAINER);
    if ($participants) {
        $children = $participants->children;
        $participants->remove();
    }

    if (has_capability('local/rwth_participants_view:view', context_course::instance($courseid))) {

        // Create new entry.
        $url = new moodle_url('/local/rwth_participants_view/participants.php', array('id' => $courseid));
        $participants = navigation_node::create(
                get_string('participants_gendered', 'local_rwth_participants_view'),
                $url,
                navigation_node::TYPE_CONTAINER,
                get_string('participants_gendered', 'local_rwth_participants_view'),
                'participants',
                new pix_icon('i/users', ''));
        $participants->check_if_active(URL_MATCH_PARAMS);
        foreach ($children as $child) {
            $participants->add_node($child);
        }

        // Add entry to the parent as the first child.
        $prevkey = null;
        if ($childkeys = $parent->get_children_key_list()) {
            $prevkey = $childkeys[0];
        }
        $parent->add_node($participants, $prevkey);
    }
}

/**
 * Change the default target of the 'participants' link to this plugin's 'participants' page.
 *
 * @param global_navigation $nav The global navigation instance
 * @return void
 */
function local_rwth_participants_view_extend_navigation(global_navigation $nav) {
    global $CFG;

    if (empty($CFG->navshowmycoursecategories)) {
        $mycourses = $nav->find('mycourses', global_navigation::TYPE_ROOTNODE);
        if ($mycourses) {
            foreach ($mycourses->children as $course) {
                if ($course->key == 'courseindexpage') {
                    continue;
                }
                local_rwth_participants_view_replace_participants_link($course, $course->key);
            }
        }
    } else {
        $mycategories = $nav->find('mycourses', global_navigation::TYPE_ROOTNODE);
        if ($mycategories) {
            foreach ($mycategories->children as $category) {
                if ($category->key == 'courseindexpage') {
                    continue;
                }
                foreach ($category->children as $course) {
                    local_rwth_participants_view_replace_participants_link($course, $course->key);
                }
            }
        }
    }

    $addedcourses = global_navigation_exposed::get_property($nav, 'addedcourses');
    foreach ($addedcourses as $course) {
        local_rwth_participants_view_replace_participants_link($course, $course->key);
    }
}

/**
 * Change the default target of the 'participants' link to this plugin's 'participants' page.
 *
 * @param navigation_node $navigationnode The settings navigation instance
 * @param context $context The context the settings have been loaded for (settings is context specific)
 * @return void
 */

function local_rwth_participants_view_extend_settings_navigation(settings_navigation $settingsnav, context $context) {
    global $PAGE;

    $course = $PAGE->course;

    $participantsadministration = $settingsnav->find('users', navigation_node::TYPE_CONTAINER);
    if (!$participantsadministration) {
        return;
    }

    $isparticipantspage = $PAGE->url->compare(new moodle_url('/local/rwth_participants_view/participants.php'), URL_MATCH_BASE);

    if ($PAGE->theme->name == 'boost_campus_rwth' && $isparticipantspage) {
        $deletekeys = $participantsadministration->get_children_key_list();
        // Remove the other navigationnodes because we dont need them in our theme
        foreach ($deletekeys as $key) {
            $deletenode = $participantsadministration->find($key, navigation_node::TYPE_SETTING);
            $deletenode->remove();
        }
    }

    if (has_capability('local/rwth_participants_view:exportstudents', $context)) {
        $groupid = groups_get_course_group($course, true);
        $search     = optional_param('search', '', PARAM_RAW);
        $url = new moodle_url('/local/rwth_participants_view/students.php', ['id' => $course->id, 'groupid' => $groupid, 'search' => $search]);

        $node = navigation_node::create(get_string('rwth_participants_view:exportstudents', 'local_rwth_participants_view'), $url, navigation_node::TYPE_SETTING, null, 'exportstudents');
        $participantsadministration->add_node($node);
    }
    if (has_capability('local/course_role_customising:changeteacher', $context)) {
        $url = new moodle_url('/local/course_role_customising/view.php', ['contextid' => $context->id, 'roleset' => 'tutor']);
        $node = navigation_node::create(get_string('navigation_link_text', 'local_course_role_customising'), $url, navigation_node::TYPE_SETTING, null, 'course_role_customising');
        $node->showinflatnavigation = true;
        $participantsadministration->add_node($node);
    } else if (has_capability('local/course_role_customising:changestudent', $context)) {
        $url = new moodle_url('/local/course_role_customising/view.php', ['contextid' => $context->id, 'roleset' => 'student']);
        $node = navigation_node::create(get_string('navigation_link_text', 'local_course_role_customising'), $url, navigation_node::TYPE_SETTING, null, 'course_role_customising');
        $node->showinflatnavigation = true;
        $participantsadministration->add_node($node);
    }
    if (has_capability('moodle/course:managegroups', $context)) {
        $url = new moodle_url('/group/index.php', ['id' => $course->id]);
        $node = navigation_node::create(get_string('rwth_participants_view:managegroups', 'local_rwth_participants_view'), $url, navigation_node::TYPE_SETTING, null, 'managegroups', new pix_icon('i/groups', ''));
        $participantsadministration->add_node($node);
    }
    if (has_capability('local/rwth_participants_view:groupimport', $context)) {
        $url = new moodle_url('/local/rwth_participants_view/groupimport.php', ['id' => $course->id]);
        $node = navigation_node::create(get_string('rwth_participants_view:groupimport', 'local_rwth_participants_view'), $url, navigation_node::TYPE_SETTING, null, 'groupimport');
        $participantsadministration->add_node($node);
    }
}

/**
 * Adds some custom HTML header code.
 *
 * @return string The HTML code to include in the header
 */
function local_rwth_participants_view_before_standard_html_head(): string {

    if ($GLOBALS['SCRIPT'] === '/local/rwth_participants_view/participants.php') {
        // This CSS code will only be applied if JavaScript is not available. Thus
        // any HTML element with the class "jsonly" will not even be displayed if
        // JavaScript is not available.
        return "<noscript><style> .jsonly { display: none } </style></noscript>\n";
    } else {
        return '';
    }
}

/**
 * Override default get participants webservice function.
 */
function local_rwth_participants_view_override_webservice_execution($function, $params) {
    global $ACCESSLIB_PRIVATE, $CFG, $DB, $USER;
    if ($function->name == 'core_enrol_get_enrolled_users') {

        $courseid = $params[0];
        $options = $params[1];

        $course = $DB->get_record('course', ['id' => $courseid], '*', MUST_EXIST);
        $context = context_course::instance($course->id, MUST_EXIST);

        require_course_login($course, false);
        require_capability('local/rwth_participants_view:view', $context);

        $groupmode = groups_get_course_groupmode($course);
        $groupid = groups_get_course_group($course, true);

        /*
         * TODO implement $options?
         *
         * withcapability (string) return only users with this capability. This option requires 'moodle/role:review' on the course context.
         * groupid (integer) return only users in this group id. If the course has groups enabled and this param
                             isn't defined, returns all the viewable users.
                             This option requires 'moodle/site:accessallgroups' on the course context if the
                             user doesn't belong to the group.
         * onlyactive (integer) return only users with active enrolments and matching time restrictions. This option requires 'moodle/course:enrolreview' on the course context.
         * userfields ('string, string, ...') return only the values of these user fields.
         * limitfrom (integer) sql limit from.
         * limitnumber (integer) maximum number of returned users.
         * sortby (string) sort by id, firstname or lastname. For ordering like the site does, use siteorder.
         * sortdirection (string) ASC or DESC
         */
        if (!empty($options)) {
            throw new invalid_parameter_exception('options parameter currently not implemented');
        }

        // These are the only roles we want to show.
        $visibleroles = role_helper::get_visible_roles($courseid);

        $factory = new course_participants_view_factory($courseid);
        if ($groupmode == SEPARATEGROUPS && !has_capability('moodle/site:accessallgroups', $context)) {
            if ($groupid == 0) {
                // User is not in a group.
                throw new moodle_exception('notingroup');
            }
            $factory->add_filter(new group_filter($groupid));
        }
        $rawusers = $factory->create();

        $users = [];
        foreach ($rawusers as $rawuser) {
            $user = (object) [
                'id' => $rawuser->id,
                'firstname' => $rawuser->firstname,
                'lastname' => $rawuser->lastname,
                'fullname' => \local_rwth_participants_view\user_helper::get_fullname($rawuser),
                'email' => $rawuser->email,
                // 'idnumber' => $rawuser->idnumber,
                'roles' => [],
                'groups' => [],
            ];

            if (!has_capability('local/rwth_participants_view:api_canseeuserids', $context)) {
                $user->id = LOCAL_RWTH_PARTICIPANTS_VIEW_API_HIDDEN_INT;
            }

            if ($rawuser->mailhidden) {
                $user->email = LOCAL_RWTH_PARTICIPANTS_VIEW_API_HIDDEN_STRING;

                if (get_config('rwth_participants_view', 'emaildisplaycontrolsnamedisplay')) {
                    // We (ab)use the maildisplay setting to control display of names aswell.
                    $user->firstname = LOCAL_RWTH_PARTICIPANTS_VIEW_API_HIDDEN_STRING;
                    $user->lastname = LOCAL_RWTH_PARTICIPANTS_VIEW_API_HIDDEN_STRING;
                    $user->fullname = LOCAL_RWTH_PARTICIPANTS_VIEW_API_HIDDEN_STRING;
                }
            }

            if ($rawuser->tutorhidden) {
                $user->email = LOCAL_RWTH_PARTICIPANTS_VIEW_API_HIDDEN_STRING;
                $user->firstname = LOCAL_RWTH_PARTICIPANTS_VIEW_API_HIDDEN_STRING;
                $user->lastname = LOCAL_RWTH_PARTICIPANTS_VIEW_API_HIDDEN_STRING;
                $user->fullname = LOCAL_RWTH_PARTICIPANTS_VIEW_API_HIDDEN_STRING;
            }

            foreach ($rawuser->roles as $role) {
                if (array_key_exists($role->id, $visibleroles)) {
                    $visiblerole = $visibleroles[$role->id];
                    $user->roles[] = (object) [
                        'roleid' => $visiblerole->id,
                        'name' => $visiblerole->guiname,
                        'shortname' => $visiblerole->shortname,
                        'sortorder' => $visiblerole->sortorder,
                    ];
                }
            }
            if (empty($user->roles)) {
                // Don't show users without roles.
                continue;
            }

            foreach ($rawuser->groups as $group) {
                $user->groups[] = (object) [
                    'id' => $group->id,
                    'name' => format_string($group->name),
                    'description' => $group->description,
                    'descriptionformat' => $group->descriptionformat,
                ];
            }

            $users[] = $user;
        }
        return $users;
    } else if ($function->name == 'core_user_get_users_by_field') {
        // This override allows managers to get students' lmsids. It does so by temporarily
        // (just for this function call) giving them the capability 'moodle/user:viewdetails'.
        // Originally developed for ticket 20201029-1667.

        $cap = 'local/rwth_participants_view:api_canqueryuserprofilefields';
        $courses = get_user_capability_course($cap);
        foreach ($courses as $course) {
            $coursecontext = context_course::instance($course->id);
            $roleids = get_roles_with_caps_in_context($coursecontext, [$cap]);
            // Function calls above ensure $ACCESSLIB_PRIVATE->cacheroledefs is filled.
            foreach ($roleids as $roleid) {
                if (!array_key_exists($roleid, $ACCESSLIB_PRIVATE->cacheroledefs)) {
                    // Caller doesn't have this role.
                    continue;
                }
                if (!array_key_exists($coursecontext->path, $ACCESSLIB_PRIVATE->cacheroledefs[$roleid])) {
                    $ACCESSLIB_PRIVATE->cacheroledefs[$roleid][$coursecontext->path] = [];
                }
                $ACCESSLIB_PRIVATE->cacheroledefs[$roleid][$coursecontext->path]['moodle/user:viewdetails'] = CAP_ALLOW;
            }
        }

        return core_user_external::get_users_by_field($params[0], $params[1]);
    }

    return false;
}

function local_rwth_participants_view_output_fragment_enrol_overview($params) {
    global $DB, $PAGE;
    $courseid = $params['courseid'];
    $roleid = $params['roleid'];
    $search = $params['search'];
    $context = $params['context'];

    require_capability('local/rwth_participants_view:enrol', $context);

    $course = $DB->get_record('course', ['id' => $courseid]);
    $manager = new rwth_enrolment_manager($PAGE, $course);
    $results = $manager->search_users_rwth($search, $roleid);

    ob_start();
    $mform = new enrol_confirm_form(null, [
        'id' => $courseid,
        'roleid' => $roleid,
        'enrollable' => $results->enrollable,
        'invitable' => $results->invitable,
        'toomanyhits' => $results->toomanyhits,
        'notfound' => $results->notfound,
    ], 'post', '', ['id' => 'enrol-confirm-form']);
    $mform->display();
    $html = ob_get_contents();
    ob_end_clean();

    return $html;
}

function local_rwth_participants_view_output_fragment_participants_table($params) {
    global $DB, $PAGE;
    $courseid = $params['courseid'];
    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
    $context = context_course::instance($course->id, MUST_EXIST);

    require_capability('local/rwth_participants_view:view', $context);

    // Trigger events.
    user_list_view($course, $context);

    $baseurl = new moodle_url('/local/rwth_participants_view/participants.php', ['id' => $courseid]);

    if ($_GET) {
        $callctx = &$_GET;
    } else {
        $callctx = &$_POST;
    }

    foreach ($params as $filter => $value) {
        switch ($filter) {
            case 'courseid':
                $callctx['id'] = $value;
                break;
            case 'roleid':
                $callctx['role'] = $value;
                break;
            case 'group':
                $callctx['group'] = $value;
                break;
            case 'nameselector':
                if ($value != "initialbarall") {
                    $callctx['ifirst'] = $value;
                } else {
                    $callctx['ifirst'] = '';
                }
                break;
            case 'surnameselector':
                if ($value != "initialbarall") {
                    $callctx['ilast'] = $value;
                } else {
                    $callctx['ilast'] = '';
                }
                break;
            case 'reset':
                $callctx['reset'] = $value;
                break;
            case 'perpage':
                $callctx['perpage'] = $value;
                break;
            case 'page':
                $callctx['page'] = $value;
                break;
        }
    }

    $optionalcolumns = explode(',', get_config('rwth_participants_view', 'showcolumns'));

    // Render table.
    $table = new participants_table($context, $baseurl, $course);
    $table->set_show_fullname(true);
    foreach ($optionalcolumns as $colnr) {
        $table->set_show_optional_column($colnr);
    }
    $table->setup();
    $tablehtml = $table->render();

    // Provide title in hidden div.
    $totalcount = $table->get_total_count();
    $matchcount = $table->get_match_count();
    if ($totalcount === $matchcount) {
        $count = $totalcount;
    } else {
        $count = $matchcount.'/'.$totalcount;
    }

    $title = get_string('participants_gendered', 'local_rwth_participants_view') . " ($count)";
    $titlehtml = '<div class="new-title" style="display: none;">'.$title.'</div>';

    $js = <<<EOS
    let participantstab = document.getElementById('participants');
    let newtitle = participantstab.querySelector('.new-title');
    let title = document.querySelector('[href="#participants"');
    title.innerText = newtitle.innerText;
    newtitle.remove();
    EOS;
    $PAGE->requires->js_amd_inline($js);
    return $titlehtml.$tablehtml;
}

function local_rwth_participants_view_output_fragment_invitations_table($params) {
    global $DB, $PAGE;
    $courseid = $params['courseid'];
    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
    $context = context_course::instance($course->id, MUST_EXIST);

    require_capability('local/rwth_participants_view:manageinvitations', $context);

    $baseurl = new moodle_url('/local/rwth_participants_view/participants.php', ['id' => $courseid]);
    $title = get_string('sent_invitations', 'local_rwth_participants_view');

    // Render table.
    $table = new invitations_table($context, $baseurl, $course);
    $table->setup();
    $tablehtml = $table->render();

    // Provide title in hidden div.
    $count = $table->get_total_count();
    $title .= " ($count)";
    $titlehtml = '<div class="new-title" style="display: none;">'.$title.'</div>';

    $js = <<<EOS
    let invitationstab = document.getElementById('invitations');
    let newtitle = invitationstab.querySelector('.new-title');
    if (newtitle == null) {
        newtitle = document.querySelector('[href="#invitations"').innerHTML.replace('/\(\d+\)/', '(0)');
    }

    let title = document.querySelector('[href="#invitations"');
    title.innerText = newtitle.innerText;
    newtitle.remove();
    EOS;
    $PAGE->requires->js_amd_inline($js);

    return $titlehtml.$tablehtml;
}
