<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2022 RWTH Aachen University
 * @author    Donath@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

use local_rwth_participants_view\data;
use local_rwth_participants_view\cache;
use tool_sdapi\sitedirectory_api;

/** returns all invitations that match the ids in $invitationids */
function get_invitations_with_ids(int $courseid, array $invitationids): array {
    $factory = new data\course_invitations_view_factory($courseid);
    return $factory->add_filter(new data\invitationids_filter($invitationids))->create();
}

/** returns the invitation matching the $invitationid as an object */
function get_invitation_with_id(int $courseid, $invitationid) {
    $factory = new data\course_invitations_view_factory($courseid);
    $invitation = $factory->add_filter(new data\invitationids_filter([$invitationid]))->create();
    return reset($invitation);
}

/** updates the status of the given invitation in the cache */
function update_cache_invitation_status(int $courseid, $invitationid, $status) {
    $cache = new cache\manager(cache\manager::AREA_COURSE_INVITATIONS);
    $cv = $cache->get($courseid.'_invitations');
    $cv->invitations[$invitationid]->status = $status;
    $cv->invitations[$invitationid]->modified = time();
    if ($status == sitedirectory_api::INVITATION_STATUS_OPEN) {
        $openduration = get_config('tool_sdapi', 'invitation_duration');
        $cv->invitations[$invitationid]->expires = $cv->invitations[$invitationid]->modified + $openduration;
    }
    $cache->set($courseid.'_invitations', $cv);
}
