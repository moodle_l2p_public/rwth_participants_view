<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/locallib.php');


require_once($CFG->dirroot.'/enrol/locallib.php');
require_once($CFG->dirroot.'/enrol/renderer.php');
require_once($CFG->dirroot.'/user/lib.php');
require_once($CFG->libdir.'/outputcomponents.php');

use local_rwth_participants_view\participants_table;
use local_rwth_participants_view\invitations_table;
use local_rwth_participants_view\output\page;
use local_rwth_participants_view\role_helper;
use local_rwth_participants_view\enrol_search_form;
use local_rwth_participants_view\user_helper;
use \tool_sdapi\sitedirectory_api;


$courseid = required_param('id', PARAM_INT); // Course Module ID.
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

$PAGE->set_url(new moodle_url('/local/rwth_participants_view/participants.php', ['id' => $courseid]));
$PAGE->set_context($context);

require_course_login($course, false);
require_capability('local/rwth_participants_view:view', $context);

$action = optional_param('action', '', PARAM_ALPHANUMEXT);

$baseurl = new moodle_url('/local/rwth_participants_view/participants.php', ['id' => $courseid]);

$PAGE->set_title("$course->shortname: ".get_string('participants_gendered', 'local_rwth_participants_view'));
$PAGE->set_heading($course->fullname);

// Trigger events.
user_list_view($course, $context);

$manager = new course_enrolment_manager($PAGE, $course, 0, 0, '', 0, -1);
if ($action) {

    // Check if the page is confirmed (and sesskey is correct).
    $confirm = optional_param('confirm', false, PARAM_BOOL) && confirm_sesskey();

    $actiontaken = false;
    $pagetitle = '';
    $pageheading = '';
    $pagecontent = null;

    switch ($action) {
        /*
         * Removes a role from the user in this course. Also unenrols the user if
         * that's his last role in the course.
         */
        case 'unassign':
            if (!has_capability('moodle/role:assign', $context)) {
                break;
            }
            $rid = required_param('roleid', PARAM_INT);
            $uid = required_param('user', PARAM_INT);

            // Check if we're removing the user's last role assigned by the respective enrolment plugin
            // in this course. If so, we must also unenrol him through that enrolment plugin.
            $user = user_helper::get_user_with_id($course->id, $uid);
            $component = 'unknown';
            foreach ($user->roles as $role) {
                if ($role->id == $rid) {
                    $component = $role->assigning_component;
                    break;
                }
            }
            $countroles = 0;
            foreach ($user->roles as $role) {
                if ($role->assigning_component == $component) {
                    $countroles++;
                }
            }
            if (count($user->enrolments) == 1) {
                $enrol = reset($user->enrolments)->enrol;
            } else {
                $enrol = empty($component) ? 'manual' : preg_replace('/^enrol_/', '', $component);
            }
            $unenrol = $countroles <= 1;

            $unenrolinstances = [];
            foreach ($user->enrolments as $enrolinstance) {
                if ($enrolinstance->enrol != $enrol) {
                    // Not the enrol instance for this role assignment.
                    continue;
                }
                $eid = $enrolinstance->id;
                $ue = $user->userenrolments[$eid];
                $plugin = enrol_get_plugin($enrol);
                if ($unenrol && $plugin->allow_unenrol_user($enrolinstance, $ue)
                        && has_capability("enrol/$enrol:unenrol", $context)) {
                    $unenrolinstances[] = $enrolinstance;
                }
            }

            if ($unenrol && empty($unenrolinstances)) {
                echo $OUTPUT->header();
                throw new moodle_exception('erroreditenrolment', 'enrol');
            }

            if ($confirm && $manager->unassign_role_from_user($uid, $rid)) {
                // Role has been removed from user.
                if ($unenrol) {
                    // Unenrol user from course as he has no roles left.
                    foreach ($unenrolinstances as $enrolinstance) {
                        $plugin = enrol_get_plugin($enrol);
                        $plugin->unenrol_user($enrolinstance, $uid);
                    }
                }
                redirect($PAGE->url);
            } else {
                $roles = role_helper::get_visible_roles($courseid);
                $yesurl = new moodle_url($PAGE->url, array('action' => 'unassign', 'roleid' => $rid, 'user' => $user->id, 'confirm' => 1, 'sesskey' => sesskey()));
                $message = get_string('unassignconfirm', 'role', array('user' => user_helper::get_fullname($user), 'role' => $roles[$rid]->guiname));
                $pagetitle = get_string('unassignarole', 'role', $roles[$rid]->guiname);
                $pagecontent = $OUTPUT->confirm($message, $yesurl, $PAGE->url);
            }
            $actiontaken = true;
            break;

        case 'unenrol':
            if (!has_capability('local/rwth_participants_view:enrol', $context)) {
                break;
            }
            $uid = required_param('user', PARAM_INT);
            $eid = required_param('enrol', PARAM_INT);
            $user = user_helper::get_user_with_id($course->id, $uid);
            if (!array_key_exists($eid, $user->enrolments)) {
                break;
            }
            $enrolinstance = $user->enrolments[$eid];
            $enrol = $enrolinstance->enrol;
            if ($enrol != 'manual') {
                throw new moodle_exception('otherenrolment_help', 'local_rwth_participants_view', '', $enrol);
            }
            $plugin = enrol_get_plugin($enrol);
            foreach ($user->userenrolments as $ue) {
                if ($ue->enrolid == $enrolinstance->id) {
                    if ($plugin->allow_unenrol_user($enrolinstance, $ue)
                            && has_capability("enrol/$enrol:unenrol", $context)) {
                        $unenrolinstances[] = $enrolinstance;
                    }
                }
            }
            if ($confirm) {
                foreach ($unenrolinstances as $enrolinstance) {
                    $plugin->unenrol_user($enrolinstance, $uid);
                }
                redirect($PAGE->url);
            } else {
                $yesurl = new moodle_url($PAGE->url, ['action' => 'unenrol', 'user' => $user->id, 'enrol' => $eid, 'confirm' => 1, 'sesskey' => sesskey()]);
                $message = get_string('unenrolconfirm', 'enrol', [
                    'user' => user_helper::get_fullname($user),
                    'course' => format_string($course->fullname),
                    'enrolinstancename' => $plugin->get_instance_name($enrolinstance),
                ]);
                $pagetitle = get_string('unenrol', 'enrol_manual');
                $pagecontent = $OUTPUT->confirm($message, $yesurl, $PAGE->url);
            }
            $actiontaken = true;
            break;

        case 'revokeinvitation':
            global $USER;
            if (!has_capability('moodle/role:assign', $context)) {
                break;
            }
            $invitationid = required_param('invitation', PARAM_TEXT);
            if ($confirm) {
                $sdapi = new sitedirectory_api();
                $sdapi->update_invitation_status($invitationid, sitedirectory_api::INVITATION_STATUS_REVOKED);
                update_cache_invitation_status($courseid, $invitationid, sitedirectory_api::INVITATION_STATUS_REVOKED);
                redirect($PAGE->url."#invitations");
            } else {
                $invitation = get_invitation_with_id($courseid, $invitationid);

                $yesurl = new moodle_url($PAGE->url, array('action' => 'revokeinvitation', 'invitation' => $invitationid, 'confirm' => 1, 'sesskey' => sesskey()));
                $message = get_string('bulkrevokeinvitationconfirm', 'local_rwth_participants_view');
                $message .= $OUTPUT->heading($invitation->rolename, 5);
                $message .= html_writer::start_tag('p');
                $message .= " ($invitation->email) <br />";
                $message .= html_writer::end_tag('p');

                $pagetitle = get_string('invitestatuschange', 'local_rwth_participants_view');
                $pagecontent = $OUTPUT->confirm($message, $yesurl, $PAGE->url."#invitations");
            }
            $actiontaken = true;
            break;
    }

    // If we took an action display we need to display something special.
    if ($actiontaken) {
        if (empty($pageheading)) {
            $pageheading = $pagetitle;
        }
        $PAGE->set_title($pagetitle);
        $PAGE->set_heading($pageheading);
        echo $OUTPUT->header();
        echo $pagecontent;
        echo $OUTPUT->footer();
        exit;
    }
}
echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('participants_gendered', 'local_rwth_participants_view'));

if (!role_helper::can_see_other_students($courseid)) {
    $output = $PAGE->get_renderer('local_rwth_participants_view');
    echo $output->notification_without_closebutton(get_string('notingroup', 'local_rwth_participants_view'), 'warning');
}

echo '<div class="userlist">';

$optionalcolumns = explode(',', get_config('rwth_participants_view', 'showcolumns'));

$viewparticipants = true;
$participantshtml = false;
try {
    // Participants tab.
    $table = new participants_table($context, $baseurl, $course);
    $table->set_show_fullname(true);
    foreach ($optionalcolumns as $colnr) {
        $table->set_show_optional_column($colnr);
    }
    $table->setup();
    $participantshtml = $table->render();
    $totalcount = $table->get_total_count();
    $matchcount = $table->get_match_count();
    if ($totalcount === $matchcount) {
        $participantscount = $totalcount;
    } else {
        $participantscount = $matchcount.'/'.$totalcount;
    }
} catch (\tool_sdapi\access_sitedirectory_exception $sdex) {
    // Defaults - 0 hits, blank tab filled by async load.
    $participantscount = 0;
    \core\notification::warning(get_string('sdfail_banner', 'local_rwth_participants_view'));

}
$participantstitle = get_string('participants_gendered', 'local_rwth_participants_view') . " ($participantscount)";

$viewinvitations = false;
$invitationshtml = false;
$invitationstitle = null;
if (has_capability('local/rwth_participants_view:manageinvitations', $context)) {
    // Sent invitations tab.
    $viewinvitations = true;
    $invitationstitle = get_string('sent_invitations', 'local_rwth_participants_view');
}

// Add participants tab.
$addparticipantshtml = false;
if (has_capability('local/rwth_participants_view:enrol', $context)) {
    ob_start();
    $mform = new enrol_search_form(null, ['context' => $context], 'post', '', ['id' => 'enrol-search-form']);
    $mform->display();
    $addparticipantshtml = ob_get_contents();
    ob_end_clean();
    $PAGE->requires->js_call_amd('local_rwth_participants_view/enrolment', 'init', [$context->id]);
}

// Render users.
$page = new page($viewparticipants, $participantstitle, $participantshtml,
                 $viewinvitations, $invitationstitle, $invitationshtml,
                 $addparticipantshtml);
echo $OUTPUT->render($page);
$PAGE->requires->js_call_amd('local_rwth_participants_view/asyncview', 'init', [$context->id, $course->id]);

echo '</div>';  // Userlist.

echo $OUTPUT->footer();
