<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../../config.php');
require_once($CFG->dirroot.'/enrol/locallib.php');

use local_rwth_participants_view\data;
use local_rwth_participants_view\user_helper;

$courseid = required_param('id', PARAM_INT);
$returnto = required_param('returnto', PARAM_LOCALURL);
$confirm = optional_param('confirm', false, PARAM_BOOL) && confirm_sesskey();

$url = new moodle_url('/local/rwth_participants_view/roleremove.php', ['id' => $courseid, 'returnto' => $returnto]);
$PAGE->set_url($url);

if (!$course = $DB->get_record('course', array('id' => $courseid))) {
    throw new moodle_exception('invalidcourseid');
}

require_login($course);

$context = context_course::instance($courseid);   // Course context.
require_capability('moodle/role:assign', $context);
$assignableroles = get_assignable_roles($context);

$userids = [];
if ($data = data_submitted()) {
    require_sesskey();
    foreach ($data as $k => $v) {
        if (preg_match('/^user(\d+)$/', $k, $m)) {
            $uid = $m[1];
            if (!in_array($uid, $userids)) {
                $userids[] = $uid;
            }
        }
    }
}

$users = get_users_with_ids($courseid, $userids);

// Create a list of all roles each user has (grouped by enrolment plugin).
$userid2enrol2roles = [];
$useridswithnoroles = [];
foreach ($userids as $uid) {
    $user = $users[$uid];

    if (empty($user->roles)) {
        $useridswithnoroles[] = $uid;
        continue;
    }

    $userid2enrol2roles[$uid] = [];
    foreach ($user->roles as $role) {
        $component = $role->assigning_component;
        $enrol = empty($component) ? 'manual' : preg_replace('/^enrol_/', '', $component);
        if (!isset($userid2enrol2roles[$uid][$enrol])) {
            $userid2enrol2roles[$uid][$enrol] = [];
        }
        $userid2enrol2roles[$uid][$enrol][] = $role;
    }
}

$userid2removableroles = [];
$roleid2removableuserids = [];
foreach ($userid2enrol2roles as $uid => $enrol2roles) {
    foreach ($enrol2roles as $enrol => $roles) {
        if ($enrol == 'manual' || $enrol == 'self') {
            foreach ($roles as $role) {
                if (!array_key_exists($uid,  $userid2removableroles)) {
                    $userid2removableroles[$uid] = [];
                }
                if (!array_key_exists($role->id,  $roleid2removableuserids)) {
                    $roleid2removableuserids[$role->id] = [];
                }
                $roleid2removableuserids[$role->id][] = $uid;
            }
        }
    }
}

$strtitle = get_string('courseremoverole', 'local_rwth_participants_view');
$PAGE->set_pagelayout('incourse');

$link = null;
if (has_capability('moodle/course:viewparticipants', $context)) {
    $link = new moodle_url("/local/rwth_participants_view/participants.php", ['id' => $course->id]);
}
$PAGE->navbar->add(get_string('participants_gendered', 'local_rwth_participants_view'), $link);
$PAGE->navbar->add($strtitle);
$PAGE->set_title($strtitle);
$PAGE->set_heading($strtitle);

$manager = new course_enrolment_manager($PAGE, $course, 0, 0, '', 0, -1);
$rolenames = role_fix_names(get_all_roles($context), $context, ROLENAME_ALIAS, true);

if ($confirm) {
    foreach ($users as $user) {
        $uid = $user->id;
        $dounenrol = true;

        if (!in_array($uid, $useridswithnoroles)) {

            // Check whether the user has any non-manually assigned roles. If so, unenrol him.
            foreach ($userid2enrol2roles[$uid] as $enrol => $roles) {
                if ($enrol != 'manual' && $enrol != 'self') {
                    $dounenrol = false;
                    break;
                }
            }

            // Unassign roles.
            foreach ($userid2removableroles[$uid] as $role) {
                $manager->unassign_role_from_user($uid, $role->id);
            }
        }

        // Unenrol user if necessary.
        if ($dounenrol) {
            $unenrolled = false;
            foreach ($user->enrolments as $enrolinstance) {
                if ($enrolinstance->enrol == 'manual' || $enrolinstance->enrol == 'self') {
                    $ei = $enrolinstance;
                    $ue = $user->userenrolments[$ei->id];
                    $plugin = enrol_get_plugin($ei->enrol);
                    if (!$plugin->allow_unenrol_user($ei, $ue) || !has_capability("enrol/$ei->enrol:unenrol", $context)) {
                        echo $OUTPUT->header();
                        throw new moodle_exception('erroreditenrolment', 'enrol');
                    }
                    // Unenrol user from course as he has no roles left.
                    $plugin->unenrol_user($ei, $uid);
                    $unenrolled = true;
                }
            }
            if (!$unenrolled) {
                echo $OUTPUT->header();
                throw new moodle_exception('erroreditenrolment', 'enrol');
            }
        }
    }

    redirect($returnto);
} else {
    echo $OUTPUT->header();
    $yesurl = new moodle_url($PAGE->url,
            ['id' => $courseid, $returnto => $returnto, 'confirm' => 1, 'sesskey' => sesskey()]);
    $message = get_string('bulkunassignconfirm', 'local_rwth_participants_view');
    if (!empty($useridswithnoroles)) {
        $message .= $OUTPUT->heading(get_string('noroles', 'role'), 5);
        $message .= html_writer::start_tag('p');
        foreach ($useridswithnoroles as $uid) {
            $yesurl->param('user'.$uid, 'on');
            $user = $users[$uid];
            $message .= user_helper::get_fullname($user)." ($user->email) <br />";
        }
        $message .= html_writer::end_tag('p');
    }
    foreach ($roleid2removableuserids as $rid => $uids) {
        $message .= $OUTPUT->heading($rolenames[$rid], 5);
        $message .= html_writer::start_tag('p');
        foreach ($uids as $uid) {
            $yesurl->param('user'.$uid, 'on');
            $user = $users[$uid];
            $message .= user_helper::get_fullname($user)." ($user->email) <br />";
        }
        $message .= html_writer::end_tag('p');
    }
    echo $OUTPUT->confirm($message, $yesurl, $returnto);
}

echo $OUTPUT->footer();


function get_users_with_ids(int $courseid, array $uids): array {
    $factory = new data\course_participants_view_factory($courseid);
    return $factory->add_filter(new data\userids_filter($uids))->create();
}
