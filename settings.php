<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

use local_rwth_participants_view\participants_table;
use local_rwth_participants_view\role_helper;

// Ensure the configurations for this site are set
if ($hassiteconfig) {
    $roles = role_helper::get_visible_roles();
    $rolenames = array_column($roles, 'guiname', 'id');

    $settings = new admin_settingpage( 'local_rwth_participants_view', get_string('generalsettings', 'local_rwth_participants_view'));
    $ADMIN->add('localplugins', $settings);

    $settings->add(new admin_setting_configmulticheckbox(
            'rwth_participants_view/showcolumns',
            get_string('show_columns', 'local_rwth_participants_view'),
            get_string('show_columns_help', 'local_rwth_participants_view'),
            [
                participants_table::COLUMN_PICTURE => 0
            ],
            [
                participants_table::COLUMN_PICTURE => get_string('column_picture', 'local_rwth_participants_view')
            ]));

    $settings->add(new admin_setting_configcheckbox(
            'rwth_participants_view/emaildisplaycontrolsnamedisplay',
            get_string('emaildisplay_controls_namedisplay', 'local_rwth_participants_view'),
            get_string('emaildisplay_controls_namedisplay_help', 'local_rwth_participants_view'),
            0));

    $settings->add(new admin_setting_pickroles(
            'rwth_participants_view/visible_roles',
            get_string('visible_roles', 'local_rwth_participants_view'),
            get_string('visible_roles_help', 'local_rwth_participants_view'),
            array('manager', 'teacher', 'student')));

    $settings->add(new admin_setting_pickroles(
            'rwth_participants_view/roles_with_visible_user_info',
            get_string('roles_with_visible_user_info', 'local_rwth_participants_view'),
            get_string('roles_with_visible_user_info_help', 'local_rwth_participants_view'),
            array('manager', 'teacher')));

    $settings->add(new admin_setting_pickroles(
            'rwth_participants_view/student_roles',
            get_string('student_roles', 'local_rwth_participants_view'),
            get_string('student_roles_help', 'local_rwth_participants_view'),
            array('student')));

    $defaultroleid = array_keys($roles)[0]; // By default use the first role.
    foreach ($roles as $role) {
            $settings->add(new admin_setting_configselect(
                    'rwth_participants_view/default_rolefilter_'.$role->shortname,
                    get_string('role_default_rolefilter', 'local_rwth_participants_view', $role->guiname),
                    get_string('role_default_rolefilter_help', 'local_rwth_participants_view', $role->guiname),
                    $defaultroleid,
                    $rolenames));
    }

    foreach ($roles as $role) {
        $options = array(0 => get_string('role_no_mapping', 'local_rwth_participants_view'));
        foreach ($roles as $role2) {
            if ($role != $role2) {
                        $options [$role2->id] = $role2->guiname;
            }
        }
        $settings->add(new admin_setting_configselect(
                'rwth_participants_view/rolemapping_'.$role->shortname,
                get_string('role_mapping', 'local_rwth_participants_view', $role->guiname),
                get_string('role_mapping_help', 'local_rwth_participants_view', $role->guiname),
                array_keys($options)[0],
                $options));
    }

    $settings->add(new admin_setting_configtext(
            'rwth_participants_view/groupsize_visibility',
            get_string('groupsize_visibility', 'local_rwth_participants_view'),
            get_string('groupsize_visibility_help', 'local_rwth_participants_view'),
            10,
            PARAM_INT));

    $settings->add(new admin_setting_heading('enrolheader',
            get_string('enrol_settings_header', 'local_rwth_participants_view'), ''));

    $student = get_archetype_roles('student');
    $student = reset($student);
    $settings->add(new admin_setting_configselect(
            'rwth_participants_view/enrol_default_role',
            get_string('enrol_default_role', 'local_rwth_participants_view'),
            get_string('enrol_default_role_help', 'local_rwth_participants_view'),
            $student->id, $rolenames));

    $settings->add(new admin_setting_configtext(
            'rwth_participants_view/enrol_threshold',
            get_string('enrol_threshold', 'local_rwth_participants_view'),
            get_string('enrol_threshold_help', 'local_rwth_participants_view'),
            50, PARAM_INT));

    $settings->add(new admin_setting_heading('groupimportheader',
            get_string('groupimport_settings_header', 'local_rwth_participants_view'), ''));

    $student = get_archetype_roles('student');
    $student = reset($student);
    $settings->add(new admin_setting_configselect(
            'rwth_participants_view/groupimport_role',
            get_string('groupimport_role', 'local_rwth_participants_view'),
            get_string('groupimport_role_help', 'local_rwth_participants_view'),
            $student->id, $rolenames));

    $settings->add(new admin_setting_configtext(
            'rwth_participants_view/groupimport_threshold',
            get_string('groupimport_threshold', 'local_rwth_participants_view'),
            get_string('groupimport_threshold_help', 'local_rwth_participants_view'),
            200, PARAM_INT));

    $settings->add(new admin_setting_configtext(
           'rwth_participants_view/groupimport_maximum',
            get_string('groupimport_maximum', 'local_rwth_participants_view'),
            get_string('groupimport_maximum_help', 'local_rwth_participants_view'),
            3000, PARAM_INT));

    $settings->add(new admin_setting_configtext(
            'rwth_participants_view/groupimport_timeframe',
             get_string('groupimport_timeframe', 'local_rwth_participants_view'),
             get_string('groupimport_timeframe_help', 'local_rwth_participants_view'),
             60, PARAM_INT));
}