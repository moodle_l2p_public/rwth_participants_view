<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/locallib.php');

use \tool_sdapi\sitedirectory_api;

$courseid = required_param('id', PARAM_INT);
$returnto = required_param('returnto', PARAM_LOCALURL);
$confirm = optional_param('confirm', false, PARAM_BOOL) && confirm_sesskey();

$url = new moodle_url('/local/rwth_participants_view/statuschange.php', ['id' => $courseid, 'returnto' => $returnto]);
$PAGE->set_url($url);

if (!$course = $DB->get_record('course', ['id' => $courseid])) {
    throw new moodle_exception('invalidcourseid');
}

require_login($course);

$context = context_course::instance($courseid);   // Course context.
require_capability('moodle/role:assign', $context);
$assignableroles = get_assignable_roles($context);

$invitationids = [];
if ($data = data_submitted()) {
    require_sesskey();
    foreach ($data as $key => $value) {
        if (preg_match('/^(?:invitation)([1-9][0-9]*)$/', $key, $m)) {
            $invitationids[] = $m[1];
        }
    }
}

$strtitle = get_string('invitestatuschange', 'local_rwth_participants_view');
$PAGE->set_pagelayout('incourse');

$link = null;
if (has_capability('moodle/course:viewparticipants', $context)) {
    $link = new moodle_url("/local/rwth_participants_view/participants.php", ['id' => $course->id]);
}
$PAGE->navbar->add(get_string('participants_gendered', 'local_rwth_participants_view'), $link);
$PAGE->navbar->add($strtitle);
$PAGE->set_title($strtitle);
$PAGE->set_heading($strtitle);

$rolenames = role_fix_names(get_all_roles($context), $context, ROLENAME_ALIAS, true);

if ($confirm) {
    global $USER;
    $sdapi = new sitedirectory_api();
    foreach ($invitationids as $invitationid) {
        $sdapi->update_invitation_status($invitationid, $_POST['status']);
        update_cache_invitation_status($courseid, $invitationid, $_POST['status']);
    }
    redirect($returnto);
} else {
    echo $OUTPUT->header();
    $yesurl = new moodle_url($PAGE->url, ['id' => $courseid, $returnto => $returnto,
            'confirm' => 1, 'sesskey' => sesskey()]);
    // Change message and status depending on which button was pressed.
    if ($_POST['action'] == get_string('renewinvitation', 'local_rwth_participants_view')) {
        $message = get_string('bulkrenewinvitationconfirm', 'local_rwth_participants_view');
        $status = sitedirectory_api::INVITATION_STATUS_OPEN;
    } else {
        $message = get_string('bulkrevokeinvitationconfirm', 'local_rwth_participants_view');
        $status = sitedirectory_api::INVITATION_STATUS_REVOKED;
    }
    $invitations = get_invitations_with_ids($courseid, $invitationids);
    $rolename2invitations = [];
    foreach ($invitations as $invitation) {
        $rolename = $invitation->rolename;
        if (!array_key_exists($rolename, $rolename2invitations)) {
            $rolename2invitations[$rolename] = [];
        }
        $rolename2invitations[$rolename][] = $invitation;
    }
    foreach ($rolename2invitations as $rolename => $invitations) {
        $message .= $OUTPUT->heading($rolename, 5);
        $message .= html_writer::start_tag('p');

        foreach ($invitations as $invitation) {
            $yesurl->param('invitation'.$invitation->id, 'on');
            $yesurl->param('status', $status);
            $message .= "$invitation->email<br />";
        }

        $message .= html_writer::end_tag('p');
    }
    echo $OUTPUT->confirm($message, $yesurl, $returnto);
}

echo $OUTPUT->footer();
