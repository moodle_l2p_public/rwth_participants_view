<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   local_rwth_participants_view
 * @copyright 2017 RWTH Aachen University
 * @author    t.schroeder@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../../config.php');

use local_rwth_participants_view\participants_table;
use local_rwth_participants_view\role_helper;

$courseid = required_param('id', PARAM_INT); // Course Module ID.

$PAGE->set_url('/local/rwth_participants_view/students.php', ['id' => $courseid]);

$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

require_course_login($course, false, null, false, true);
require_capability('local/rwth_participants_view:exportstudents', $context);

// Should use this variable so that we don't break stuff every time a variable is added or changed.
$baseurl = new moodle_url('/local/rwth_participants_view/students.php', ['id' => $course->id]);

$roleids = explode(',', get_config('rwth_participants_view', 'student_roles'));
$roles = array_filter(role_helper::get_visible_roles($courseid), function($key) use($roleids) {
    return in_array($key, $roleids);
}, ARRAY_FILTER_USE_KEY);

$table = new participants_table($context, $baseurl, $course, $roles);
$table->set_downloading(true)
    ->set_show_fullname(false);
$table->setup();
$output = $PAGE->get_renderer('local_rwth_participants_view');
$csv = $table->render($output);

echo $csv;